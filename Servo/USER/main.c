#include "uart_lib.h"
#include "gpio_lib.h"
#include "servo.h"


static uint8_t main_degree = 0;


int main(void){
	Gpio_Init();
	Servo_Init(main_degree);
	
	USART1_Init(9600);
	while(1){
		if(timer4_ticks >= 5){
			timer4_ticks = 0;
			GPIOC->ODR ^= GPIO_Pin_13;
			Servo_SetDegree(main_degree+=5);
			if(main_degree > 180)	main_degree = 0;
		}
	}
}



