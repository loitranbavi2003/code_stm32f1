#include "servo.h"

void Servo_Init(uint8_t original_degree){
	uint16_t pulse;
	pulse = 200*original_degree/9 + 1000;
	Timer4_Base_Init(0x0000, 39999, 35, 0x0000, 0, ENABLE);		// 20 ms ~ 50 Hz
	Timer_OCx_Init(TIM4, 4, TIM_OCMode_PWM1, ENABLE, pulse, TIM_OCPolarity_High);
}

void Servo_SetDegree(uint8_t degree){
	uint16_t pulse;
	pulse = 200*degree/9 + 1000;
	Timer_OCx_Init(TIM4, 4, TIM_OCMode_PWM1, ENABLE, pulse, TIM_OCPolarity_High);			// 0.5ms - 2.5ms ~ 1000 - 5000
}









