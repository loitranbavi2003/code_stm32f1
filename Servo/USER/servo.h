/*
	**********************************************
	*@file				servo.h						*
	*@author			Vu Quang Luong				*
	*@date				18/10/2023						*
	**********************************************
*/

#ifndef	_SERVO_H_
#define	_SERVO_H_

#ifdef __cplusplus
	extern "C"{
#endif

#include "timer_lib.h"

void Servo_Init(uint8_t original_degree);
void Servo_SetDegree(uint8_t degree);

#ifdef __cplusplus
}
#endif

#endif



/***********************************************/
