#include "stm32f10x.h"
#include "delay.h"
#include "led.h"

int main()
{
	led_init();
	Delay_Init();
	while(1)
	{
		
		GPIO_SetBits(PORT_CONTROL_RS485, GPIO_CONTROL_RS485);
		delay_ms(200);
		GPIO_ResetBits(PORT_CONTROL_RS485, GPIO_CONTROL_RS485);
		delay_ms(200);
	}
}
