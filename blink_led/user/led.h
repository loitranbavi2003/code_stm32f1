#ifndef _LED_H_
#define _LED_H_

#define GPIO_CONTROL_RS485 GPIO_Pin_13
#define PORT_CONTROL_RS485 GPIOC

void led_init(void);

#endif
