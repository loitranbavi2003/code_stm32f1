#include "button.h"

uint8_t flag_button_select_state = 0;
uint8_t flag_button_increase_state = 0;
uint8_t flag_button_reduce_state = 0;

volatile button_state_t button_select, button_increase, button_reduce;

void BUTTON_Config(void)
{
	GPIO_InitTypeDef button;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	button.GPIO_Pin = BUTTON_MODE_GPIO | BUTTON_INCREASE_GPIO | BUTTON_REDUCE_GPIO;			
	button.GPIO_Mode = GPIO_Mode_IPU;
	button.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(BUTTON_PORT, &button);
}

void BUTTON_Read(uint8_t button_input, button_state_t *button_state)
{
	if(button_input == 1)
	{
		if(button_state->count_time <= 2000)
		{
			button_state->count_time++;
			if(button_state->count_time == 70)
			{
				button_state->flag = 1;
				button_state->count_time = 0;
			}
		}
	}
	else
	{
		button_state->flag = 0;
		button_state->count_time = 0;
	}
}

void BUTTON_ReadAll(void)
{
	BUTTON_Read(BUTTON_MODE_INPUT, (button_state_t *)&button_select);
	BUTTON_Read(BUTTON_INCREASE_INPUT, (button_state_t *)&button_increase);
	BUTTON_Read(BUTTON_REDUCE_INPUT, (button_state_t *)&button_reduce);
	
	if(button_select.flag == 1)
	{
		flag_button_select_state = 1;
		button_select.flag = 0;
	}
	
	if(button_increase.flag == 1)
	{
		flag_button_increase_state = 1;
		button_increase.flag = 0;
	}
	
	if(button_reduce.flag == 1)
	{
		flag_button_reduce_state = 1;
		button_reduce.flag = 0;
	}
}
