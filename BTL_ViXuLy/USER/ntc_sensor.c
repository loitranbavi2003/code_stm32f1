#include "ntc_sensor.h"

uint16_t adc_value = 0;
uint32_t kalman_value = 0;
float Vout = 0;
double Rntc = 0;
float Temp_K = 0.0; 
float Temp_C = 0.0; 

void NtcSensor_Init(void)
{
	ADC1_Init();
}

void ADC1_Init(void)
{
	GPIO_InitTypeDef gpio;
	ADC_InitTypeDef   Adc;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1,  ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	
	/* ADC1 channel_7	*/
	gpio.GPIO_Mode = GPIO_Mode_AIN;
	gpio.GPIO_Pin = NTC_PIN;
	gpio.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(NTC_PORT, &gpio);
	
	/* cau hinh ADC1 */
	Adc.ADC_Mode 		 = ADC_Mode_Independent;
	Adc.ADC_ScanConvMode = DISABLE;
	Adc.ADC_ContinuousConvMode = ENABLE;
	Adc.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None;
	Adc.ADC_DataAlign = ADC_DataAlign_Right;
	Adc.ADC_NbrOfChannel = 1;
	ADC_Init(ADC1, &Adc);
	
	/* cau hinh chanel, rank, thoi gian lay mau */
	ADC_RegularChannelConfig(ADC1, ADC_Channel_7, 1, ADC_SampleTime_239Cycles5);
	/* cho phep bo ADC1 hoat dong */
	ADC_Cmd(ADC1, ENABLE);
	/* Reset thanh ghi cablib */
	ADC_ResetCalibration(ADC1);
	/* cho thanh ghi reset cablib xong */
	while(ADC_GetResetCalibrationStatus(ADC1));
	/* khoi dong bo ADC */
	ADC_StartCalibration(ADC1);
	/* cho trang thai cablib duoc bat */
	while(ADC_GetCalibrationStatus(ADC1));
	/* bat dau chuyen doi ADC */
	ADC_SoftwareStartConvCmd(ADC1, ENABLE);
}

void Ntc_Read(void)
{
		adc_value = ADC_GetConversionValue(ADC1);
		kalman_value = (uint32_t)updateEstimate((float)adc_value);
		Vout = kalman_value * 3.3 / 4095.0 - VOUT_CALIB_FACTOR;
		Rntc = (3.3-Vout)*R10K_PULLDOWN / Vout;
		Temp_K = (T0 * B_param)/( B_param + T0 * log(Rntc / R0));
		Temp_C = Temp_K - 273.0;
}






