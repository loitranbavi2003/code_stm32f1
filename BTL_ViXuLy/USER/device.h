#ifndef _DEVICE_H_
#define _DEVICE_H_

#include "sys.h"

#define GPIO_FAN	GPIO_Pin_1
#define PORT_FAN	GPIOB
#define GPIO_LIGHT	GPIO_Pin_0
#define PORT_LIGHT	GPIOB

void Device_Config(void);
void Device_TurnOnFan(void);
void Device_TurnOffFan(void);
void Device_TurnOnLight(void);
void Device_TurnOffLight(void);

#endif
