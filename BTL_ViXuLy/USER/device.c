#include "device.h"

void Device_Config(void)
{
	GPIO_InitTypeDef gpio;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	
	/* Fan */
	gpio.GPIO_Mode = GPIO_Mode_Out_PP;
	gpio.GPIO_Pin = GPIO_FAN;
	gpio.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(PORT_FAN, &gpio);
	
	/* Light */
	gpio.GPIO_Mode = GPIO_Mode_Out_PP;
	gpio.GPIO_Pin = GPIO_LIGHT;
	gpio.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(PORT_LIGHT, &gpio);
}

void Device_TurnOnFan(void)
{
	GPIO_SetBits(PORT_FAN, GPIO_FAN);
}

void Device_TurnOffFan(void)
{
	GPIO_ResetBits(PORT_FAN, GPIO_FAN);
}

void Device_TurnOnLight(void)
{
	GPIO_SetBits(PORT_LIGHT, GPIO_LIGHT);
}

void Device_TurnOffLight(void)
{
	GPIO_ResetBits(PORT_LIGHT, GPIO_LIGHT);
}
