#ifndef _SYS_H_
#define _SYS_H_

#include "math.h"
#include "stdio.h"
#include "stdint.h"
#include "string.h"
#include "stm32f10x.h"
#include "stm32f10x_adc.h"
#include "stm32f10x_i2c.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_usart.h"

#include "i2c.h"
#include "lcd.h"
#include "uart.h"
#include "delay.h"
#include "device.h"
#include "button.h"
#include "ntc_sensor.h"
#include "kalman_filter_lib.h"

void SYS_Config(void);
void SYS_Run(void);
void Display_LCD(void);
void Handle_FanLight(void);

#endif
