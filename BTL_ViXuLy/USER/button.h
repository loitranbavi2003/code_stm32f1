#ifndef _BUTTON_H_
#define _BUTTON_H_

#include "sys.h"

#define BUTTON_MODE_GPIO 		GPIO_Pin_4  
#define BUTTON_INCREASE_GPIO 	GPIO_Pin_5
#define BUTTON_REDUCE_GPIO 		GPIO_Pin_6 
#define BUTTON_PORT 			GPIOA 

#define BUTTON_MODE_INPUT		(uint8_t) GPIO_ReadInputDataBit(BUTTON_PORT, GPIO_Pin_4)
#define BUTTON_INCREASE_INPUT	(uint8_t) GPIO_ReadInputDataBit(BUTTON_PORT, GPIO_Pin_5)
#define BUTTON_REDUCE_INPUT		(uint8_t) GPIO_ReadInputDataBit(BUTTON_PORT, GPIO_Pin_6)

typedef struct
{
	uint8_t flag;
	uint16_t count_time;
} button_state_t;

extern uint8_t flag_button_select_state;
extern uint8_t flag_button_increase_state;
extern uint8_t flag_button_reduce_state;

void BUTTON_Config(void);
void BUTTON_Read(uint8_t button_input, button_state_t *button_state);
void BUTTON_ReadAll(void);

#endif
