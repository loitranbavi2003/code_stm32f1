#include "sys.h"


static uint8_t flag_save_config = 0;
static uint8_t count_push_select = 0;
static uint8_t temp_increase = 0, temp_reduce = 0;
static float current_temp = 25.55;
static float setup_temp_down = 35.55, setup_temp_up = 0;

void SYS_Config(void)
{
	DELAY_Config();
	I2C_Config();
	LCD_Config();
	UART_Config();
	BUTTON_Config();
	Device_Config();
	SimpleKalmanFilter(2, 2, 0.001);
	NtcSensor_Init();
}

void SYS_Run(void)
{
	Ntc_Read();
	Display_LCD();
	
	if(flag_save_config == 1)
	{
		Handle_FanLight();
	}
}

void Display_LCD(void)
{
	current_temp = Temp_C;
	
	if(flag_button_select_state == 1 && count_push_select < 5)
	{
		count_push_select++;
		flag_button_select_state = 0;
	}
	
	if(count_push_select == 0)
	{
		// Config Temp
		flag_button_reduce_state = 0;
		flag_button_increase_state = 0;
		LCD_Print_On();
	}
	else if(count_push_select == 1)
	{
		LCD_SetTempDown(setup_temp_down);
		
		// Temp Up
		if(flag_button_increase_state == 1)
		{
			temp_increase++;
			setup_temp_down = setup_temp_down + temp_increase - temp_reduce;
			temp_reduce = 0;
			temp_increase = 0;
			flag_button_increase_state = 0;
		}
		
		// Temp Down
		if(flag_button_reduce_state == 1)
		{
			temp_reduce++;
			setup_temp_down = setup_temp_down + temp_increase - temp_reduce;
			temp_reduce = 0;
			temp_increase = 0;
			flag_button_reduce_state = 0;
		}
	}
	else if(count_push_select == 2)
	{
		// Save Temp Down
		setup_temp_up = setup_temp_down;
		LCD_SaveTempDown(setup_temp_down);
	}
	else if(count_push_select == 3)
	{
		LCD_SetTempUp(setup_temp_up);
		
		// Temp Up
		if(flag_button_increase_state == 1)
		{
			temp_increase++;
			setup_temp_up = setup_temp_up + temp_increase - temp_reduce;
			temp_reduce = 0;
			temp_increase = 0;
			flag_button_increase_state = 0;
		}
		
		// Temp Down
		if(flag_button_reduce_state == 1)
		{
			temp_reduce++;
			setup_temp_up = setup_temp_up + temp_increase - temp_reduce;
			temp_reduce = 0;
			temp_increase = 0;
			flag_button_reduce_state = 0;
		}
	}
	else if(count_push_select == 4)
	{
		// Save Temp Up
		LCD_SaveTempUp(setup_temp_up);
	}
	else if(count_push_select == 5)
	{
		// Config Done
		flag_save_config = 1;
		LCD_DisplayCurrentTemp(current_temp);
	}
}

void Handle_FanLight(void)
{
	if(Temp_C >= setup_temp_down && Temp_C <= setup_temp_up)
	{
		// Turn Off Fan and Light
		Device_TurnOffFan();
		Device_TurnOffLight();
	}
	else if(Temp_C < setup_temp_down)
	{
		// Turn On Light
		Device_TurnOnLight();
	}
	else if(Temp_C > setup_temp_up)
	{
		// Turn On Fan
		Device_TurnOnFan();
	}
}
