#ifndef _DELAY_H_
#define _DELAY_H_

#include "sys.h"

#ifdef __cplusplus
 extern "C" {
#endif


void SysTick_Init(void);
void SysTick_Handler(void);
uint64_t SysTick64(void);
uint32_t SysTick32(void);
uint32_t SysTick24(void);
uint64_t SysTick_Millis(void);
uint64_t SysTick_Micros(void);
void delay_us(unsigned long us);
void delay_ms(unsigned long ms);


#ifdef __cplusplus
}
#endif

#endif
