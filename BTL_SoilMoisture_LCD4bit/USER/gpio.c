#include "gpio.h"


void GPIO_INIT(void)
{
	GPIO_InitTypeDef  Gpio;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB, ENABLE);
	
	/* cau hinh chan input cua bo ADC1 la chan PA0 */
	Gpio.GPIO_Mode = GPIO_Mode_AIN;
	Gpio.GPIO_Pin = GPIO_Pin_0;
	Gpio.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &Gpio);
	
	/* cau hinh Led Red A1 */
	Gpio.GPIO_Mode = GPIO_Mode_Out_PP;
	Gpio.GPIO_Pin = LED_RED_GPIO;
	Gpio.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(LED_PORT, &Gpio);
	
	/* cau hinh Led Green A2 */
	Gpio.GPIO_Mode = GPIO_Mode_Out_PP;
	Gpio.GPIO_Pin = LED_GREEN_GPIO;
	Gpio.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(LED_PORT, &Gpio);
}
