#include "adc.h"

int ADC_Read(void)
{
	return(ADC_GetConversionValue(ADC1));
}

void ADC_Config(void)
{
	ADC_InitTypeDef   Adc;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1,  ENABLE);
	
	/* cau hinh ADC1 */
	Adc.ADC_Mode 		 = ADC_Mode_Independent;
	Adc.ADC_ScanConvMode = DISABLE;
	Adc.ADC_ContinuousConvMode = ENABLE;
	Adc.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None;
	Adc.ADC_DataAlign = ADC_DataAlign_Right;
	Adc.ADC_NbrOfChannel = 1;
	ADC_Init(ADC1, &Adc);
	
	/* cau hinh chanel, rank, thoi gian lay mau */
	ADC_RegularChannelConfig(ADC1, ADC_Channel_0, 1, ADC_SampleTime_55Cycles5);
	/* cho phep bo ADC1 hoat dong */
	ADC_Cmd(ADC1, ENABLE);
	/* cho phep cam bien nhiet do hoat dong 
	ADC_TempSensorVrefintCmd(ENABLE);*/
	/* Reset thanh ghi cablib */
	ADC_ResetCalibration(ADC1);
	/* cho thanh ghi reset cablib xong */
	while(ADC_GetResetCalibrationStatus(ADC1));
	/* khoi dong bo ADC */
	ADC_StartCalibration(ADC1);
	/* cho trang thai cablib duoc bat */
	while(ADC_GetCalibrationStatus(ADC1));
	/* bat dau chuyen doi ADC */
	ADC_SoftwareStartConvCmd(ADC1, ENABLE);
}
