#include "sys.h"


void SYS_Init(void)
{
	GPIO_INIT();
	DELAY_INIT();
	UART_INIT();
}

void SYS_Run(void)
{
	if(RX_FLAG_END_LINE == 1)
	{
		if(strstr(RRX,"ON") != NULL)
		{
			printf(" Bat\n");
			GPIO_ResetBits(PORT_BUTTON_1, PIN_BUTTON_1);
		}
		else if(strstr(RRX,"OFF") != NULL)
		{
			printf(" Tat\n");
			GPIO_SetBits(PORT_BUTTON_1, PIN_BUTTON_1);
		}
		RX_FLAG_END_LINE = 0;
	}
	
//	GPIO_SetBits(PORT_BUTTON_1, PIN_BUTTON_1);
//	delay_ms(1000);
//	GPIO_ResetBits(PORT_BUTTON_1, PIN_BUTTON_1);
//	delay_ms(1000);
}
