#include "stm32f10x.h"                  
#include "stm32f10x_gpio.h"
#include "stm32f10x_rcc.h"

uint8_t stt_old = 0, stt_new = 0;
uint16_t counttime = 0;
void delay(uint16_t time);
void led_Init(void);
void button_Init(void);



int main(){
	led_Init();
	button_Init();
	uint16_t led7[10] = {0x7F, 0x06, 0x9B, 0x8F, 0xE6, 0xED, 0xFD, 0x07, 0xFF, 0xEF};
	uint16_t dem=0;
	while(1){
		stt_new = GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_8);
		if(stt_new == 1)
		{
			if(counttime <= 1000)
			{
				counttime++;
				if(counttime == 100)
				{
					if(dem >= 9)
					dem = 0;
					else
					dem++;
				}
			}
		}
		else{
			counttime = 0;
		}
		GPIO_Write(GPIOA, led7[dem]);
		delay(1);
	}
		
}

void delay(uint16_t time){
	uint16_t i;
	while(time--){
		for(i = 0; i < 1998; i++);
	}
}

void led_Init(void){
	GPIO_InitTypeDef led;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	led.GPIO_Mode = GPIO_Mode_Out_PP;
	led.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1| GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_6 | GPIO_Pin_7;
	led.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &led);
}

void button_Init(void){
	GPIO_InitTypeDef button;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	button.GPIO_Mode = GPIO_Mode_IPU;
	button.GPIO_Pin = GPIO_Pin_8;
	button.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &button);
}
