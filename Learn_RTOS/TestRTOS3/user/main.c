#include "stm32f10x.h"                  // Device header
#include "stm32f10x_gpio.h"             // Keil::Device:StdPeriph Drivers:GPIO
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"                      // ARM.FreeRTOS::RTOS:Core
#include "event_groups.h"               // ARM.FreeRTOS::RTOS:Event Groups
#include "usart.h"
uint8_t Recei[1000];

// Viet Doan nay thoi!
GPIO_InitTypeDef  GPIO_InitStructure;

void Fn_GPIO_Init (void);
void Fn_RTOS_TaskLed1(void *p);
void Fn_RTOS_TaskLed2(void *p);
void Fn_RTOS_TaskUSART(void *p);

xQueueHandle xQueue;
xSemaphoreHandle xMutex;
EventGroupHandle_t xEvent;

int main (void)
{
	SystemInit();
	SystemCoreClockUpdate();
	
	USART_Config(9600);
	Fn_GPIO_Init();
	
	xMutex = xSemaphoreCreateMutex();
	xQueue = xQueueCreate(10, sizeof(int));
	xEvent = xEventGroupCreate();
	
	xTaskCreate(Fn_RTOS_TaskUSART, (const char*) "USART send", 128, NULL, 1, NULL);
	xTaskCreate(Fn_RTOS_TaskLed1, (const char*) "Red LED send", 128, NULL, 1, NULL);
	xTaskCreate(Fn_RTOS_TaskLed2, (const char*) "Red LED Blink", 128, NULL, 1, NULL);

	vTaskStartScheduler();
	return 0;
}

void Fn_GPIO_Init (void)
{
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOC, &GPIO_InitStructure);
}

void Fn_RTOS_TaskLed1(void *p)
{
	uint32_t queue;
	uint32_t vri_queue_flag1;
	
	while(1){
		vri_queue_flag1 = xEventGroupWaitBits(xEvent,1,pdTRUE, pdFALSE, 10);
		if(vri_queue_flag1 == 0x0001)
		{
			USART_Printf("Led1\n");
			GPIO_WriteBit(GPIOC,GPIO_Pin_13, 1);
			queue = 1;
			xSemaphoreTake(xMutex, portMAX_DELAY);
			xQueueSend(xQueue, &queue, 0);
			xSemaphoreGive(xMutex);
		}
	}
}

void Fn_RTOS_TaskLed2(void *p)
{
	uint32_t queue;
	uint32_t vri_queue_flag2 = 0;
	
	while(1)
	{
		vri_queue_flag2 = xEventGroupWaitBits(xEvent,2,pdTRUE, pdFALSE, 10);	
		if(vri_queue_flag2 == 0x0002)
		{
			USART_Printf("Led2\n");
			GPIO_WriteBit(GPIOC,GPIO_Pin_13, 0);
			queue = 2;
			xSemaphoreTake(xMutex, portMAX_DELAY);
			xQueueSend(xQueue, &queue, 0);
			xSemaphoreGive(xMutex);
		}
	}
}

void Fn_RTOS_TaskUSART(void *p)
{
	uint32_t queue_receive;
	while(1)
	{
		if(USART_FlagDB() == 1)
		{
			if(USART_Compare("set1") == 1)
			{
				xEventGroupSetBits(xEvent,1);
			}
			else if(USART_Compare("set2") == 1)
			{
				xEventGroupSetBits(xEvent,2);
			}
		}
		
		if(xQueueReceive(xQueue, &queue_receive, 10))
		{
			if(queue_receive == 1)
			{
				USART_SendString("Set1 OK\n");
			}
			if(queue_receive == 2)
			{
				USART_SendString("Set2 OK\n");
			}
		}
		
		vTaskDelay(1/portTICK_RATE_MS);
	}
}
