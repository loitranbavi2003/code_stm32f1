#include "stm32f10x.h"    
#include "stdio.h"
#include "button.h"
#include "delay.h"
#include "usart.h"

vrts_button_config vrts_button_BT1, vrts_button_BT2, vrts_button_BT3;

int main(){
	Delay_Init();
	Usart_Init();
	Fn_button_Setup_pin_button(PA7);
	Fn_button_Setup_pin_button(PB5);
	Fn_button_Setup_pin_button(PB6);
	while(1){
		printf("PA7 one press \n");
		delay_ms(500);
		Fn_button_push_button_survey(PA7, &vrts_button_BT1);
		if(vrts_button_BT1.vrui8_count_push == 1)
		{
			printf("PA7 one press \n");
			Fn_button_reset_count_push(&vrts_button_BT1);
		}
		else if(vrts_button_BT1.vrui8_count_push == 2)
		{
			printf("PA7 two press \n");
			Fn_button_reset_count_push(&vrts_button_BT1);
		}
		else if(vrts_button_BT1.vrui8_count_push == 3)
		{
			printf("PA7 three press \n");
			Fn_button_reset_count_push(&vrts_button_BT1);
		}
		else if(vrts_button_BT1.vrui8_count_push == 4)
		{
			printf("PA7 four press \n");
			Fn_button_reset_count_push(&vrts_button_BT1);
		}
		else if(vrts_button_BT1.vrui8_count_push == 5)
		{
			printf("PA7 five press \n");
			Fn_button_reset_count_push(&vrts_button_BT1);
		}
		else if(vrts_button_BT1.vrui8_button_hold == 1)
		{
			printf("PA7 long press \n");
			Fn_button_reset_count_push(&vrts_button_BT1);
		}

/*-----------------------------------------------------------------*/		
		Fn_button_push_button_survey(PB5, &vrts_button_BT2);
		if(vrts_button_BT2.vrui8_count_push == 1)
		{
			printf("PB5 one press \n");
			Fn_button_reset_count_push(&vrts_button_BT2);
		}
		else if(vrts_button_BT2.vrui8_count_push == 2)
		{
			printf("PB5 two press \n");
			Fn_button_reset_count_push(&vrts_button_BT2);
		}
		else if(vrts_button_BT2.vrui8_count_push == 3)
		{
			printf("PB5 three press \n");
			Fn_button_reset_count_push(&vrts_button_BT2);
		}
		else if(vrts_button_BT2.vrui8_count_push == 4)
		{
			printf("PB5 four press \n");
			Fn_button_reset_count_push(&vrts_button_BT2);
		}
		else if(vrts_button_BT2.vrui8_count_push == 5)
		{
			printf("PB5 five press \n");
			Fn_button_reset_count_push(&vrts_button_BT2);
		}
		else if(vrts_button_BT2.vrui8_button_hold == 1)
		{
			printf("PB5 long press \n");
			Fn_button_reset_count_push(&vrts_button_BT2);
		}
		
/*-----------------------------------------------------------------*/
		Fn_button_push_button_survey(PB6, &vrts_button_BT3);
		if(vrts_button_BT3.vrui8_count_push == 1)
		{
			printf("PB6 one press\n");
			Fn_button_reset_count_push(&vrts_button_BT3);
		}
		else if(vrts_button_BT3.vrui8_count_push == 2)
		{
			printf("PB6 two press \n");
			Fn_button_reset_count_push(&vrts_button_BT3);
		}
		else if(vrts_button_BT3.vrui8_count_push == 3)
		{
			printf("PB6 three press \n");
			Fn_button_reset_count_push(&vrts_button_BT3);
		}
		else if(vrts_button_BT3.vrui8_count_push == 4)
		{
			printf("PB6 four press \n");
			Fn_button_reset_count_push(&vrts_button_BT3);
		}
		else if(vrts_button_BT3.vrui8_count_push == 5)
		{
			printf("PB6 five press \n");
			Fn_button_reset_count_push(&vrts_button_BT3);
		}
		else if(vrts_button_BT3.vrui8_button_hold == 1)
		{
			printf("PB6 long press \n");
			Fn_button_reset_count_push(&vrts_button_BT3);
		}
	}
}

