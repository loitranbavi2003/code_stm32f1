#ifndef _BUTTON_H_
#define _BUTTON_H_

#include "stm32f10x.h"

/*--------------------------------------------*/

void Fn_button_Pin_config(uint16_t vrui16_Pinx);
void Fn_button_Setup_pin_button(uint16_t vrui16_Pinx);
typedef struct
{
	GPIO_TypeDef *GPIOx;
	uint16_t vrui16_Pinx;
	uint16_t vrui16_RCC_APB2Periph_GPIOx;
	
}Array_pin_button;

/*--------------------------------------------*/

typedef struct
{
	uint8_t vrui8_array_stt_button[50];
	
	uint8_t vrui8_flag_button;
	uint8_t vrui8_flag_hold;
	uint8_t vrui8_flag_start_1000ms;
	
	uint8_t vrui8_button_hold;
	
	uint8_t vrui8_count_press_button;
	uint8_t vrui8_count_push;
	uint8_t vrui8_count_array;
	
	uint16_t 	vrui16_count_button_hold;
	uint16_t 	vrui16_count_1000ms;
}vrts_button_config;

extern vrts_button_config vrts_button_BT1, vrts_button_BT2, vrts_button_BT3;
void Fn_button_push_button_survey(uint16_t Pinx, vrts_button_config *vrts_BT);
void Fn_button_reset_count_push(vrts_button_config *BT);

/*--------------------------------------------*/


#define PA0 			(0u)
#define PA1				(1u)
#define PA2				(2u)
#define PA3				(3u)
#define PA4				(4u)
#define PA5				(5u)
#define PA6				(6u)
#define PA7				(7u)
#define PA8				(8u)
#define PA9				(9u)
#define PA10			(10u)
#define PA11			(11u)
#define PA12			(12u)
#define PA13			(13u)
#define PA14			(14u)
#define PA15			(15u)

#define PB0				(16u)
#define PB1				(17u)		
#define PB2				(18u)
#define PB3				(19u)
#define PB4				(20u)
#define PB5				(21u)
#define PB6				(22u)
#define PB7				(23u)
#define PB8				(24u)
#define PB9				(25u)
#define PB10			(26u)
#define PB11			(27u)
#define PB12			(28u)
#define PB13			(29u)
#define PB14			(30u)
#define PB15			(31u)

#define PC0 			(32u)
#define PC1 			(33u)			
#define PC2 			(34u)
#define PC3 			(35u)
#define PC4 			(36u)
#define PC5 			(37u)
#define PC6 			(38u)
#define PC7 			(39u)
#define PC8 			(40u)
#define PC9 			(41u)
#define PC10 			(42u)
#define PC11 			(43u)
#define PC12 			(44u)
#define PC13 			(45u)
#define PC14 			(46u)
#define PC15 			(47u)

#endif
