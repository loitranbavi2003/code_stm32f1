#include "stm32f10x.h"
#include "stm32f10x_usart.h"
#include "stdio.h"
#include "string.h"
#include "stdlib.h"
#include "usart.h"

struct FILE{
	int dummy;
};
FILE __stdout;

int fputc(int ch, FILE *f){
	Usart_SendChar(ch);
	return ch;
}

void Usart_Init(void){
	USART_InitTypeDef Usart;
	GPIO_InitTypeDef Gpio;
	// enable clock
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
	// config Tx-A9
	Gpio.GPIO_Mode = GPIO_Mode_AF_PP;
	Gpio.GPIO_Pin = GPIO_Pin_9;
	Gpio.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &Gpio);
	// config Rx-A10
	Gpio.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	Gpio.GPIO_Pin = GPIO_Pin_10;
	Gpio.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &Gpio);
	// config Usart
	Usart.USART_BaudRate = 9600;
	Usart.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	Usart.USART_Mode = USART_Mode_Tx | USART_Mode_Rx;
	Usart.USART_Parity = USART_Parity_No;
	Usart.USART_StopBits = USART_StopBits_1;
	Usart.USART_WordLength = USART_WordLength_8b;
	USART_Init(USART1, &Usart);
	// enable usart1 global interrupt
	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
	USART_Cmd(USART1, ENABLE);
	USART_ClearFlag(USART1, ENABLE);
	NVIC_EnableIRQ(USART1_IRQn);
}

void Usart_SendChar(char _ch){
	USART_SendData(USART1, _ch);
	while(USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET);
}

void Usart_SendString(char *_str){
	while(*_str != NULL){
		Usart_SendChar(*_str++);
	}
}

void Usart_SendNumber(int _number){
	char temp[20];
	sprintf(temp, "%d", _number);
	Usart_SendString(temp);
}
