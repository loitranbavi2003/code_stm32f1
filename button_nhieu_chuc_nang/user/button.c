#include "stm32f10x.h"
#include "stdio.h"
#include "button.h"
#include "usart.h"
#include "delay.h"

/*------------------------------------------------------------------------------*/

Array_pin_button vrts_array_pin_button[]=
{
		{GPIOA , GPIO_Pin_0, RCC_APB2Periph_GPIOA},		
		{GPIOA , GPIO_Pin_1, RCC_APB2Periph_GPIOA},
		{GPIOA , GPIO_Pin_2, RCC_APB2Periph_GPIOA},
		{GPIOA , GPIO_Pin_3, RCC_APB2Periph_GPIOA},
		{GPIOA , GPIO_Pin_4, RCC_APB2Periph_GPIOA},
		{GPIOA , GPIO_Pin_5, RCC_APB2Periph_GPIOA},
		{GPIOA , GPIO_Pin_6, RCC_APB2Periph_GPIOA},
		{GPIOA , GPIO_Pin_7, RCC_APB2Periph_GPIOA},
		{GPIOA , GPIO_Pin_8, RCC_APB2Periph_GPIOA},
		{GPIOA , GPIO_Pin_9, RCC_APB2Periph_GPIOA},
		{GPIOA , GPIO_Pin_10, RCC_APB2Periph_GPIOA},
		{GPIOA , GPIO_Pin_11, RCC_APB2Periph_GPIOA},
		{GPIOA , GPIO_Pin_12, RCC_APB2Periph_GPIOA},
		{GPIOA , GPIO_Pin_13, RCC_APB2Periph_GPIOA},
		{GPIOA , GPIO_Pin_14, RCC_APB2Periph_GPIOA},
		{GPIOA , GPIO_Pin_15, RCC_APB2Periph_GPIOA},
		
		{GPIOB , GPIO_Pin_0, RCC_APB2Periph_GPIOB},		
		{GPIOB , GPIO_Pin_1, RCC_APB2Periph_GPIOB},
		{GPIOB , GPIO_Pin_2, RCC_APB2Periph_GPIOB},
		{GPIOB , GPIO_Pin_3, RCC_APB2Periph_GPIOB},
		{GPIOB , GPIO_Pin_4, RCC_APB2Periph_GPIOB},
		{GPIOB , GPIO_Pin_5, RCC_APB2Periph_GPIOB},
		{GPIOB , GPIO_Pin_6, RCC_APB2Periph_GPIOB},
		{GPIOB , GPIO_Pin_7, RCC_APB2Periph_GPIOB},
		{GPIOB , GPIO_Pin_8, RCC_APB2Periph_GPIOB},
		{GPIOB , GPIO_Pin_9, RCC_APB2Periph_GPIOB},
		{GPIOB , GPIO_Pin_10, RCC_APB2Periph_GPIOB},
		{GPIOB , GPIO_Pin_11, RCC_APB2Periph_GPIOB},
		{GPIOB , GPIO_Pin_12, RCC_APB2Periph_GPIOB},
		{GPIOB , GPIO_Pin_13, RCC_APB2Periph_GPIOB},
		{GPIOB , GPIO_Pin_14, RCC_APB2Periph_GPIOB},
		{GPIOB , GPIO_Pin_15, RCC_APB2Periph_GPIOB},
		
		{GPIOC , GPIO_Pin_0, RCC_APB2Periph_GPIOC},		
		{GPIOC , GPIO_Pin_1, RCC_APB2Periph_GPIOC},
		{GPIOC , GPIO_Pin_2, RCC_APB2Periph_GPIOC},
		{GPIOC , GPIO_Pin_3, RCC_APB2Periph_GPIOC},
		{GPIOC , GPIO_Pin_4, RCC_APB2Periph_GPIOC},
		{GPIOC , GPIO_Pin_5, RCC_APB2Periph_GPIOC},
		{GPIOC , GPIO_Pin_6, RCC_APB2Periph_GPIOC},
		{GPIOC , GPIO_Pin_7, RCC_APB2Periph_GPIOC},
		{GPIOC , GPIO_Pin_8, RCC_APB2Periph_GPIOC},
		{GPIOC , GPIO_Pin_9, RCC_APB2Periph_GPIOC},
		{GPIOC , GPIO_Pin_10, RCC_APB2Periph_GPIOC},
		{GPIOC , GPIO_Pin_11, RCC_APB2Periph_GPIOC},
		{GPIOC , GPIO_Pin_12, RCC_APB2Periph_GPIOC},
		{GPIOC , GPIO_Pin_13, RCC_APB2Periph_GPIOC},
		{GPIOC , GPIO_Pin_14, RCC_APB2Periph_GPIOC},
		{GPIOC , GPIO_Pin_15, RCC_APB2Periph_GPIOC}
};

void Fn_button_Pin_config(uint16_t vrui16_Pinx)
{
	GPIO_InitTypeDef GPIO;
	RCC_APB2PeriphClockCmd(vrts_array_pin_button[vrui16_Pinx].vrui16_RCC_APB2Periph_GPIOx, ENABLE );
	GPIO_PinRemapConfig(GPIO_Remap_SWJ_Disable, ENABLE);
	GPIO.GPIO_Pin = vrts_array_pin_button[vrui16_Pinx].vrui16_Pinx;
	GPIO.GPIO_Mode = GPIO_Mode_IPU;
	GPIO.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(vrts_array_pin_button[vrui16_Pinx].GPIOx, &GPIO );
}

void Fn_button_Setup_pin_button(uint16_t vrui16_Pinx)
{
	Fn_button_Pin_config(vrui16_Pinx);
}


/*------------------------------------------------------------------------------*/



void Fn_button_push_button_survey(uint16_t vrui16_Pinx, vrts_button_config *vrts_BT)
{
	uint8_t vrui8_i;

	if(GPIO_ReadInputDataBit(vrts_array_pin_button[vrui16_Pinx].GPIOx , vrts_array_pin_button[vrui16_Pinx].vrui16_Pinx)==1)
	{
		vrts_BT->vrui8_count_press_button++;
		if(vrts_BT->vrui8_count_press_button == 10)
		{
			vrts_BT->vrui8_count_press_button = 0;
			vrts_BT->vrui8_array_stt_button[vrts_BT->vrui8_count_array++] = 1;
			vrts_BT->vrui8_flag_button = 1;
		}
		
		vrts_BT->vrui16_count_button_hold++;
		if(vrts_BT->vrui16_count_button_hold > 700)
		{
			vrts_BT->vrui8_flag_hold = 1;
			vrts_BT->vrui16_count_button_hold = 710;
		}
	}
	else if(vrts_BT->vrui8_flag_button == 1)
	{
		if(vrts_BT->vrui8_count_press_button == 10)
		{
			vrts_BT->vrui8_count_press_button = 0;
			vrts_BT->vrui8_array_stt_button[vrts_BT->vrui8_count_array++] = 0;
			vrts_BT->vrui8_flag_button = 0;
		}
		else
		{
			vrts_BT->vrui8_count_press_button++;
		}
		
		if(vrts_BT->vrui8_flag_hold == 1)
		{
			vrts_BT->vrui8_button_hold = 1;
		}
		
		vrts_BT->vrui16_count_button_hold = 0;
		vrts_BT->vrui8_flag_hold = 0;
	}
	
	if(vrts_BT->vrui8_flag_hold == 0)
	{
		if(vrts_BT->vrui8_flag_button == 1 && vrts_BT->vrui8_count_array <= 1)
		{
			vrts_BT->vrui8_flag_button = 0;
			vrts_BT->vrui16_count_1000ms = 0;
			vrts_BT->vrui8_flag_start_1000ms = 1;
			printf("----------------Stats---------------\n");
		}
		
		if(vrts_BT->vrui8_flag_start_1000ms == 1)
		{
			if(vrts_BT->vrui16_count_1000ms < 1000)
			{
				vrts_BT->vrui16_count_1000ms++;
			}
			else
			{
				vrts_BT->vrui16_count_1000ms = 0;
				vrts_BT->vrui8_flag_start_1000ms = 0;
				
	//			for(vrui8_i = 0; vrui8_i < 50; vrui8_i++)
	//			{
	//				printf("%d ", vrts_BT->vrui8_array_stt_button[vrui8_i]);
	//			}
				
				if(vrts_BT->vrui8_array_stt_button[0] == 1)
				{
					vrts_BT->vrui8_count_push = 1;
					for(vrui8_i = 2; vrui8_i < 49; vrui8_i++)
					{
						if(vrts_BT->vrui8_array_stt_button[vrui8_i] == 0 && vrts_BT->vrui8_array_stt_button[vrui8_i+1] == 1)
						{
							vrts_BT->vrui8_count_push++;
						}
					}
				}
				
				for(vrui8_i = 0; vrui8_i < 50; vrui8_i++)
				{
					vrts_BT->vrui8_array_stt_button[vrui8_i] = 0;
				}
				vrts_BT->vrui8_count_array = 0;
				vrts_BT->vrui8_flag_button = 0;
				printf("\n");
			}
		}
	}
	delay_ms(1);
}

void Fn_button_reset_count_push(vrts_button_config *vrts_BT)
{
	uint8_t vrui8_i;
	
	vrts_BT->vrui8_button_hold = 0;
	vrts_BT->vrui8_flag_button = 0;
	vrts_BT->vrui8_count_push = 0;
	vrts_BT->vrui8_count_array = 0;
	vrts_BT->vrui16_count_button_hold = 0;
	
	for(vrui8_i = 0; vrui8_i < 50; vrui8_i++)
	{
		vrts_BT->vrui8_array_stt_button[vrui8_i] = 0;
	}
}

/*------------------------------------------------------------------------------*/
