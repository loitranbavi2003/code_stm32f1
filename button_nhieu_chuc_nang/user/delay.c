#include "stm32f10x.h"
#include "delay.h"

uint16_t nTicks = 0;
void Delay_Init(void){
	SystemCoreClockUpdate();
	SysTick_Config(SystemCoreClock/1000);
}

void SysTick_Handler(void){
	nTicks--;
}

void delay_ms(uint16_t time){
	nTicks = time;
	while(nTicks != 0);
}
