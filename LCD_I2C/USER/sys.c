#include "sys.h"


void SYS_Config(void)
{
	DELAY_Config();
	UART_Config();
	I2C_Config();
	LCD_Config();
}

void SYS_Run(void)
{
	LCD_Goto_XY(1,0);
	LCD_Write_String("Initializing...");
}
