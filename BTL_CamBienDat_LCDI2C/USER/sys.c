#include "sys.h"


static uint16_t count_time = 0;
static long adc = 0, soil_moisture = 0;

void SYS_Config(void)
{
	DELAY_Config();
	UART_Config();
	GPIO_INIT();
	ADC_Config();
	I2C_Config();
	LCD_Config();
}

void SYS_Run(void)
{
	Read_SoilMoisture();
	
	LCD_Goto_XY(1,0);
	LCD_Write_String("Soil Moist: ");
	LCD_Interger_Number(soil_moisture);
	
	if(soil_moisture < 30)
	{
		GPIO_SetBits(LED_PORT, LED_RED_GPIO);
		GPIO_ResetBits(LED_PORT, LED_GREEN_GPIO);
		LCD_Goto_XY(2,1);
		LCD_Write_String("Status: Dried");
	}
	else if(soil_moisture >= 30 && soil_moisture <= 60)
	{
		GPIO_ResetBits(LED_PORT, LED_RED_GPIO);
		GPIO_SetBits(LED_PORT, LED_GREEN_GPIO);
		LCD_Goto_XY(2,1);
		LCD_Write_String("Status: Good ");
	}
	else
	{
		GPIO_SetBits(LED_PORT, LED_RED_GPIO);
		GPIO_ResetBits(LED_PORT, LED_GREEN_GPIO);
		LCD_Goto_XY(2,1);
		LCD_Write_String("Status: Wet  ");
	}
	
//	delay_ms(1);
}

void Read_SoilMoisture(void)
{
	if(count_time < 100)
	{
		count_time++;
		adc += ADC_Read();
	}
	else
	{
		adc /= 100;
		count_time = 0;
		soil_moisture = map(adc, 0, 4095, 100, 0);
//		printf("%ld\n", soil_moisture);
	}
}

long map(long x, long in_min, long in_max, long out_min, long out_max) 
{
    const long run = in_max - in_min;
    if(run == 0)
	{
        printf("map(): Invalid input range, min == max\n");
        return -1; 
    }
	
    const long rise = out_max - out_min;
    const long delta = x - in_min;
	
    return (delta * rise) / run + out_min;
}
