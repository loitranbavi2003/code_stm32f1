#ifndef _GPIO_H_
#define _GPIO_H_

#include "sys.h"

#define LED_RED_GPIO 	GPIO_Pin_7
#define LED_GREEN_GPIO 	GPIO_Pin_5
#define LED_PORT		GPIOB

void GPIO_INIT(void);

#endif
