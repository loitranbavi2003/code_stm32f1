#include "sys.h"


void SYS_Config(void)
{
	DELAY_Config();
	UART_Config();
	I2C_Config();
	LCD_Config();
	NtcSensor_Init();
	SimpleKalmanFilter(2, 2, 0.001);
}

void SYS_Run(void)
{
	Ntc_Read();
	
	LCD_Goto_XY(1,2);
	LCD_Write_String("Nhiet do la:");
	LCD_Goto_XY(0,6);
	LCD_Float_Number(Temp_C);
	LCD_Goto_XY(0,10);
	LCD_Write_String("   ");
}
