
#ifndef	__NTC__
#define	__NTC__

#ifdef __cplusplus
	extern "C"{
#endif

#include "sys.h"

#define VOUT_CALIB_FACTOR	0.019912
#define B_param				4413.608339
#define R0					10570
#define T0					298.15
#define R10K_PULLDOWN		9950 

#define NTC_PIN			GPIO_Pin_7				// NTC Pin
#define NTC_PORT		GPIOA					// Port: A

extern uint8_t sample;
extern uint16_t adc_value;
extern uint32_t	kalman_value;
extern float Vout;
extern float Temp_C;

void ADC1_Init(void);
void NtcSensor_Init(void);
void Ntc_Read(void);

#ifdef __cplusplus
}
#endif

#endif



/***********************************************/


