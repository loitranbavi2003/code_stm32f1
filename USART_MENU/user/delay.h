#ifndef _DELAY_H_
#define _DELAY_H_

void Delay_Init(void);
void SysTick_Handler(void);
void delay_ms(unsigned int time);
	
#endif 
