#include "stm32f10x.h"
#include "stm32f10x_usart.h"
#include "string.h"
#include "stdio.h"
#include "uart.h"
#include "delay.h"
#include "led.h"
#include "menu.h"
#include "gw_gpio.h"
#include "gw_test.h"

int main()
{
	Gw_GPIO_Init();
	fn_usart_config(9600);

	while(1)
	{
		fn_menu();
	}
}
