#ifndef _TEST_H_
#define _TEST_H_

#include "stm32f10x.h"
#include "stdio.h"

void fn_test_ena(int port);
void Gw_Test_Gpio_ReadStt(uint8_t mode);

#endif
