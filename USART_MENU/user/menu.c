#include "stm32f10x.h"
#include "stm32f10x_usart.h"
#include "string.h"
#include "stdio.h"
#include "menu.h"
#include "uart.h"
#include "delay.h"
#include "led.h"
#include "gw_gpio.h"
#include "gw_test.h"


uint8_t vrui_flag_menu = 1;

void fn_menu(void)
{
	if(vrui_flag_menu == 1)
	{
		printf("+++++++++++++++++++++++++++++++++ \n");
		printf("-------- Chon Port test --------- \n");
		printf("-  Port_1 : 1                   - \n");
		printf("-  Port_2 : 2                   - \n");
		printf("-  Port_3 : 3                   - \n");
		printf("-  Port_4 : 4                   - \n");
		printf("-  Port_5 : 5                   - \n");
		printf("-  Port_6 : 6                   - \n");
		printf("--------------------------------- \n");
		
		vrui_flag_menu = 0;
	}
		
	if(RX_FLAG_END_LINE == 1)
	{	
		printf("<->");
		
		if(strstr(RRX,"1")!= NULL)
		{
			Gw_GPIO_Set_ON(1);
			if(GPIO_ReadInputDataBit(GPIOB, GET_GPIO_PORTB_1) == 0)
			{
				printf("Port_1 done!");
			}
			else
			{
				printf("Port_1 error!");
			}
			Gw_GPIO_Set_OFF(1);
			vrui_flag_menu = 1;
		}
		if(strstr(RRX,"2")!= NULL)
		{
			Gw_GPIO_Set_ON(2);
			if(GPIO_ReadInputDataBit(GPIOB, GET_GPIO_PORTB_2) == 0)
			{
				printf("Port_2 done!");
			}
			else
			{
				printf("Port_2 error!");
			}
			Gw_GPIO_Set_OFF(2);
			vrui_flag_menu = 1;
		}
		if(strstr(RRX,"3")!= NULL)
		{
			Gw_GPIO_Set_ON(3);
			if(GPIO_ReadInputDataBit(GPIOB, GET_GPIO_PORTB_3) == 0)
			{
				printf("Port_3 done!");
			}
			else
			{
				printf("Port_3 error!");
			}
			Gw_GPIO_Set_OFF(3);
			vrui_flag_menu = 1;
		}
		if(strstr(RRX,"4")!= NULL)
		{
			Gw_GPIO_Set_ON(4);
			if(GPIO_ReadInputDataBit(GPIOB, GET_GPIO_PORTB_4) == 0)
			{
				printf("Port_4 done!");
			}
			else
			{
				printf("Port_4 error!");
			}
			Gw_GPIO_Set_OFF(4);
			vrui_flag_menu = 1;
		}
		if(strstr(RRX,"5")!= NULL)
		{
			Gw_GPIO_Set_ON(5);
			if(GPIO_ReadInputDataBit(GPIOB, GET_GPIO_PORTB_5) == 0)
			{
				printf("Port_5 done!");
			}
			else
			{
				printf("Port_5 error!");
			}
			Gw_GPIO_Set_OFF(5);
			vrui_flag_menu = 1;
		}
		if(strstr(RRX,"6")!= NULL)
		{
			Gw_GPIO_Set_ON(6);
			if(GPIO_ReadInputDataBit(GPIOB, GET_GPIO_PORTB_6) == 0)
			{
				printf("Port_6 done!");
			}
			else
			{
				printf("Port_6 error!");
			}
			Gw_GPIO_Set_OFF(6);
			vrui_flag_menu = 1;
		}
//		fn_usart_Putchar("<->");
//		fn_usart_Putchar(RRX);
		printf("\n--------------- End! ------------ \n");
		RX_FLAG_END_LINE = 0;
	}
}
