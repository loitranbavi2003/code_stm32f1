#include "delay.h"


static uint32_t i=0;

void DELAY_INIT(void)
{
	SystemCoreClockUpdate();
	SysTick_Config(SystemCoreClock/1000);
}

void SysTick_Handler(void)
{
	i--;
}

void delay_ms(uint32_t time)
{
	i = time;
	while(i != 0){}
}
