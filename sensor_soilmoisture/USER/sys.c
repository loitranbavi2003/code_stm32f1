#include "sys.h"


static uint16_t count_time = 0;
static long adc = 0, soil_moisture = 0;

void SYS_Init(void)
{
	GPIO_INIT();
	DELAY_INIT();
	UART_INIT();
	ADC_Config();
}

void SYS_Run(void)
{	
	if(count_time < 1000)
	{
		count_time++;
		adc += ADC_Read();
	}
	else
	{
		adc /= 1000;
		count_time = 0;
		soil_moisture = map(adc, 0, 4095, 100, 0);
		printf("%ld\n", soil_moisture);
	}
	
	delay_ms(1);
}


long map(long x, long in_min, long in_max, long out_min, long out_max) 
{
    const long run = in_max - in_min;
    if(run == 0)
	{
        printf("map(): Invalid input range, min == max\n");
        return -1; // AVR returns -1, SAM returns 0
    }
	
    const long rise = out_max - out_min;
    const long delta = x - in_min;
	
    return (delta * rise) / run + out_min;
}
