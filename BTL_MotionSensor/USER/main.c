#include "servo.h"
#include "stdio.h"
#include "uart_lib.h"
#include "gpio_lib.h"
#include "stm32f10x.h"
#include "stm32f10x_rcc.h"  
#include "stm32f10x_gpio.h"


int main(void)
{
	Gpio_Init();
	Servo_Init(0);	
	uint8_t flag_servo = 0;
	
	while(1)
	{		
		if(GPIO_ReadInputDataBit(GPIOA, MOTION_SENSOR_PORT) == 1)
		{
			flag_servo = 1;
			GPIO_SetBits(GPIOA, SIRENS_PORT);
			GPIO_SetBits(GPIOA, LED_PORT);
		}
		else
		{
			flag_servo = 0;
			GPIO_ResetBits(GPIOA, SIRENS_PORT);
			GPIO_ResetBits(GPIOA, LED_PORT);
		}
		
		if(timer4_ticks >= 5)
		{
			timer4_ticks = 0;
			
			if(flag_servo == 1)
			{
				Servo_SetDegree(0);
			}
			else
			{
				Servo_SetDegree(90);
			}
		}
	}
}



