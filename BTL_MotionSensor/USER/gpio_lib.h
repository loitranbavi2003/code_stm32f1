#ifndef	_GPIO_
#define	_GPIO_

#ifdef __cplusplus
	extern "C"{
#endif
  
#include "stm32f10x_gpio.h" 

#define USART1_TX			GPIO_Pin_9				// Transmit:	TX
#define USART1_RX			GPIO_Pin_10				// Receive:		RX
#define USART1_PORT			GPIOA					// Port:		A

#define SERVO_PWM_PIN		GPIO_Pin_9				// PWM Pin
#define SERVO_PWM_PORT		GPIOB					// Port:		B

#define MOTION_SENSOR_PORT	GPIO_Pin_5
#define SIRENS_PORT			GPIO_Pin_6
#define LED_PORT			GPIO_Pin_7

void Gpio_Init(void);

#ifdef __cplusplus
}
#endif

#endif



/***********************************************/
