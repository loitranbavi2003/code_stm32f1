#include "delay.h"


int nTick = 0;

void Delay_Init(void)
{
	SystemCoreClockUpdate();
	SysTick_Config(SystemCoreClock/1000);
}

void SysTick_Handler(void)
{
	nTick--;
}

void delay_ms(int time)
{
	nTick = time;
	while(nTick != 0){}
}
