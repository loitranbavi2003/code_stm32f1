#ifndef _ADC_H_
#define _ADC_H_

#include "sys.h"
#include "stdint.h"

void ADC_Config(void);
int ADC_Read(void);

#endif
