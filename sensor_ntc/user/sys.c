#include "sys.h"

static int count = 0;
static double Vout = 0;
static double Rntc = 0;
static double R_10k = 9840; 
static double B_param = 24000; 
static double T0 = 298.15; 
static double Temp_K = 0; 
static double Temp_C = 0; 


void SYS_Init(void)
{
	ADC_Config();
	GPIO_Config();
	Delay_Init();
	UART_Init(9600);
}

void SYS_Run(void)
{
	if(count < 100)
	{
		Vout += ADC_Read();
		count++;
	}
	else
	{
		
		Vout /= 100;
		
		Vout = (Vout*3.3)/4095;
		Rntc = (Vout*R_10k)/(3.3 - Vout);
		Temp_K = (T0*B_param)/(T0*log(Rntc/R_10k) + B_param);
		Temp_C = Temp_K - 273.15;
		
		printf("Temp_C :%.2f\n", Temp_C);
		count = 0;
	}
	//delay_ms(10);
}
