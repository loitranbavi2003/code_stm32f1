#ifndef _UART_H_
#define _UART_H_


#include "sys.h"


#define TX GPIO_Pin_9
#define RX GPIO_Pin_10

#define string_size 80

extern char RX_FLAG_END_LINE;
extern char RRX[string_size];

void UART_Init(int BaudRates);

void fn_usart_SendChar(char _ch);
void fn_usart_SendString(char *_str);
void fn_usart_SendNumber(int _number);
void fn_usart_Putchar(char *String);

#endif
