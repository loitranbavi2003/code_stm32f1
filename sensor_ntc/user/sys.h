#ifndef _SYS_H_
#define _SYS_H_

#include "adc.h"
#include "uart.h"
#include "gpio.h"
#include "delay.h"
#include "stdio.h"
#include "stdint.h"
#include "string.h"
#include "stm32f10x.h"
#include "stm32f10x_adc.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_usart.h"

void SYS_Init(void);
void SYS_Run(void);

#endif
