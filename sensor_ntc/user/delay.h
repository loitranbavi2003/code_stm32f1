#ifndef _DELAY_H_
#define _DELAY_H_

#include "sys.h"

void Delay_Init(void);
void SysTick_Handler(void);
void delay_ms(int time);

#endif
