#include "gpio.h"


void GPIO_Config(void)
{
	GPIO_InitTypeDef  Gpio;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	
	/* cau hinh chan input cua bo ADC1 la chan PA0 */
	Gpio.GPIO_Mode = GPIO_Mode_AIN;
	Gpio.GPIO_Pin = GPIO_Pin_0;
	Gpio.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &Gpio);
}
