#ifndef _DELAY_H_
#define _DELAY_H_

#include "sys.h"

void DELAY_INIT(void);
void SysTick_Handler(void);
void delay_ms(uint32_t time);

#endif
