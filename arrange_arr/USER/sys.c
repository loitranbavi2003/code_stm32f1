#include "sys.h"
#include <stdlib.h> // Th�m thu vi?n d? s? d?ng h�m atoi

void SYS_Init(void)
{
	GPIO_INIT();
	DELAY_INIT();
	UART_INIT();
}

void SYS_Run(void)
{
	int arr[200] = {0};
	
	if(RX_FLAG_END_LINE == 1)
	{
		RX_FLAG_END_LINE = 0;
		
		// Copy mang RRX vao mang arr
		char *token = strtok(RRX, " ");
		int index = 0;
		
		while (token != NULL) 
		{
			arr[index++] = atoi(token);
			token = strtok(NULL, " ");
		}
		// Do dai cua mang RRX
		printf("\nArr Length: %d\n", index);
		
		// Sap xep mang tang dan
		for(int i = 0; i < index - 1; i++){
			for(int j = i + 1; j < index; j++){
				if(arr[i] > arr[j]){
					int temp = arr[i];
					arr[i] = arr[j];
					arr[j] = temp;        
				}
			}
		}
		
		// Hien thi mang ra man hinh
		for(int i = 0; i < index; i++)
		{
			printf("%d ", arr[i]);
			delay_ms(10);
		}
		printf("\n");
	}
}
