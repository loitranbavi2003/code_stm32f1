#ifndef _USART_H_
#define _USART_H_

void Usart_Init(void);		   // ham khoi tao toc do truyen
void Usart_SendChar(char _ch); // ham truyen mot ky tu
void Usart_SendString(char *_str); // ham truyen 1 chuoi ky tu
void Usart_SendNumber(int _number); // ham truyen 1 so

#endif
