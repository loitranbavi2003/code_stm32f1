#ifndef _DELAY_H_
#define _DELAY_H_

#include "stm32f10x_tim.h"

void Timer_Init(void);
void delay_1ms(void);
void delay_ms(unsigned int time);

#endif
