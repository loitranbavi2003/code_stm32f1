// #ifdef TEST

#include "unity.h"
#include "mock_button.h"
#include "button.h"


/* 
** This test case will pass. Because we have already told that
** We are expecting argument as 10 and it should return 1.
** In our Test_gpio function we are passing 10 as arg.
** So this test will pass.
*/
void test_test_gpio_0(void)
{
  gpio_read_ExpectAndReturn(10, 1);

  Test_gpio();
}

/* 
** This test case will fail. Because we have already told that
** We are expecting argument as 12 and it should return 1.
** In our Test_gpio function we are passing 10 as arg.
** So this test will fail.
*/
void test_test_gpio_1(void)
{
  gpio_read_ExpectAndReturn(12, 1);

  Test_gpio();
}

void test_button_NeedToImplement(void)
{
    TEST_ASSERT_EQUAL_INT8();
}

// #endif // TEST
