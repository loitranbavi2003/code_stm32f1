#include "sys.h"


void SYS_Init(void)
{
	LED_INIT();
	GPIO_INIT();
	DELAY_INIT();
	UART_INIT();
	SPI2_INIT();
	LCD_INIT();
	TOUCH_INIT();
	BUTTON_INIT();
	UI_Frame();
}

void SYS_Run(void)
{
	u8 player1[10] = "BIEN", player2[10] = "CUONG";
	UI_RoundNumber(1);
	UI_NamePlayer(1, player1, player2);
	printf("Hello world\n");
	for(uint8_t i = 0; i <= 9; i++)
	{
		UI_Points1(i);
		UI_Points2(i);
		delay_ms(1000);
	}
}
