#ifndef _UI_H_
#define _UI_H_

#include "sys.h"

#define ROUND_ONE 	1
#define	ROUND_TWO 	2
#define	FINAL		3

void UI_Frame(void);
void UI_RoundNumber(uint8_t number);
void UI_Points1(uint8_t point);
void UI_Points2(uint8_t point);
void UI_NamePlayer(u8 mode, u8 *player1, u8 *player2);

#endif
