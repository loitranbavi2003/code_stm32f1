#ifndef _SPI_H_
#define _SPI_H_

#include "sys.h"


void SPI1_INIT(void);
void SPI2_INIT(void);
uint8_t SPI_WriteByte(SPI_TypeDef* SPIx, uint8_t byte);

/*
	VCC 
	GND 
	CS - A4 
	RESET - A2 
	DC/RS - A1 
	SDI (MOSI) - A7
	SCK - A5 
	LED - 3,3 V 
	SDO (MISO) - A6
	
	T_CLK - A5
	T_CS - A4
	T_DIN - A7
	T_DO - A6
	T_IRQ - pas connect�e
*/

#endif
