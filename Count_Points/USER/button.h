#ifndef _BUTTON_H_
#define _BUTTON_H_

#include "sys.h"

#define PIN_BUTTON_1 GPIO_Pin_0
#define PORT_BUTTON_1 GPIOA
#define PIN_BUTTON_2 GPIO_Pin_1
#define PORT_BUTTON_2 GPIOA
#define PIN_BUTTON_RESET GPIO_Pin_2
#define PORT_BUTTON_RESET GPIOA

void BUTTON_INIT(void);
uint8_t Button_CountPoints(void);

#endif
