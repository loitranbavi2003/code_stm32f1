#include "ui.h"



void UI_Frame(void)
{
	LCD_DrawLine(0, 60, 320, 60);
	LCD_DrawLine(160, 60, 160, 240);
	LCD_Fill(0, 0, 320, 59, WHITE);
	LCD_Fill(0, 61, 159, 240, GREEN);
	LCD_Fill(161, 61, 320, 240, YELLOW);
	gui_circle(30, 30, RED, 25, 1);
	LCD_ShowString(10, 22, BLACK, RED, 16, "Reset", 1);
	gui_circle(290, 30, RED, 25, 1);
	LCD_ShowString(274, 22, BLACK, RED, 16, "Menu", 1);
}

void UI_RoundNumber(uint8_t number)
{
	switch(number)
	{
		case ROUND_ONE:
			LCD_ShowString(93, 17, RED, WHITE, 24, "- ROUND 1 -", 0);
		break;
		
		case ROUND_TWO:
			LCD_ShowString(93, 17, RED, WHITE, 24, "- ROUND 2 -", 0);
		
		break;
		
		case FINAL:
			LCD_ShowString(83, 17, RED, WHITE, 24, "- CHUNG KET -", 0);
		break;
	}
}

void UI_NamePlayer(u8 mode, u8 *player1, u8 *player2)
{
	switch(mode%2)
	{
		case 1:
			LCD_ShowString(55, 70, BLACK, GREEN, 24, player1, 0);
			LCD_ShowString(210, 70, BLACK, YELLOW, 24, player2, 0);
		break;
		
		case 0:
			LCD_ShowString(55, 70, BLACK, GREEN, 24, player2, 0);
			LCD_ShowString(210, 70, BLACK, YELLOW, 24, player1, 0);
		break;
	}
}

void UI_Points1(uint8_t point)
{
	switch(point)
	{
		case 0:
			LCD_Show_24x48_char(70, 120, GREEN, GREEN, '9', 1);
			LCD_Show_24x48_char(70, 120, RED, GREEN, '0', 1);
		break;
		
		case 1:
			LCD_Show_24x48_char(70, 120, GREEN, GREEN, '0', 1);
			LCD_Show_24x48_char(70, 120, RED, GREEN, '1', 1);
		break;
		
		case 2:
			LCD_Show_24x48_char(70, 120, GREEN, GREEN, '1', 1);
			LCD_Show_24x48_char(70, 120, RED, GREEN, '2', 1);
		break;
		
		case 3:
			LCD_Show_24x48_char(70, 120, GREEN, GREEN, '2', 1);
			LCD_Show_24x48_char(70, 120, RED, GREEN, '3', 1);
		break;
		
		case 4:
			LCD_Show_24x48_char(70, 120, GREEN, GREEN, '3', 1);
			LCD_Show_24x48_char(70, 120, RED, GREEN, '4', 1);
		break;
		
		case 5:
			LCD_Show_24x48_char(70, 120, GREEN, GREEN, '4', 1);
			LCD_Show_24x48_char(70, 120, RED, GREEN, '5', 1);
		break;
		
		case 6:
			LCD_Show_24x48_char(70, 120, GREEN, GREEN, '5', 1);
			LCD_Show_24x48_char(70, 120, RED, GREEN, '6', 1);
		break;
		
		case 7:
			LCD_Show_24x48_char(70, 120, GREEN, GREEN, '6', 1);
			LCD_Show_24x48_char(70, 120, RED, GREEN, '7', 1);
		break;
		
		case 8:
			LCD_Show_24x48_char(70, 120, GREEN, GREEN, '7', 1);
			LCD_Show_24x48_char(70, 120, RED, GREEN, '8', 1);
		break;
		
		case 9:
			LCD_Show_24x48_char(70, 120, GREEN, GREEN, '8', 1);
			LCD_Show_24x48_char(70, 120, RED, GREEN, '9', 1);
		break;
	}
}

void UI_Points2(uint8_t point)
{
	switch(point)
	{
		case 0:
			LCD_Show_24x48_char(225, 120, YELLOW, YELLOW, '9', 1);
			LCD_Show_24x48_char(225, 120, RED, YELLOW, '0', 1); 
		break;
		
		case 1:
			LCD_Show_24x48_char(225, 120, YELLOW, YELLOW, '0', 1);
			LCD_Show_24x48_char(225, 120, RED, YELLOW, '1', 1); 
		break;
		
		case 2:
			LCD_Show_24x48_char(225, 120, YELLOW, YELLOW, '1', 1);
			LCD_Show_24x48_char(225, 120, RED, YELLOW, '2', 1); 
		break;
		
		case 3:
			LCD_Show_24x48_char(225, 120, YELLOW, YELLOW, '2', 1);
			LCD_Show_24x48_char(225, 120, RED, YELLOW, '3', 1); 
		break;
		
		case 4:
			LCD_Show_24x48_char(225, 120, YELLOW, YELLOW, '3', 1);
			LCD_Show_24x48_char(225, 120, RED, YELLOW, '4', 1); 
		break;
		
		case 5:
			LCD_Show_24x48_char(225, 120, YELLOW, YELLOW, '4', 1);
			LCD_Show_24x48_char(225, 120, RED, YELLOW, '5', 1); 
		break;
		
		case 6:
			LCD_Show_24x48_char(225, 120, YELLOW, YELLOW, '5', 1);
			LCD_Show_24x48_char(225, 120, RED, YELLOW, '6', 1); 
		break;
		
		case 7:
			LCD_Show_24x48_char(225, 120, YELLOW, YELLOW, '6', 1);
			LCD_Show_24x48_char(225, 120, RED, YELLOW, '7', 1); 
		break;
		
		case 8:
			LCD_Show_24x48_char(225, 120, YELLOW, YELLOW, '7', 1);
			LCD_Show_24x48_char(225, 120, RED, YELLOW, '8', 1); 
		break;
		
		case 9:
			LCD_Show_24x48_char(225, 120, YELLOW, YELLOW, '8', 1);
			LCD_Show_24x48_char(225, 120, RED, YELLOW, '9', 1); 
		break;
	}
}
