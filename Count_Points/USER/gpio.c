#include "gpio.h"


void LED_INIT(void)
{
	GPIO_InitTypeDef Led;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	GPIO_PinRemapConfig(GPIO_Remap_SWJ_Disable, ENABLE);
	Led.GPIO_Mode = GPIO_Mode_Out_PP;
	Led.GPIO_Pin = PIN_LED_RED | PIN_LED_GREEN;
	Led.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &Led);
	
	GPIO_SetBits(PORT_LED_RED, PIN_LED_RED);
	GPIO_SetBits(PORT_LED_GREEN, PIN_LED_GREEN);
}


void GPIO_INIT(void)
{
	GPIO_InitTypeDef GPIOInit;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	
	/* PB8 - DC/RS */
	GPIOInit.GPIO_Mode	= GPIO_Mode_Out_PP;
	GPIOInit.GPIO_Pin 	= GPIO_Pin_8;
	GPIOInit.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOB, &GPIOInit);
	
	/* PB9 - CS */
	GPIOInit.GPIO_Mode 	= GPIO_Mode_Out_PP;
	GPIOInit.GPIO_Pin 	= GPIO_Pin_9;
	GPIOInit.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOB, &GPIOInit);
	
	/* PB12 - Reset */
	GPIOInit.GPIO_Mode 	= GPIO_Mode_Out_PP;
	GPIOInit.GPIO_Pin 	= GPIO_Pin_12;
	GPIOInit.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOB, &GPIOInit);
}

void LED_RedOn(void)
{
	GPIO_ResetBits(PORT_LED_RED, PIN_LED_RED);
	GPIO_SetBits(PORT_LED_GREEN, PIN_LED_GREEN);
}

void LED_GreenON(void)
{
	GPIO_SetBits(PORT_LED_RED, PIN_LED_RED);
	GPIO_ResetBits(PORT_LED_GREEN, PIN_LED_GREEN);
}
