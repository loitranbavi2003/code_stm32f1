#ifndef _GPIO_H_
#define _GPIO_H_

#include "sys.h"

#define PIN_LED_RED GPIO_Pin_3
#define PORT_LED_RED GPIOA
#define PIN_LED_GREEN GPIO_Pin_4
#define PORT_LED_GREEN GPIOA


void LED_INIT(void);
void GPIO_INIT(void);

void LED_RedOn(void);
void LED_GreenON(void);

#endif
