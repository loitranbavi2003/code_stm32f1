#include "button.h"


void BUTTON_INIT(void)
{
	GPIO_InitTypeDef Button;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	GPIO_PinRemapConfig(GPIO_Remap_SWJ_Disable, ENABLE);
	Button.GPIO_Mode = GPIO_Mode_IPU;
	Button.GPIO_Pin = PIN_BUTTON_1 | PIN_BUTTON_2 | PIN_BUTTON_RESET;
	Button.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &Button);
}

uint8_t Button_CountPoints(void)
{
	uint8_t t = 0;
	if(GPIO_ReadInputDataBit(PORT_BUTTON_1, PIN_BUTTON_1) == 1)
	{
		t = 1;
	}
	return t;
}
