#include "stm32f10x.h"
#include "delay.h"

unsigned int nTicks = 0;
void Delay_Init(void){
	SystemCoreClockUpdate();
	SysTick_Config(SystemCoreClock/1000);
}

void SysTick_Handler(void){
	nTicks--;
}

void delay_ms(unsigned int time){
	nTicks = time;
	while(nTicks != 0);
}
