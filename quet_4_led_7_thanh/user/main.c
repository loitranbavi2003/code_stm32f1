#include <stm32f10x.h>
#include "delay.h"

void delay_ms(uint16_t time);
void led4_Init(void);
void reset(void);

int main(){
	led4_Init();
	uint16_t led7[11] = {0xC0, 0xF9, 0xA4, 0xB0, 
						 0x99, 0x92, 0x82, 0xF8, 
						 0x80, 0x90, 0xFF};	
	while(1){
		uint16_t i;
		for(i = 0;i < 9999; i++)
		{
			int stt=250;
			while(stt--)
			{
				reset();
				GPIO_Write(GPIOA, led7[i/1000]);
				GPIO_SetBits(GPIOA, GPIO_Pin_8);
				delay_ms(1);

				reset();
				GPIO_Write(GPIOA, led7[(i%1000)/100]);
				GPIO_SetBits(GPIOA, GPIO_Pin_9);
				delay_ms(1);

				reset();
				GPIO_Write(GPIOA, led7[(i%100)/10]);
				GPIO_SetBits(GPIOA, GPIO_Pin_10);
				delay_ms(1);
				
				reset();
				GPIO_Write(GPIOA, led7[i%10]);
				GPIO_SetBits(GPIOA, GPIO_Pin_11);
				delay_ms(1);		
			}			
		}
	}
}

void delay_ms(uint16_t time){
	uint16_t i;
	while(time--){
		for(i = 0; i < 1998; i++);
	}
}
void led4_Init(void){
	GPIO_InitTypeDef led;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO,ENABLE);
	led.GPIO_Mode = GPIO_Mode_Out_PP;
	led.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_4 | 
								 GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10 | GPIO_Pin_11;
	led.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &led);
}

void reset(void){
	GPIO_ResetBits(GPIOA, GPIO_Pin_8);
	GPIO_ResetBits(GPIOA, GPIO_Pin_9);
	GPIO_ResetBits(GPIOA, GPIO_Pin_10);
	GPIO_ResetBits(GPIOA, GPIO_Pin_11);
}
