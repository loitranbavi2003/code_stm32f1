#include "uart.h"


char RX_FLAG_END_LINE = 0;
char RRX[string_size];
uint8_t RXI = 0;
char temp_char;
char a[10];


void USART1_Receive(char *arr_out)
{
	if(USART_GetITStatus(USART1,USART_IT_RXNE) != RESET)
	{
		temp_char = USART_ReceiveData(USART1);
		// printf("temp_char: %c\n", temp_char);
		if(temp_char != '\n')
		{	
			arr_out[RXI] = temp_char;
			RXI++;
			// sprintf(a,"%d",RXI);
		}
		else
		{
			arr_out[RXI] = 0x00;
			RX_FLAG_END_LINE=1;
			RXI = 0;
		}
	}
	USART_ClearITPendingBit(USART1, USART_IT_RXNE);
}

