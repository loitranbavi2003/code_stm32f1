#ifndef MESSAGE_H
#define MESSAGE_H

#include "stdint.h"

#define START_BYTE 0xAA55
#define LENGTH_DEFAULT 3 /*StartFrame 2 + TypeMessage 1*/

#define TYPE_MESSAGE_SIZE 4 // Có bao nhiêu loại bản tin
/* TypeMessage */
typedef enum
{
    NAME_PLAYER_1 = 1,
    NAME_PLAYER_2,
    ROUND3x3,
    ROUND5x5,
} type_message_e;

/* Bản tin cập nhật */
typedef struct
{
    uint16_t StartFrame;
    uint8_t  TypeMessage;
    uint16_t LengthData; 
    uint8_t  Data[17];
    uint16_t CheckFrame;
} FrameMsg_t;


/*
    @brief: Hàm tạo Frame data chuẩn.
    @node: Chức năng của hàm là chuyển data từ dạng struct về dạng arr.
    @param datain: Là một struct chưa các thông tin được người dùng chỉ định để tạo bản tin.
    @param dataout: Là con trỏ dùng để lấy mảng sao khi tạo thành công.
    @retval: Trả về 0 là lỗi, Lớn hơn 0 là thành công.
*/
uint8_t SEC_Message_Create_Frame(FrameMsg_t DataIn, uint8_t *DataOut);

/*
    @brief: Hàm tách Frame data.
    @node: Chức năng của hàm là chuyển data từ dạng arr về dạng struct.
    @param datain: Là một mảng chứa data.
    @param dataout: Là một struct chứa data đầu ra.
    @retval: trả về 1 thành công, trả về 0 là lỗi.
*/
uint8_t SEC_Message_Detect_Frame(uint8_t *DataIn, FrameMsg_t *DataOut);

#endif // MESSAGE_H
