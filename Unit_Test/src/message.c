#include "message.h"
#include "convert.h"


static uint8_t lenght_data_arr[TYPE_MESSAGE_SIZE] = {};
static type_message_e type_message[TYPE_MESSAGE_SIZE] = {NAME_PLAYER_1, NAME_PLAYER_2, ROUND3x3, ROUND5x5};

uint16_t CheckSum(uint8_t *buf, uint8_t len);


uint8_t SEC_Message_Create_Frame(FrameMsg_t DataIn, uint8_t *DataOut)
{

}

uint8_t SEC_Message_Detect_Frame(uint8_t *DataIn, FrameMsg_t *DataOut)
{

}


/*-------------------------------------------------------------------------------------------------------*/
uint16_t CheckSum(uint8_t *buf, uint8_t len)
{
    uint16_t crc = 0xFFFF, pos = 0, i = 0;
    for (pos = 0; pos < len; pos++)
    {
        crc ^= (uint16_t)buf[pos]; // XOR byte into least sig. byte of crc
        for (i = 8; i != 0; i--)   // Loop over each bit
        {
            if ((crc & 0x0001) != 0) // If the LSB is set
            {
                crc >>= 1; // Shift right and XOR 0xA001
                crc ^= 0xA001;
            }
            else // Else LSB is not set
            {
                crc >>= 1; // Just shift right
            }
        }
    }
    return crc;
}
