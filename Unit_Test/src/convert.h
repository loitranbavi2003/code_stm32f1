#ifndef CONVERT_H
#define CONVERT_H

#include "stdio.h"
#include "stdint.h"
#include <stdlib.h>
#include "string.h"

void Convert_StringToHex(char *arr_in, char *arr_out);
void Convert_HexToString(char *arr_in, char *arr_out);

#endif // CONVERT_H
