#include "gpio.h"

uint8_t GPIO_ReadPin(void)
{
    if(GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_0) == 0)
    {
        return 0;
    } 
    else
    {
        return 1;
    }
}