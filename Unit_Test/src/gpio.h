#ifndef __GPIO_LIB_H__
#define __GPIO_LIB_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f10x_gpio.h"
uint8_t GPIO_ReadPin(void);

#ifdef __cplusplus
} 
#endif

#endif