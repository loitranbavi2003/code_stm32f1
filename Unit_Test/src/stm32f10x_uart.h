#ifndef __STM32F1_GPIO_H__
#define __STM32F1_GPIO_H__

#include "unity.h"

#define     __IO    volatile

#define PERIPH_BASE         ((uint32_t)0x40000000)
#define APB2PERIPH_BASE     (PERIPH_BASE + 0x10000)
#define USART_IT_RXNE       ((uint16_t)0x0525)
#define USART1_BASE         (APB2PERIPH_BASE + 0x3800)
#define USART1              ((USART_TypeDef *) USART1_BASE)


typedef enum {RESET = 0, SET = !RESET} FlagStatus, ITStatus;

typedef struct
{
  __IO uint16_t SR;
  uint16_t  RESERVED0;
  __IO uint16_t DR;
  uint16_t  RESERVED1;
  __IO uint16_t BRR;
  uint16_t  RESERVED2;
  __IO uint16_t CR1;
  uint16_t  RESERVED3;
  __IO uint16_t CR2;
  uint16_t  RESERVED4;
  __IO uint16_t CR3;
  uint16_t  RESERVED5;
  __IO uint16_t GTPR;
  uint16_t  RESERVED6;
} USART_TypeDef;

uint16_t USART_ReceiveData(USART_TypeDef* USARTx);
void USART_ClearITPendingBit(USART_TypeDef* USARTx, uint16_t USART_IT);
ITStatus USART_GetITStatus(USART_TypeDef* USARTx, uint16_t USART_IT);

#endif
