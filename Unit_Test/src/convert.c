#include "convert.h"


void Convert_StringToHex(char *arr_in, char *arr_out)
{
    for (int i = 0; arr_in[i] != '\0'; i++) 
    {
        sprintf(arr_out + (i * 4), "0x%02X ", (unsigned char)arr_in[i]);
    }
}

void Convert_HexToString(char *arr_in, char *arr_out)
{
    const char* delimiter = ", ";
    char* token;

    char* hex_copy = strdup(arr_in);  // Tạo một bản sao của chuỗi mã hex

    token = strtok(hex_copy, delimiter);
    while (token != NULL) 
    {
        if (strncmp(token, "0x", 2) == 0) 
        {
            int hex_value = strtol(token + 2, NULL, 16);
            *arr_out = (char)hex_value;
            arr_out++;
        }
        token = strtok(NULL, delimiter);
    }
    *arr_out = '\0';

    free(hex_copy);  // Giải phóng bộ nhớ của bản sao chuỗi mã hex
}
