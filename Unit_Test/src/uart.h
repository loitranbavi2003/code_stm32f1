#ifndef __UART_LIB_H__
#define __UART_LIB_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f10x_uart.h"
#include "stdio.h"

#define string_size 80

void USART1_Receive(char *arr_out);

#ifdef __cplusplus
} 
#endif

#endif