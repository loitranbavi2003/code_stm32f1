#include "/var/lib/gems/3.0.0/gems/ceedling-0.31.1/vendor/unity/src/unity.h"


















typedef enum

{ Bit_RESET = 0,

Bit_SET

}BitAction;



typedef struct

{

volatile uint32_t CRL;

volatile uint32_t CRH;

volatile uint32_t IDR;

volatile uint32_t ODR;

volatile uint32_t BSRR;

volatile uint32_t BRR;

volatile uint32_t LCKR;

} GPIO_TypeDef;

uint8_t GPIO_ReadInputDataBit(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin);

uint16_t GPIO_ReadInputData(GPIO_TypeDef* GPIOx);

void GPIO_SetBits(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin);

void GPIO_ResetBits(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin);

void GPIO_WriteBit(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin, BitAction BitVal);

void GPIO_Write(GPIO_TypeDef* GPIOx, uint16_t PortVal);
