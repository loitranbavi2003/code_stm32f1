#include "src/uart.h"
#include "build/test/mocks/mock_stm32f10x_uart.h"
#include "/var/lib/gems/3.0.0/gems/ceedling-0.31.1/vendor/unity/src/unity.h"






void setUp(void)

{ }



void tearDown(void)

{ }



void test_readgpio(void)

{

    char arr[100];



    USART_GetITStatus_CMockIgnoreAndReturn(17, SET);

    USART_ReceiveData_CMockExpectAndReturn(18, ((USART_TypeDef *) ((((uint32_t)0x40000000) + 0x10000) + 0x3800)), 'h');

    USART_ClearITPendingBit_CMockIgnore();

    USART1_Receive(arr);

    USART_GetITStatus_CMockIgnoreAndReturn(21, SET);

    USART_ReceiveData_CMockExpectAndReturn(22, ((USART_TypeDef *) ((((uint32_t)0x40000000) + 0x10000) + 0x3800)), 'e');

    USART_ClearITPendingBit_CMockIgnore();

    USART1_Receive(arr);



    printf("Arr Receive: %s", arr);

}
