#include "/var/lib/gems/3.0.0/gems/ceedling-0.31.1/vendor/unity/src/unity.h"






typedef enum {RESET = 0, SET = !RESET} FlagStatus, ITStatus;



typedef struct

{

  volatile uint16_t SR;

  uint16_t RESERVED0;

  volatile uint16_t DR;

  uint16_t RESERVED1;

  volatile uint16_t BRR;

  uint16_t RESERVED2;

  volatile uint16_t CR1;

  uint16_t RESERVED3;

  volatile uint16_t CR2;

  uint16_t RESERVED4;

  volatile uint16_t CR3;

  uint16_t RESERVED5;

  volatile uint16_t GTPR;

  uint16_t RESERVED6;

} USART_TypeDef;



uint16_t USART_ReceiveData(USART_TypeDef* USARTx);

void USART_ClearITPendingBit(USART_TypeDef* USARTx, uint16_t USART_IT);

ITStatus USART_GetITStatus(USART_TypeDef* USARTx, uint16_t USART_IT);
