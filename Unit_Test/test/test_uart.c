// #ifdef TEST

#include "unity.h"
#include "mock_stm32f10x_uart.h"
#include "uart.h"

void setUp(void)
{ } 

void tearDown(void)
{ }

void test_readgpio(void)
{
    char arr[100];

    USART_GetITStatus_IgnoreAndReturn(SET);
    USART_ReceiveData_ExpectAndReturn(USART1, 'h');
    USART_ClearITPendingBit_Ignore();
    USART1_Receive(arr);
    USART_GetITStatus_IgnoreAndReturn(SET);
    USART_ReceiveData_ExpectAndReturn(USART1, 'e');
    USART_ClearITPendingBit_Ignore();
    USART1_Receive(arr);

    printf("Arr Receive: %s", arr);
}

// #endif // TEST