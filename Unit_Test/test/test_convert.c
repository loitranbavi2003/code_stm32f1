// #ifdef TEST

#include "unity.h"
#include "convert.h"


void test_Convert_HexToString(void)
{
    char hex_string[] = "0x48, 0x65, 0x6C, 0x6C, 0x6F";
    char char_result[50];
    char string_test[] = "Hello";

    printf("Start_Test_Convert_HexToString\n");

    Convert_HexToString(hex_string, char_result);

    TEST_ASSERT_EQUAL_STRING(string_test, char_result);

    printf("End_Test_Convert_HexToString\n");
}

// #endif // TEST
