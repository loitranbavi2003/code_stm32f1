#ifdef TEST

#include "unity.h"
#include "mock_stm32f10x_gpio.h"
#include "gpio.h"

void setUp(void)
{ } 

void tearDown(void)
{ }

void test_readgpio(void)
{
    /*Hàm này giả lập hàm đọc nút nhấn*/
    GPIO_ReadInputDataBit_IgnoreAndReturn(0);
    printf("GPIO_ReadPin: %d \n", GPIO_ReadPin());
}

#endif // TEST