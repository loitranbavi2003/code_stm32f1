#ifndef __OLED_H
#define __OLED_H			  	 
#include "sys.h"
#include "stdlib.h"	    
//////////////////////////////////////////////////////////////////////////////////	 
//Edit by HTpro DevTeam
//info@htpro.vn
// v2.0 03/08/2014
//////////////////////////////////////////////////////////////////////////////////	
   						  
//----------------- OLED PIN Define----------------  					   

#define OLED_RST_Clr() GPIO_ResetBits(GPIOB,GPIO_Pin_5) //rST -- B5
#define OLED_RST_Set() GPIO_SetBits(GPIOB,GPIO_Pin_5)

#define OLED_RS_Clr() GPIO_ResetBits(GPIOB,GPIO_Pin_6)    //D/C  ---B6
#define OLED_RS_Set() GPIO_SetBits(GPIOB,GPIO_Pin_6)

#define OLED_SCLK_Clr() GPIO_ResetBits(GPIOB,GPIO_Pin_3) //SClK  ---B3 (SCL)
#define OLED_SCLK_Set() GPIO_SetBits(GPIOB,GPIO_Pin_3)

#define OLED_SDIN_Clr() GPIO_ResetBits(GPIOB,GPIO_Pin_4)  //SDIN ----B4 (SDA)
#define OLED_SDIN_Set() GPIO_SetBits(GPIOB,GPIO_Pin_4)
//================================================================================
 		     
#define OLED_CMD  0	
#define OLED_DATA 1	
//OLED function
void OLED_WR_Byte(u8 dat,u8 cmd);	    
void OLED_Display_On(void);
void OLED_Display_Off(void);
void OLED_Refresh_Gram(void);		   
							   		    
void OLED_Init(void);
void OLED_Clear(void);
void OLED_DrawPoint(u8 x,u8 y,u8 t);
void OLED_Fill(u8 x1,u8 y1,u8 x2,u8 y2,u8 dot);
void OLED_ShowChar(u8 x,u8 y,u8 chr,u8 size,u8 mode);
void OLED_ShowNum(u8 x,u8 y,u32 num,u8 len,u8 size);
void OLED_ShowString1206(u8 x,u8 y,const u8 *p);	
void OLED_ShowString1608(u8 x,u8 y,const u8 *p);
#endif  
	 



