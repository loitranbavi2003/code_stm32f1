/****************************************Copyright (c)****************************************************
**                                      
**                                 http://www.powermcu.com
**
**--------------File Info---------------------------------------------------------------------------------
** File name:               RTC_Time.c
** Descriptions:            The RTC application function
**
**--------------------------------------------------------------------------------------------------------
** Created by:              AVRman
** Created date:            2010-10-30
** Version:                 v1.0
** Descriptions:            The original version
**
**--------------------------------------------------------------------------------------------------------
** Modified by: HTpro DevTeam             
** Modified date:  05-10-2016         
** Version:       3.01          
** Descriptions:   RTC Display         
**
*********************************************************************************************************/

/*******************************************************************************

*******************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "stm32f10x_rtc.h"
#include "stm32f10x_bkp.h"
#include "stm32f10x_pwr.h"
#include "RTC_Time.h" 
#include "oled.h"
/* Private define ------------------------------------------------------------*/
//#define RTCClockOutput_Enable  /* RTC Clock/64 is output on tamper pin(PC.13) */  

/* Private function prototypes -----------------------------------------------*/
void Time_Set(u32 t);

/*******************************************************************************
* Function Name  : Time_ConvUnixToCalendar
* Description    : Chuyen doi thoi gian dong bo ra lich ngay thang
* Input          : 
* Output         : None
* Return         : struct tm
* Attention		 : None
*******************************************************************************/
struct tm Time_ConvUnixToCalendar(time_t t)
{
	struct tm *t_tm;
	t_tm = localtime(&t);
	t_tm->tm_year += 1900;	/* */
	return *t_tm;
}

/*******************************************************************************
* Function Name  : Time_ConvCalendarToUnix
* Description    : 
* Input          : - t: struct tm
* Output         : None
* Return         : time_t
* Attention		 : None
*******************************************************************************/
time_t Time_ConvCalendarToUnix(struct tm t)
{
	t.tm_year -= 1900;  //1900 la moc thoi gian tinh Duong lich.
	return mktime(&t);
}


/*******************************************************************************
* Function Name  : Time_GetUnixTime
* Description    : 
* Input          : None
* Output         : None
* Return         : time_t
* Attention		 : None
*******************************************************************************/
time_t Time_GetUnixTime(void)
{
	return (time_t)RTC_GetCounter();
}

/*******************************************************************************
* Function Name  : Time_GetCalendarTime
* Description    : 
* Input          : None
* Output         : None
* Return         : struct tm
* Attention		 : None
*******************************************************************************/
struct tm Time_GetCalendarTime(void)
{
	time_t t_t;
	struct tm t_tm;

	t_t = (time_t)RTC_GetCounter();
	t_tm = Time_ConvUnixToCalendar(t_t);
	return t_tm;
}

/*******************************************************************************
* Function Name  : Time_SetUnixTime
* Description    : 
* Input          : - t: time_t 
* Output         : None
* Return         : None
* Attention		 : None
*******************************************************************************/
void Time_SetUnixTime(time_t t)
{
	RTC_WaitForLastTask();
	RTC_SetCounter((u32)t);
	RTC_WaitForLastTask();
	return;
}

/*******************************************************************************
* Function Name  : Time_SetCalendarTime
* Description    : 
* Input          : - t: struct tm
* Output         : None
* Return         : None
* Attention		 : None
*******************************************************************************/
void Time_SetCalendarTime(struct tm t)
{
	Time_SetUnixTime(Time_ConvCalendarToUnix(t));
	return;
}

/*******************************************************************************
* Function Name  : NVIC_Configuration
* Description    : Configures the nested vectored interrupt controller.
* Input          : None
* Output         : None
* Return         : None
* Attention		 : None
*******************************************************************************/


/*******************************************************************************
* Function Name  : NVIC_Configuration
* Description    : Configures the RTC.
* Input          : None
* Output         : None
* Return         : None
* Attention		 : None
*******************************************************************************/
static void RTC_Configuration(void)
{
  /* Enable PWR and BKP clocks */
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR | RCC_APB1Periph_BKP, ENABLE);

  /* Allow access to BKP Domain */
  PWR_BackupAccessCmd(ENABLE);

  /* Reset Backup Domain */
  BKP_DeInit();

  /* Enable LSE */
  RCC_LSEConfig(RCC_LSE_ON);
  /* Wait till LSE is ready */
  while (RCC_GetFlagStatus(RCC_FLAG_LSERDY) == RESET)
  {}

  /* Select LSE as RTC Clock Source */
  RCC_RTCCLKConfig(RCC_RTCCLKSource_LSE);

  /* Enable RTC Clock */
  RCC_RTCCLKCmd(ENABLE);

  /* Wait for RTC registers synchronization */
  RTC_WaitForSynchro();

  /* Wait until last write operation on RTC registers has finished */
  RTC_WaitForLastTask();

  /* Enable the RTC Second */
  RTC_ITConfig(RTC_IT_SEC, ENABLE);

  /* Wait until last write operation on RTC registers has finished */
  RTC_WaitForLastTask();

  /* Set RTC prescaler: set RTC period to 1sec */
  RTC_SetPrescaler(32767); /* RTC period = RTCCLK/RTC_PR = (32.768 KHz)/(32767+1) */

  /* Wait until last write operation on RTC registers has finished */
  RTC_WaitForLastTask();
}

/*******************************************************************************
* Function Name  : USART_Scanf
* Description    : USART Receive
* Input          : - min_value: 
*                  - max_value:
*                  - lenght:
* Output         : None
* Return         : uint8_t
* Attention		 : None
*******************************************************************************/


/*******************************************************************************
* Function Name  : Time_Regulate
* Description    : None
* Input          : None
* Output         : None
* Return         : None
* Attention		 : None
*******************************************************************************/
void Time_Regulate(void)
{
  struct tm time;

  memset(&time, 0 , sizeof(time) );

    time.tm_year = 2016; 
    time.tm_mon= 6;
    time.tm_mday = 12;
    time.tm_hour = 23;
    time.tm_min = 59; 
    time.tm_sec = 0;
  /* Return the value to store in RTC counter register */
  Time_SetCalendarTime(time);  
}

/*******************************************************************************
* Function Name  : RTC_Init
* Description    : RTC Initialization
* Input          : None
* Output         : None
* Return         : None
* Attention		 : None	
*******************************************************************************/
void RTC_Init(void)
{

  if (BKP_ReadBackupRegister(BKP_DR1) != 0xA5A5)
  {
    /* Backup data register value is not correct or not yet programmed (when
       the first time the program is executed) */

 //   printf("RTC not yet configured....\r\n");

    /* RTC Configuration */
    RTC_Configuration();

	Time_Regulate();

	/* Adjust time by values entred by the user on the hyperterminal */

   // printf("RTC configured....\r\n");

    BKP_WriteBackupRegister(BKP_DR1, 0xA5A5);
  }
  else
  {
    /* Check if the Power On Reset flag is set */
    if (RCC_GetFlagStatus(RCC_FLAG_PORRST) != RESET)
    {
   //   printf("Power On Reset occurred....\r\n");
    }
    /* Check if the Pin Reset flag is set */
    else if (RCC_GetFlagStatus(RCC_FLAG_PINRST) != RESET)
    {
   //   printf("External Reset occurred....\r\n");
    }

   // printf("No need to configure RTC....\r\n");
    /* Wait for RTC registers synchronization */
    RTC_WaitForSynchro();

    /* Enable the RTC Second */
    RTC_ITConfig(RTC_IT_SEC, ENABLE);
    /* Wait until last write operation on RTC registers has finished */
    RTC_WaitForLastTask();
  }

   /* NVIC configuration */
  // NVIC_Configuration();

#ifdef RTCClockOutput_Enable
  /* Enable PWR and BKP clocks */
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR | RCC_APB1Periph_BKP, ENABLE);

  /* Allow access to BKP Domain */
  PWR_BackupAccessCmd(ENABLE);

  /* Disable the Tamper Pin */
  BKP_TamperPinCmd(DISABLE); /* To output RTCCLK/64 on Tamper pin, the tamper
                                 functionality must be disabled */

  /* Enable RTC Clock Output on Tamper Pin */
  BKP_RTCOutputConfig(BKP_RTCOutputSource_CalibClock);
#endif

   /* Clear reset flags */
  RCC_ClearFlag();
  return;
}

void change_min(int min)
{
  struct tm tm_now;
  memset(&tm_now, 0 , sizeof(tm_now) );
  	 tm_now = Time_GetCalendarTime();	
 //tm_now.tm_sec = 0;   /* seconds after the minute, 0 to 60
 //                    (0 - 60 allows for the occasional leap second) */
 tm_now.tm_min = min;   /* minutes after the hour, 0 to 59 */
// tm_now.tm_hour =2;  /* hours since midnight, 0 to 23 */
// tm_now.tm_mday =2;  /* day of the month, 1 to 31 */
// tm_now.tm_mon = 2;   /* months since January, 0 to 11 */
// tm_now.tm_year = 2013;  /* years since 1900 */
 RTC_Configuration();
 Time_SetCalendarTime(tm_now);
   BKP_WriteBackupRegister(BKP_DR1, 0xA5A5);
}
void change_sec(int sec)
{
  struct tm tm_now;
  memset(&tm_now, 0 , sizeof(tm_now) );
  	 tm_now = Time_GetCalendarTime();	
 tm_now.tm_sec = sec;   
 
 RTC_Configuration();
 Time_SetCalendarTime(tm_now);
BKP_WriteBackupRegister(BKP_DR1, 0xA5A5);
}
void change_date(int date)
{
  struct tm tm_now;
  memset(&tm_now, 0 , sizeof(tm_now) );
  tm_now = Time_GetCalendarTime();	
 tm_now.tm_mday = date;  /* day of the month, 1 to 31 */
 RTC_Configuration();
 Time_SetCalendarTime(tm_now);
 BKP_WriteBackupRegister(BKP_DR1, 0xA5A5);
}
void change_month(int month)
{
  struct tm tm_now;
  memset(&tm_now, 0 , sizeof(tm_now) );
  tm_now = Time_GetCalendarTime();	
 tm_now.tm_mon = month;   /* months since January, 0 to 11 */
 RTC_Configuration();
 Time_SetCalendarTime(tm_now);
 BKP_WriteBackupRegister(BKP_DR1, 0xA5A5);
}
void change_year(int year)
{
  struct tm tm_now;
  memset(&tm_now, 0 , sizeof(tm_now) );
  tm_now = Time_GetCalendarTime();	
 tm_now.tm_year = year;  /* years since 1900 */
 RTC_Configuration();
 Time_SetCalendarTime(tm_now);
BKP_WriteBackupRegister(BKP_DR1, 0xA5A5);
}
void change_hour(int hour)
{
  struct tm tm_now;
  memset(&tm_now, 0 , sizeof(tm_now) );
  tm_now = Time_GetCalendarTime();	
 tm_now.tm_hour =hour;  /* hours since midnight, 0 to 23 */
 RTC_Configuration();
 Time_SetCalendarTime(tm_now);
BKP_WriteBackupRegister(BKP_DR1, 0xA5A5);
}
/*******************************************************************************
* Function Name  : Time_Display
* Description    : Printf Time
* Input          : None
* Output         : None
* Return         : None
* Attention		 : None
*******************************************************************************/
void Time_Display(void)
{
  char temp[30];
//  uint8_t i;
   struct tm time;
   time = Time_GetCalendarTime();
  //LCD_Clear(Black);
	
OLED_Clear();
if(time.tm_sec >=5 && time.tm_sec <=10)	{
	 	OLED_ShowString1206(0,0, "HTpro R&D");  
	OLED_ShowString1206(0,16,"Tel: 0866560782 ");  
 	OLED_ShowString1206(0,32,"info@htpro.vn");  
 	OLED_ShowString1206(0,48,"RTC Example"); 

OLED_Refresh_Gram();}
	else{
  sprintf(temp, "  _%d/%d/%d_", time.tm_mday, time.tm_mon,  time.tm_year);
	OLED_ShowString1206(10,40, temp);
 
  sprintf(temp, "%02d:%02d:%02d", time.tm_hour, time.tm_min, time.tm_sec);
	OLED_ShowString1608(30,10, temp);
OLED_Refresh_Gram();
 }
  
}
