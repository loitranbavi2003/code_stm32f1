	
#ifndef __PWM__
#define __PWM__

#ifdef __cplusplus
 extern "C" {
#endif
#include "stm32f10x.h" 
void PWM_Config(void);
#ifdef __cplusplus
}
#endif

#endif

/********************************* END OF FILE ********************************/
/******************************************************************************/

