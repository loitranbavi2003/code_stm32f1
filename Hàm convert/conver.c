#include "conver.h"

void Conver_test(void(*printff)(char*))
{
/*1-------------------------Test ham Conver_strTofloat, Conver_floatToStr---------------------------*/
  char *arr_strTofloat = "1234.20", arr_floatTostr[20];
  float temp_strTofloat = Conver_strTofloat(arr_strTofloat);
  temp_strTofloat = temp_strTofloat + 6.3;
  Conver_floatToStr(temp_strTofloat, arr_floatTostr, 2);
  printff(arr_floatTostr);
/*-------------------------end---------------------------*/
  printff("\n");
  
/*2-------------------------Test ham Conver_strToint, Conver_intToStr---------------------------*/
  char *arr_strToint = "1234567", arr_intTostr[20];
  long temp_strToint = Conver_strToint(arr_strToint);
  temp_strToint = temp_strToint + 2;
  Conver_intToStr(temp_strToint, arr_intTostr, 0);
  printff(arr_intTostr);
/*-------------------------end---------------------------*/
  printff("\n");
  
/*3-------------------------Test ham Conver_strhexToint---------------------------*/
  char *arr_strhexToint = "AAAA", arr_inthexTostr[20];
  long temp_strhexToint = Conver_strhexToint(arr_strhexToint);
  temp_strhexToint = temp_strhexToint + 1;
  Conver_intToStr(temp_strhexToint, arr_inthexTostr, 0);
  printff(arr_inthexTostr);
/*-------------------------end---------------------------*/
  printff("\n");
  
/*4-------------------------Test ham Conver_strstr---------------------------*/
  char *data1 = "xin 12345chao", *data2 = "chao";
  if(Conver_strstr(data1, data2) != NULL)
  {
    printff("done");
  }
/*-------------------------end---------------------------*/
  printff("\n");  
  
 /*5-------------------------Test ham int to hex string---------------------------*/
  int test_data = 32767; // max = 7fff
  char str[10];
  Conver_decToHexa(test_data,str);
  printff(str);
/*-------------------------end---------------------------*/
  printff("\n"); 
  
 /*5-------------------------Test ham size kieu du lieu--------------------------*/
  char stra[5];
  Conver_intToStr(sizeof(int),stra,0);
  printff("int = ");
  printff(stra);
  printff("\n"); 
  
  Conver_intToStr(sizeof(char),stra,0);
  printff("char = ");
  printff(stra);
  printff("\n");
  
  Conver_intToStr(sizeof(long),stra,0);
  printff("long = ");
  printff(stra);
  printff("\n");
  
  Conver_intToStr(sizeof(float),stra,0);
  printff("float = ");
  printff(stra);
  printff("\n");
  
  Conver_intToStr(sizeof(long long),stra,0);
  printff("long long = ");
  printff(stra);
  printff("\n");
  
/*-------------------------end---------------------------*/
  printff("\n");  
}

void Conver_decToHexa(int n, char *str)
{
   int i = 0;
    while (n != 0) {
        int temp = 0;
        temp = n % 16;
        if (temp < 10) {
            str[i] = temp + 48;
            i++;
        }
        else {
            str[i] = temp + 55;
            i++;
        }
 
        n = n / 16;
    }
    Conver_reverse(str,i);
    str[i] = 0x00;
}

float Conver_strTofloat(char *arr)
{
  float val = 0;
  int afterdot=0;
  float scale=1;
  int neg = 0; 

  if (*arr == '-') {
    arr++;
    neg = 1;
  }
  while (*arr) {
    if (afterdot) {
      scale = scale/10;
      val = val + (*arr-'0')*scale;
    } else {
      if (*arr == '.') 
    afterdot++;
      else
    val = val * 10.0 + (*arr - '0');
    }
    arr++;
  }
  if(neg) return -val;
  else    return  val;
}


long Conver_strToint(char *str)
{
  long result;
  long puiss;

  result = 0;
  puiss = 1;
  while (('-' == (*str)) || ((*str) == '+'))
  {
      if (*str == '-')
        puiss = puiss * -1;
      str++;
  }
  while ((*str >= '0') && (*str <= '9'))
  {
      result = (result * 10) + ((*str) - '0');
      str++;
  }
  return (result * puiss);
}

long Conver_strhexToint(char *in)
{
  char *pin = in; 
  long out = 0;  

  while (*pin != 0) {
    out <<= 4; 
    out +=  (*pin < 'A') ? *pin & 0xF : (*pin & 0x7) + 9; 
    pin++;
  }

  return out;
}

void Conver_reverse(char* str, int len)
{
  int i = 0, j = len - 1, temp;
  while (i < j) {
    temp = str[i];
    str[i] = str[j];
    str[j] = temp;
    i++;
    j--;
  }
}

long Conver_intToStr(long x, char str[], long d)
{
  long i = 0;
  while (x) {
    str[i++] = (x % 10) + '0';
    x = x / 10;
  }
  while (i < d)
    str[i++] = '0';

  Conver_reverse(str, i);
  str[i] = '\0';
  return i;
}

void Conver_floatToStr(float n, char* res, int afterpoint)
{
  int ipart = (int)n;
  float fpart = n - (float)ipart;
  int i = Conver_intToStr(ipart, res, 1);
  if (afterpoint != 0) {
    res[i] = '.';
    fpart = fpart * pow(10, afterpoint);
    Conver_intToStr((int)fpart, res + i + 1, afterpoint);
  }
}

int Conver_compare(const char *X, const char *Y)
{
  while (*X && *Y)
  {
    if (*X != *Y) {
      return 0;
    }

    X++;
    Y++;
  }

  return (*Y == '\0');
}

const char* Conver_strstr(const char* X, const char* Y)
{
  while (*X != '\0')
  {
    if ((*X == *Y) && Conver_compare(X, Y)) {
      return X;
    }
    X++;
  }

  return NULL;
}
