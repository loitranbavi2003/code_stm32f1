
#ifndef __CONVER_H
#define __CONVER_H

#include <stdio.h>
#include <math.h>
/*h�m conver from string to float*/
float Conver_strTofloat(char *arr); //1

/*h�m conver from string to int*/
long Conver_strToint(char *str);//2

/*h�m conver from string hex to int*/
long Conver_strhexToint(char *in); //3

/*h�m swap*/
void Conver_reverse(char* str, int len);

/*h�m conver from int to string*/
long Conver_intToStr(long x, char str[], long d);//2

/*h�m conver from float to string*/
void Conver_floatToStr(float n, char* res, int afterpoint);//1

/*h�m so sanh 2 chuoi*/
int Conver_compare(const char *X, const char *Y);//4

/*h�m phat hien chuoi*/
const char* Conver_strstr(const char* X, const char* Y);//4

/*h�m convert decimal to hexadecimal*/
void Conver_decToHexa(int n, char *str); //5

void Conver_test(void(*printff)(char*));
#endif 
