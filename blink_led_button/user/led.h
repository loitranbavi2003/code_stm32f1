#ifndef _LED_H_
#define _LED_H_

#define GPIO_LED GPIO_Pin_8
#define PORT_LED GPIOA

#define GPIO_BUTTON GPIO_Pin_4
#define PORT_BUTTON GPIOA

void Led_Init(void);
void Button_Init(void);

#endif
