#include "led.h"
#include "stm32f10x.h"


void Led_Init(void)
{
	GPIO_InitTypeDef Led;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	Led.GPIO_Mode = GPIO_Mode_Out_PP;
	Led.GPIO_Pin = GPIO_LED;
	Led.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(PORT_LED, &Led);
}

void Button_Init(void)
{
	GPIO_InitTypeDef Button;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	Button.GPIO_Mode = GPIO_Mode_IPU;
	Button.GPIO_Pin = GPIO_BUTTON;
	Button.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(PORT_BUTTON, &Button);
}
