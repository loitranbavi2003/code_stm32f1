#ifndef _DELAY_H_
#define _DELAY_H_

#include "stm32f10x.h"

void Delay_Init(void);
void SysTick_Handler(void);
void delay_ms(uint32_t time);

#endif
