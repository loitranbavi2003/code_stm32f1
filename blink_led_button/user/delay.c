#include "delay.h"

uint32_t nTick = 0;

void Delay_Init(void)
{
	SystemCoreClockUpdate();
	SysTick_Config(SystemCoreClock/1000);
}

void SysTick_Handler(void)
{
	nTick--;
}

void delay_ms(uint32_t time)
{
	nTick = time;
	while(nTick != 0){}
}
