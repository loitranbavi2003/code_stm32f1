#include "stm32f10x.h"
#include "delay.h"
#include "led.h"

int main()
{
	uint8_t count_button = 0;
	uint8_t button_state_old = 0;
	uint8_t button_state_new = 0;
	Led_Init();
	Button_Init();
	Delay_Init();
	while(1)
	{
		/*CHong nhieu nut nhan*/
		button_state_new = GPIO_ReadInputDataBit(PORT_BUTTON, GPIO_BUTTON);
		if(button_state_new != button_state_old)
		{
			delay_ms(30);
			button_state_new = GPIO_ReadInputDataBit(PORT_BUTTON, GPIO_BUTTON);
			if(button_state_new != button_state_old)
			{
				button_state_old = button_state_new;
				if(button_state_new == 1)
				{
					count_button++;
				}
			}
		}
		
		if(count_button%2 == 1)
		{
			GPIO_SetBits(PORT_LED, GPIO_LED);
		}
		else
		{
			count_button = 0;
			GPIO_ResetBits(PORT_LED, GPIO_LED);
		}
		
		/*Nhan giu*/
//		if(GPIO_ReadInputDataBit(PORT_BUTTON, GPIO_BUTTON) == 1)
//		{
//			GPIO_SetBits(PORT_LED, GPIO_LED);
//		}
//		else
//		{
//			GPIO_ResetBits(PORT_LED, GPIO_LED);
//		}
		
		/*Blink led*/
//		GPIO_SetBits(PORT_LED, GPIO_LED);
//		delay_ms(1000);
//		GPIO_ResetBits(PORT_LED, GPIO_LED);
//		delay_ms(1000);
	}
}
