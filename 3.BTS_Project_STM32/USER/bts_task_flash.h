#ifndef __TASK_FLASH__
#define __TASK_FLASH__
	
#ifdef __cplusplus
extern "C"{
#endif
#include "bts_sys.h"

void BTS_RTOS_Task_Flash(void *p);

#ifdef __cplusplus
}
#endif

#endif
