#include "bts_config_gpio.h"

#define NOT_USE_ADC 0

/**
 * @brief structure of gpio pin configuration for sensors.
 * 
 */
define_GpioPin_t gpio_pin_sensor[SIZE_LIST_SENSOR]=
{
	{GPIOA, GPIO_Mode_AIN,    GPIO_Speed_50MHz,   GPIO_Pin_0, ADC_Channel_0,  "NTC1 Pin"},
	{GPIOA, GPIO_Mode_AIN,    GPIO_Speed_50MHz,   GPIO_Pin_12, ADC_Channel_1,  "NTC2 Pin"},	
	{GPIOA, GPIO_Mode_IPD,    GPIO_Speed_50MHz, 	GPIO_Pin_14, NOT_USE_ADC,  "Door Pin"},
	{GPIOC, GPIO_Mode_IPD,    GPIO_Speed_50MHz, GPIO_Pin_4, NOT_USE_ADC, "Smoke Pin"},
	{GPIOA, GPIO_Mode_IPD,    GPIO_Speed_50MHz, GPIO_Pin_15, NOT_USE_ADC, "Water Pin"},
	{GPIOC, GPIO_Mode_IPD,    GPIO_Speed_50MHz, GPIO_Pin_10, NOT_USE_ADC, "AirCondition status Pin"},
};

/**
 * @brief structure of gpio pin configuration for devices.
 * 
 */
define_GpioPin_t gpio_pin_device[SIZE_LIST_DEVICE]=
{
//	{GPIOB, GPIO_Mode_Out_PP, GPIO_Speed_50MHz, GPIO_Pin_4, NOT_USE_ADC,  "AirCondition control Pin"},
//	{GPIOB, GPIO_Mode_Out_PP, GPIO_Speed_50MHz, GPIO_Pin_6, NOT_USE_ADC, "Fan Pin"},
//	{GPIOB, GPIO_Mode_Out_PP, GPIO_Speed_50MHz, GPIO_Pin_5, NOT_USE_ADC, "LAMP Pin"},	
	{GPIOA, GPIO_Mode_Out_PP, GPIO_Speed_50MHz, GPIO_Pin_3, NOT_USE_ADC,  "AirCondition control Pin"},
	{GPIOA, GPIO_Mode_Out_PP, GPIO_Speed_50MHz, GPIO_Pin_2, NOT_USE_ADC, "Fan Pin"},
	{GPIOA, GPIO_Mode_Out_PP, GPIO_Speed_50MHz, GPIO_Pin_1, NOT_USE_ADC, "LAMP Pin"},	
};

/**
 * @brief structure of status con trol of device.
 * Fisrt data of array if state on
 * Second data of array if state off
 */
define_status_device_t device_status[SIZE_LIST_DEVICE] =
{
	{RESET, SET},
	{RESET, SET},
	{RESET, SET},
};

/**
 * @brief Config for the gpio pin of sensor.
 * 
 * @param gpio_element : the position of the sensor in the sensor list.
 */
void BTS_Config_GPIO_Sensor(uint8_t gpio_element)
{
	GPIO_InitTypeDef GPIO_Struct;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
	GPIO_Struct.GPIO_Pin = gpio_pin_sensor[gpio_element].pin;
	GPIO_Struct.GPIO_Mode = gpio_pin_sensor[gpio_element].mode;
	GPIO_Struct.GPIO_Speed = gpio_pin_sensor[gpio_element].speed;
	GPIO_Init(gpio_pin_sensor[gpio_element].port, &GPIO_Struct);
}

/**
 * @brief Config for the gpio pin of device.
 * 
 * @param gpio_element : the position of the device in the device list.
 */
void BTS_Config_GPIO_Device(uint8_t gpio_element)
{
	GPIO_InitTypeDef GPIO_Struct;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
	GPIO_Struct.GPIO_Pin = gpio_pin_device[gpio_element].pin;
	GPIO_Struct.GPIO_Mode = gpio_pin_device[gpio_element].mode;
	GPIO_Struct.GPIO_Speed = gpio_pin_device[gpio_element].speed;
	GPIO_Init(gpio_pin_device[gpio_element].port, &GPIO_Struct);
}
