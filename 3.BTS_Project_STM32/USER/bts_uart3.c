#include "bts_uart3.h"

#define UART_FSM 1

uint8_t array_out[50];

uint8_t uart3_rxbuffer[100];
__IO uint16_t uart3_rxcount; 
__IO uint8_t uart3_flag_rx = 0;

/**
 * @brief Configure the UART3 for 9600 baud rate, 8 data bits, 1 stop bit, no parity
 * 
 */
void SmartBTS_USART3_Init(void)
{
	GPIO_InitTypeDef GPIO_Struct;
	USART_InitTypeDef USART_Struct;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA|RCC_APB2Periph_USART1,ENABLE);
	GPIO_Struct.GPIO_Mode	 = GPIO_Mode_AF_PP;
	GPIO_Struct.GPIO_Pin     = GPIO_Pin_9;
	GPIO_Struct.GPIO_Speed	 = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA,&GPIO_Struct);
	
	GPIO_Struct.GPIO_Mode	 = GPIO_Mode_IN_FLOATING;
	GPIO_Struct.GPIO_Pin     = GPIO_Pin_10;
	GPIO_Struct.GPIO_Speed	 = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA,&GPIO_Struct);
	
	USART_Struct.USART_BaudRate 			= 9600;
	USART_Struct.USART_HardwareFlowControl  = USART_HardwareFlowControl_None;
	USART_Struct.USART_Mode 				= USART_Mode_Rx|USART_Mode_Tx;
	USART_Struct.USART_Parity      			= USART_Parity_No;
	USART_Struct.USART_StopBits 			= USART_StopBits_1;
	USART_Struct.USART_WordLength 			= USART_WordLength_8b;
	USART_Init(USART1,&USART_Struct);
	
	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
	NVIC_EnableIRQ(USART1_IRQn);
	USART_Cmd(USART1,ENABLE);
}

/**
 * @brief Send a character
 * 
 * @param datain 
 */
void SmartBTS_USART3_SendChar(const uint8_t datain)
{
	USART_SendData(USART1,datain);
	while(USART_GetFlagStatus(USART1, USART_FLAG_TXE)==RESET);
}

/**
 * @brief send a string
 * 
 * @param datain 
 */
void SmartBTS_USART3_SendString(const char *datain)
{
	while(*datain != 0)
	{
		SmartBTS_USART3_SendChar(*datain++);
	}
}

/**
 * @brief Send a byte data
 * 
 * @param datain 
 */
void SmartBTS_USART3_SendOneByte(const uint8_t datain)
{
	USART_SendData(USART1,(uint8_t)datain);
	while(USART_GetFlagStatus(USART1, USART_FLAG_TXE)==RESET);
}

/**
 * @brief Send array bytes data
 * 
 * @param datain 
 */
void SmartBTS_USART3_SendArrayByte(const uint8_t *datain, const uint16_t arrsize)
{
	uint8_t countlength;
	for(countlength = 0; countlength < arrsize; countlength++)
	{
		SmartBTS_USART3_SendOneByte(datain[countlength]);
	}
}

#if (UART_FSM == 1)
/**
 * @brief Received array data of message
 * 
 * @return uint8_t* : array data of message
 */
uint8_t* SmartBTS_USART3_Get_Array_Data(void)
{
	return array_out;
}

void USART1_IRQHandler(void) 
{
	uint8_t tempdata;
	if(USART_GetITStatus(USART1, USART_IT_RXNE) != RESET)
	{
		tempdata = USART_ReceiveData(USART1);
		mobus_rx_interrupt_flag = SET;
		modbus_data_receive[bts_modbus_data_counter++] = tempdata;
		if(bts_modbus_data_counter >= BUFFERSIZE)
		{
			bts_modbus_data_counter  = 0;
		}

		modbus_uart_time_counter = 0;
	}
}

#else
void UART3_IRQHandler(void)
{
	uint8_t tempchar;
	if(RESET != usart_interrupt_flag_get(UART3, USART_INT_FLAG_RBNE))
	{
		tempchar = usart_data_receive(UART3);
		if(tempchar != '\n')
		{	
			uart3_rxbuffer[uart3_rxcount] = tempchar;
			uart3_rxcount++;
		}
		else
		{
			uart3_rxbuffer[uart3_rxcount] = 0x00;
			uart3_flag_rx = 1;
			uart3_rxcount = 0;
		}
	}
}
#endif
