#include "stm32f10x_gpio.h"
#include "stm32f10x.h"
#include "systick.h"
#include <stdio.h>
#include "usart.h"
#include "ModbusRTU_Slave.h"

void led_init(void);

int main()
{
	int i;
	led_init();
	systick_config();
	USART_Config(9600);
	USART2_Config(9600);
	
	for(i = 0; i < 40; i++)
	{
		ModbusRegister[i] = 10 + i;
	}
	
	while(1)
	{
		uartDataHandler();
	}
}

void led_init(void)
{
	GPIO_InitTypeDef Led;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
	Led.GPIO_Mode = GPIO_Mode_Out_PP;
	Led.GPIO_Pin = GPIO_Pin_13;
	Led.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOC, &Led);
	
	GPIO_SetBits(GPIOC, GPIO_Pin_13);
}

