#include "timer.h"

int vri_FlagTimer2 = 0;

void Timer_Init(void){ // 1ms
	TIM_TimeBaseInitTypeDef timer_init;
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
	timer_init.TIM_CounterMode = TIM_CounterMode_Up;
	timer_init.TIM_Period = 1000 - 1;
	timer_init.TIM_ClockDivision = 0;
	timer_init.TIM_RepetitionCounter = 0;
	timer_init.TIM_Prescaler = 72 - 1;	
	TIM_TimeBaseInit(TIM2,&timer_init);
	TIM_ClearFlag(TIM2,TIM_FLAG_Update);
	TIM_ITConfig(TIM2,TIM_IT_Update,ENABLE);
	NVIC_EnableIRQ(TIM2_IRQn);
	TIM_Cmd(TIM2,ENABLE);
}

void TIM2_IRQHandler(){
	if(TIM_GetITStatus(TIM2, TIM_IT_Update)!=RESET){		
		vri_FlagTimer2 = 1;
	}	
	TIM_ClearITPendingBit(TIM2,TIM_IT_Update);
}



