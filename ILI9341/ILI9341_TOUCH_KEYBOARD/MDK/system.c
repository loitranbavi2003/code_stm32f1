#include "stm32f10x.h"                  // Device header
#include "stm32f10x_gpio.h"             // Keil::Device:StdPeriph Drivers:GPIO
#include "stm32f10x_rcc.h"              // Keil::Device:StdPeriph Drivers:RCC
#include "stm32f10x_spi.h"              // Keil::Device:StdPeriph Drivers:SPI
#include "stm32f10x_tim.h"              // Keil::Device:StdPeriph Drivers:TIM
#include "ILI9341_Gui.h"
#include "ILI9341_Driver.h"
#include "ILI9341_Define.h"
#include "touchscreen.h"
#include "SPI_MSD0_Driver.h"
#include "ff.h"
#include "diskio.h"
#include "lib_sdcard.h"
#include "delay_sys.h"
#include "keyboard.h"
#include "usart.h"
#include "stdio.h"
#include "timer.h"
#include "string.h"

u16 ColorTab[5]={RED,GREEN,BLUE,YELLOW,MAGENTA};
FATFS fs, Fn;        		 /* Khu v?c l�m vi?c (h? th?ng t?p) cho ? dia logic */
FRESULT res;
FIL fsrc, fdst;      /* file objects */
UINT br,bw;
int k = 0, i = 0, j = 0;
unsigned char buffer1[10000];
//Test_touchV1
char counter_buff[30];
uint16_t x_pos = 0;
uint16_t y_pos = 0;				
uint16_t position_array[2];	

/*Variable KeyBoard*/
char KeyBoard_On[10]={'q','w','e','r','t','y','u','i','o','p'};
int Set_Posi_Acc = 0, Set_Posi_Pass = 0;
int vri_FlagLBAccount = 0, vri_FlagLBPassword = 0;
int Input_Account = 0, Input_Password = 0;

void Test_FillRec(void){
	u8 i=0;
	TFT_Gui_Fill(0,20,ili_dev.width,ili_dev.height-20,WHITE);
	for(i=0; i<5; i++){
		POINT_COLOR = ColorTab[i];
		TFT_DrawRectangle(ili_dev.width/2-80+(i*15),ili_dev.height/2-80+(i*15),ili_dev.width/2-80+(i*15)+60,ili_dev.height/2-80+(i*15)+60); 
	}
	Delay_ms(1500);	
	TFT_Gui_Fill(0,20,ili_dev.width,ili_dev.height-20,WHITE); 
	for (i=0; i<5; i++){
		POINT_COLOR = ColorTab[i];
		TFT_Gui_Fill(ili_dev.width/2-80+(i*15),ili_dev.height/2-80+(i*15),ili_dev.width/2-80+(i*15)+60,ili_dev.height/2-80+(i*15)+60,POINT_COLOR); 
	}
	Delay_ms(1500);
}
/*********************************************************************************/
void Test_Fill(void){
	TFT_Gui_Fill(0,0,ili_dev.width,ili_dev.height,WHITE);
	Delay_ms(1000);
	TFT_Gui_Fill(0,0,ili_dev.width,ili_dev.height,RED);
	Delay_ms(1000);
	TFT_Gui_Fill(0,0,ili_dev.width,ili_dev.height,GREEN);
	Delay_ms(1000);
	TFT_Gui_Fill(0,0,ili_dev.width,ili_dev.height,BLUE);
}
/*********************************************************************************/
void Test_Circle(void){
	u8 i=0;
	TFT_Gui_Fill(0,20,ili_dev.width,ili_dev.height-20,WHITE);
	for (i=0; i<5; i++){  
		TFT_Gui_Circle(ili_dev.width/2-80+(i*25),ili_dev.height/2-50+(i*25),ColorTab[i],30,0);
	}
	Delay_ms(1500);	
	TFT_Gui_Fill(0,20,ili_dev.width,ili_dev.height-20,WHITE); 
	for (i=0; i<5; i++){ 
		TFT_Gui_Circle(ili_dev.width/2-80+(i*25),ili_dev.height/2-50+(i*25),ColorTab[i],30,1);
	}
	Delay_ms(1500);
	
}
/*********************************************************************************/
void Test_Triangle(void){
	u8 i=0;
	TFT_Gui_Fill(0,20,ili_dev.width,ili_dev.height-20,WHITE);
	for(i=0;i<5;i++){
		POINT_COLOR=ColorTab[i];
		TFT_Gui_Draw_Triangel(ili_dev.width/2-80+(i*20),ili_dev.height/2-20+(i*15),ili_dev.width/2-50-1+(i*20),ili_dev.height/2-20-52-1+(i*15),ili_dev.width/2-20-1+(i*20),ili_dev.height/2-20+(i*15));
	}
	Delay_ms(1500);	
	TFT_Gui_Fill(0,20,ili_dev.width,ili_dev.height-20,WHITE); 
	for(i=0;i<5;i++){
		POINT_COLOR=ColorTab[i];
		ILI9431_Gui_Fill_Triangel(ili_dev.width/2-80+(i*20),ili_dev.height/2-20+(i*15),ili_dev.width/2-50-1+(i*20),ili_dev.height/2-20-52-1+(i*15),ili_dev.width/2-20-1+(i*20),ili_dev.height/2-20+(i*15));
	}
	Delay_ms(1500);
}
/*********************************************************************************/
void Test_font_Char(void){
//	TFT_Show_6x8_char(10,10,BLACK,WHITE,'T',1);
	TFT_Show_6x12_char(10,20,BLACK,WHITE,'A',1);
	TFT_Show_8x16_char(10,40,BLACK,WHITE,'A',1);
	TFT_Show_12x24_char(20,60,BLACK,WHITE,'A',1);
	TFT_Show_16x32_char(50,90,BLACK,YELLOW,'A',0);
	TFT_Show_24x48_char(20,120,BLACK,YELLOW,'A',0);
}
/*********************************************************************************/
void Test_Font_String(void){ // In Thuong
//	TFT_Print(10,10,BLACK,WHITE, 8,"Thai Salem",1);
	TFT_Print(10,20,BLACK,WHITE,12,"Thai Salem",1);
	TFT_Print(10,40,BLACK,WHITE,16,"Thai Salem",1);
	TFT_Print(10,60,BLACK,WHITE,24,"Thai Salem",1);
	TFT_Print(10,90,BLACK,WHITE,48,"Thai Salem",1);
	Delay_ms(1000);
}
/*********************************************************************************/
void Test_Font_String_Bold(void){ // In Dam
	TFT_Print_Bold(20,20,BLACK,WHITE,16,"Thai MCU",1);
	TFT_Print_Bold(20,50,BLACK,WHITE,24,"Thai Salem",1);
	TFT_Print_Bold(20,70,BLACK,WHITE,32,"22/05/2000",1);
}
/*********************************************************************************/
void Test_Print_Num(void){
	TFT_Print_Num(100,100,BLACK,YELLOW,32,1,220500,6,1); // IN Dam
	TFT_Print_Num(130,130,BLACK,YELLOW,24,0,60700,5,1); // In Thuong
}
/*********************************************************************************/
void Test_Main(void){
	Test_Fill();
	Delay_ms(1000);
	Test_FillRec();
	Delay_ms(1000);
	Test_Triangle();
	Delay_ms(1000);
	Test_Circle();
	Delay_ms(1000);
	TFT_Clear(WHITE);
	Test_font_Char();
	Delay_ms(3000);
	TFT_Clear(WHITE);
	Test_Font_String();
	Delay_ms(3000);
	TFT_Clear(WHITE);
	Test_Font_String_Bold();
	Delay_ms(3000);
	TFT_Clear(WHITE);
	Test_Print_Num();
	Delay_ms(3000);
	TFT_Clear(WHITE);
}
/*********************************************************************************/
void Button(void){
	POINT_COLOR = RED;
	TFT_DrawRectangle(20,50,80,110); 
	TFT_Gui_Fill(20,50,80,110,BLUE);
	TFT_Print_Center(20,50,80,110,BLACK,BLUE,32,BOLD,"ON",1);
	POINT_COLOR = BLUE;
	TFT_DrawRectangle(160,50,220,110); 
	TFT_Gui_Fill(160,50,220,110,RED);
	TFT_Print_Center(160,50,220,110,BLACK,BLUE,32,BOLD,"OFF",1);
	Delay_ms(1);
}
/*********************************************************************************/
void Test_TouchV1(void){
	if(TP_Touchpad_Pressed()){				
		if(TP_Read_Coordinates(position_array) == TOUCHPAD_DATA_OK){
			BT_IRQ_Pin.vruc_FlagChange = 1;
			x_pos = position_array[0];
			y_pos = position_array[1];		
				/* Hien thi vi tri cam ung */			
			//sprintf(counter_buff, "POS X: %.3d", x_pos);						
			//TFT_Print(10, 10, BLACK, WHITE, 16, (char*)counter_buff, 1);					
			//sprintf(counter_buff, "POS Y: %.3d", y_pos);						
			//TFT_Print(10, 25, BLACK, WHITE, 16, (char*)counter_buff, 1);	
			printf("X: %d\n",x_pos);
			printf("Y: %d\n",y_pos);
			printf("**************************\n");
			Delay_ms(10);
//			TFT_Clear_Pos(0,0,320,60,LIGHTGREY);
//			TFT_Clear(WHITE);
		}	
	}
//	if((x_pos > 20 && x_pos < 80) && (y_pos > 50 && y_pos < 110)){
//		TFT_Clear_Pos(0, 200, 240, 280,WHITE);
//		TFT_Print_Center(0, 200, 240, 270,  BLACK, WHITE, 24, NORMAL, "Do Van Thai", 1);
//	}	
//	else if((x_pos > 160 && x_pos < 220) && (y_pos > 50 && y_pos < 110)){
//		TFT_Clear_Pos(0, 200, 240, 280,WHITE);
//		TFT_Print_Center(0, 200, 240, 270,  BLACK, WHITE, 24, NORMAL, "Vu Thi Thu Ha", 1);
//	}
//	x_pos = y_pos = 0;
}
/*********************************************************************************/
void SD_ReadBmp(u16 x, u16 y, u16 picxel_X, u16 picxel_Y, const TCHAR* Fname){
	int m = 0;
	int npicxel = picxel_X*picxel_Y*2;
	int nByte = npicxel/(picxel_Y/10);

	res = f_mount(0,&fs);
	res = f_open(&fsrc, Fname, FA_OPEN_EXISTING | FA_READ);
	
	// Doc 70 bytes dau
	res = f_read(&fsrc, buffer1,70, &br);
	for(i=0;i<sizeof(buffer1);i++) buffer1[i]=0;
	for(m = 0; m < picxel_Y/10; m++){
		res = f_read(&fsrc, buffer1, nByte, &br);
		TFT_Gui_Img(x,y+m*10,picxel_X,10, buffer1);
		for(i=0;i<sizeof(buffer1);i++) buffer1[i]=0;
	}
	f_close(&fsrc);
	
}
/*********************************************************************************/
void SD_ReadBmp_Normal(void){
	res = f_mount(0,&fs);							// G?n k?t h? th?ng t?p	
	res = f_open(&fsrc,"login16b.bmp", FA_OPEN_EXISTING | FA_READ);
	
	// Doc 70 bytes dau
	res = f_read(&fsrc, buffer1,70, &br);     /* Read a chunk of src file */

	for(i=0;i<sizeof(buffer1);i++) buffer1[i]=0;

	// Doc cac bytes con lai va hien thi len LCD
	for(k = 0; k < 24; k++){
		res = f_read(&fsrc, buffer1,6400, &br);     /* Read a chunk of src file */
		Gui_DrawbmpUser(0,k*10,320,10,buffer1);
		for(i=0;i<sizeof(buffer1);i++) buffer1[i]=0;
	}	
	f_close(&fsrc);
}
/*********************************************************************************/
struct frame{

	unsigned char Data[100];

};
struct frame fr;
/*********************************************************************************/
unsigned char Rbuffer[5000];
void strTObyte(){ // ghi lieu vao file -> doc tu file ra -> hien thi len TFT => OKi
	unsigned char *Wbuffer;

	for(i = 0; i<5000; i++){
		fr.Data[i] = dataImg[i];
	}
	Wbuffer = (char*)malloc(sizeof(fr));
	memcpy(Wbuffer,(const unsigned char*)&fr,sizeof(fr));

	res = f_mount(0,&fs);
	/*-------------------------------------------------------------------*/
	res = f_open(&fdst, "CodeIMG.txt", FA_CREATE_ALWAYS | FA_WRITE);
	
	if(res != FR_OK){
		printf("open file error : %d\n\r",res);
		printf("Error\n");
	}
	else{
	  res = f_write(&fdst, Wbuffer, sizeof(fr), &bw);/* Ghi n� v�o t?p dst */
		if(res == FR_OK){
			printf("Creart FILE Successful\n");
			printf("write data ok! %d\n\r",bw);
		}
		else{
			printf("write data error : %d\n\r",res);
		}
	}
	f_close(&fdst);	
	
	printf("\nread file test......\n\r");
	res = f_open(&fsrc, "CodeIMG.txt", FA_OPEN_EXISTING | FA_READ);
	
	if(res != FR_OK){
		printf("open file error : %d\n\r",res);
	}
	else{
		res = f_read(&fsrc, Rbuffer,5000, &br);     /* Read a chunk of src file */
		if(res==FR_OK){
			printf("\n\rread data num : %d\n\r",br);
			printf("%s",Rbuffer);
		}
		else{
			printf("read file error : %d\n\r",res);
		}
	}		
	f_close(&fsrc);
	free(Wbuffer);
	TFT_Gui_Img(10,10,50,50,Rbuffer);
}
/*********************************************************************************/

int Check_Posi_Screen(u16 x1, u16 y1, u16 x2, u16 y2){
	if((x_pos >= x1 && x_pos <= x2) && (y_pos >= y1 && y_pos <= y2)) return 1;
	else return 0;
}

//enum SCRREEN_LAYOUT{
//	LAYOUT_HOME = 0,
//	LAYOUT_INPUT_ACCOUNT = 1,
//	LAYOUT_INPUT_PASSWORD = 2,
//};

#define	LAYOUT_HOME  						0;
#define	LAYOUT_INPUT_ACCOUNT  	1;
#define	LAYOUT_INPUT_PASSWORD  	2;

typedef struct {
	int vri_Layout[100];
}Layout;

Layout layout;

char temp_acc[20], temp_pass[20], temp_acc_after[20];
u8 *str_acc, *str_pass; 
int row_n0 = 0, row_n1 = 0; // hang so
int row_x1 = 0, row_x2 = 0; // hang tren
int row_x3 = 0, row_x4 = 0; // hang co so
int row_x5 = 0, row_x6 = 0; // Hang Duoi

int y1 = 0, y2 = 0; // hang tren
int y3 = 0, y4 = 0; // hang co so
int y5 = 0, y6 = 0; // Hang Duoi

volatile int vrvi_GetCounter_InputAccount  = 0;
volatile int vrvi_Flag_InputAccount 			 = 0;
volatile int vrvi_GetCounter_InputPassWord = 0;
volatile int vrvi_Flag_InputPassWord 			 = 0;
volatile int vrvi_Flag_InputAcc_KeyBoard   = 0;
volatile int vrvi_Flag_InputPass_KeyBoard  = 0;


void Login_Input_Account(void){
	for(i = 0; i < 10; i++){
		row_n0 = 2 + i*34;  row_n1 = row_n0 + 30;	// Hang So	
		row_x1 = 2 + i*34;  row_x2 = row_x1 + 30;	// Hang Tren		
		row_x3 = 15 + i*35; row_x4 = row_x3 + 30; // Hang Co So
		row_x5 = 50 + i*35; row_x6 = row_x5 + 30; // Hang Duoi
		
		if( Check_Posi_Screen(row_n0,60,row_n1,90) ){ // Hang So
			Set_Posi_Acc++;
			switch(i){
				case 0: TFT_Print_Num(6+(Set_Posi_Acc-1)*13, 25, BLACK, BLACK, 24, 0, 1, 1, 1);
								temp_acc[Set_Posi_Acc-1] = '1';
								printf("%c",temp_acc[Set_Posi_Acc-1]);
								x_pos = 0;
								y_pos = 0;
								break; 
				case 1: TFT_Print_Num(6+(Set_Posi_Acc-1)*13, 25, BLACK, BLACK, 24, 0, 2, 1, 1);
								temp_acc[Set_Posi_Acc-1] = '2';
								printf("%c",temp_acc[Set_Posi_Acc-1]);
								x_pos = 0;
								y_pos = 0;
								break;
				case 2: TFT_Print_Num(6+(Set_Posi_Acc-1)*13, 25, BLACK, BLACK, 24, 0, 3, 1, 1);
								temp_acc[Set_Posi_Acc-1] = '3';
								printf("%c",temp_acc[Set_Posi_Acc-1]);
								x_pos = 0;
								y_pos = 0;
								break;
				case 3: TFT_Print_Num(6+(Set_Posi_Acc-1)*13, 25, BLACK, BLACK, 24, 0, 4, 1, 1);
								temp_acc[Set_Posi_Acc-1] = '4';
								printf("%c",temp_acc[Set_Posi_Acc-1]);
								x_pos = 0;
								y_pos = 0;
								break;
				case 4: TFT_Print_Num(6+(Set_Posi_Acc-1)*13, 25, BLACK, BLACK, 24, 0, 5, 1, 1);
								temp_acc[Set_Posi_Acc-1] = '5';
								printf("%c",temp_acc[Set_Posi_Acc-1]);
								x_pos = 0;
								y_pos = 0;
								break;
				case 5: TFT_Print_Num(6+(Set_Posi_Acc-1)*13, 25, BLACK, BLACK, 24, 0, 6, 1, 1);
								temp_acc[Set_Posi_Acc-1] = '6';
								printf("%c",temp_acc[Set_Posi_Acc-1]);
								x_pos = 0;
								y_pos = 0;
								break;
				case 6: TFT_Print_Num(6+(Set_Posi_Acc-1)*13, 25, BLACK, BLACK, 24, 0, 7, 1, 1);
								temp_acc[Set_Posi_Acc-1] = '7';
								printf("%c",temp_acc[Set_Posi_Acc-1]);
								x_pos = 0;
								y_pos = 0;
								break;
				case 7: TFT_Print_Num(6+(Set_Posi_Acc-1)*13, 25, BLACK, BLACK, 24, 0, 8, 1, 1);
								temp_acc[Set_Posi_Acc-1] = '8';
								printf("%c",temp_acc[Set_Posi_Acc-1]);
								x_pos = 0;
								y_pos = 0;
								break;
				case 8: TFT_Print_Num(6+(Set_Posi_Acc-1)*13, 25, BLACK, BLACK, 24, 0, 9, 1, 1);
								printf("%c",temp_acc[Set_Posi_Acc-1]);
								temp_acc[Set_Posi_Acc-1] = '9';
								x_pos = 0;
								y_pos = 0;
								break;
				case 9: TFT_Print_Num(6+(Set_Posi_Acc-1)*13, 25, BLACK, BLACK, 24, 0, 0, 1, 1);;
								temp_acc[Set_Posi_Acc-1] = '0';
								printf("%c",temp_acc[Set_Posi_Acc-1]);
								x_pos = 0;
								y_pos = 0;
								break;
				break;
			}
		} // end Hang So
		
		if( Check_Posi_Screen(row_x1,95,row_x2,130) ){ // Hang tren
			Set_Posi_Acc++;
			switch(i){
				case 0: TFT_Show_12x24_char(6+(Set_Posi_Acc-1)*13,25,BLACK,BLACK,'q',1);
								temp_acc[Set_Posi_Acc-1] = 'q';
								printf("%c",temp_acc[Set_Posi_Acc-1]);
								x_pos = 0;
								y_pos = 0;
								break; 
				case 1: TFT_Show_12x24_char(6+(Set_Posi_Acc-1)*13,25,BLACK,BLACK,'w',1);
								temp_acc[Set_Posi_Acc-1] = 'w';
								printf("%c",temp_acc[Set_Posi_Acc-1]);
								x_pos = 0;
								y_pos = 0;
								break;
				case 2: TFT_Show_12x24_char(6+(Set_Posi_Acc-1)*13,25,BLACK,BLACK,'e',1);
								temp_acc[Set_Posi_Acc-1] = 'e';
								printf("%c",temp_acc[Set_Posi_Acc-1]);
								x_pos = 0;
								y_pos = 0;
								break;
				case 3: TFT_Show_12x24_char(6+(Set_Posi_Acc-1)*13,25,BLACK,BLACK,'r',1);
								temp_acc[Set_Posi_Acc-1] = 'r';
								printf("%c",temp_acc[Set_Posi_Acc-1]);
								x_pos = 0;
								y_pos = 0;
								break;
				case 4: TFT_Show_12x24_char(6+(Set_Posi_Acc-1)*13,25,BLACK,BLACK,'t',1);
								temp_acc[Set_Posi_Acc-1] = 't';
								printf("\n%c\n",temp_acc[Set_Posi_Acc-1]);
								x_pos = 0;
								y_pos = 0;
								break;
				case 5: TFT_Show_12x24_char(6+(Set_Posi_Acc-1)*13,25,BLACK,BLACK,'y',1);
								temp_acc[Set_Posi_Acc-1] = 'y';
								printf("%c",temp_acc[Set_Posi_Acc-1]);
								x_pos = 0;
								y_pos = 0;
								break;
				case 6: TFT_Show_12x24_char(6+(Set_Posi_Acc-1)*13,25,BLACK,BLACK,'u',1);
								temp_acc[Set_Posi_Acc-1] = 'u';
								printf("%c",temp_acc[Set_Posi_Acc-1]);
								x_pos = 0;
								y_pos = 0;
								break;
				case 7: TFT_Show_12x24_char(6+(Set_Posi_Acc-1)*13,25,BLACK,BLACK,'i',1);
								temp_acc[Set_Posi_Acc-1] = 'i';
								printf("\n%c\n",temp_acc[Set_Posi_Acc-1]);
								x_pos = 0;
								y_pos = 0;
								break;
				case 8: TFT_Show_12x24_char(6+(Set_Posi_Acc-1)*13,25,BLACK,BLACK,'o',1);
								printf("%c",temp_acc[Set_Posi_Acc-1]);
								temp_acc[Set_Posi_Acc-1] = 'o';
								x_pos = 0;
								y_pos = 0;
								break;
				case 9: TFT_Show_12x24_char(6+(Set_Posi_Acc-1)*13,25,BLACK,BLACK,'p',1);
								temp_acc[Set_Posi_Acc-1] = 'p';
								printf("%c",temp_acc[Set_Posi_Acc-1]);
								x_pos = 0;
								y_pos = 0;
								break;
				break;
			}
		} // end Hang tren
		
		if( Check_Posi_Screen(row_x3,135,row_x4,170) ){// Hang co so
			Set_Posi_Acc++;
			switch(i){
				case 0: TFT_Show_12x24_char(6+(Set_Posi_Acc-1)*13,25,BLACK,BLACK,'a',1);
								temp_acc[Set_Posi_Acc-1] = 'a';
								printf("\n%c\n",temp_acc[Set_Posi_Acc-1]);
								x_pos = 0;
								y_pos = 0;
								break;
				case 1: TFT_Show_12x24_char(6+(Set_Posi_Acc-1)*13,25,BLACK,BLACK,'s',1);
								temp_acc[Set_Posi_Acc-1] = 's';
								x_pos = 0;
								y_pos = 0;
								break;
				case 2: TFT_Show_12x24_char(6+(Set_Posi_Acc-1)*13,25,BLACK,BLACK,'d',1);
								temp_acc[Set_Posi_Acc-1] = 'd';
								x_pos = 0;
								y_pos = 0;
								break;
				case 3: TFT_Show_12x24_char(6+(Set_Posi_Acc-1)*13,25,BLACK,BLACK,'f',1);
								temp_acc[Set_Posi_Acc-1] = 'f';
								x_pos = 0;
								y_pos = 0;
								break;
				case 4: TFT_Show_12x24_char(6+(Set_Posi_Acc-1)*13,25,BLACK,BLACK,'g',1);
								temp_acc[Set_Posi_Acc-1] = 'g';
								x_pos = 0;
								y_pos = 0;
								break;
				case 5: TFT_Show_12x24_char(6+(Set_Posi_Acc-1)*13,25,BLACK,BLACK,'h',1);
								temp_acc[Set_Posi_Acc-1] = 'h';
								printf("\n%c\n",temp_acc[Set_Posi_Acc-1]);
								x_pos = 0;
								y_pos = 0;
								break;
				case 6: TFT_Show_12x24_char(6+(Set_Posi_Acc-1)*13,25,BLACK,BLACK,'j',1);
								temp_acc[Set_Posi_Acc-1] = 'j';
								x_pos = 0;
								y_pos = 0;
								break;
				case 7: TFT_Show_12x24_char(6+(Set_Posi_Acc-1)*13,25,BLACK,BLACK,'k',1);
								temp_acc[Set_Posi_Acc-1] = 'k';
								x_pos = 0;
								y_pos = 0;
								break;
				case 8: TFT_Show_12x24_char(6+(Set_Posi_Acc-1)*13,25,BLACK,BLACK,'l',1);
								temp_acc[Set_Posi_Acc-1] = 'l';
								x_pos = 0;
								y_pos = 0;
								break;
				break;
			}
		} 

		if( Check_Posi_Screen(row_x5,175,row_x6,205) ){// Hang Duoi
			Set_Posi_Acc+=1;
			switch(i){
				case 0: TFT_Show_12x24_char(6+(Set_Posi_Acc-1)*13,25,BLACK,BLACK,'z',1);
								temp_acc[Set_Posi_Acc-1] = 'z';
								printf("\n%c\n",temp_acc[Set_Posi_Acc-1]);
								x_pos = 0;
								y_pos = 0;
								break;
				case 1: TFT_Show_12x24_char(6+(Set_Posi_Acc-1)*13,25,BLACK,BLACK,'x',1);
								temp_acc[Set_Posi_Acc-1] = 'x';
								x_pos = 0;
								y_pos = 0;
								break;
				case 2: TFT_Show_12x24_char(6+(Set_Posi_Acc-1)*13,25,BLACK,BLACK,'c',1);
								temp_acc[Set_Posi_Acc-1] = 'c';
								x_pos = 0;
								y_pos = 0;
								break;
				case 3: TFT_Show_12x24_char(6+(Set_Posi_Acc-1)*13,25,BLACK,BLACK,'v',1);
								temp_acc[Set_Posi_Acc-1] = 'v';
								x_pos = 0;
								y_pos = 0;
								break;
				case 4: TFT_Show_12x24_char(6+(Set_Posi_Acc-1)*13,25,BLACK,BLACK,'b',1);
								temp_acc[Set_Posi_Acc-1] = 'b';
								x_pos = 0;
								y_pos = 0;
								break;
				case 5: TFT_Show_12x24_char(6+(Set_Posi_Acc-1)*13,25,BLACK,BLACK,'n',1);
								temp_acc[Set_Posi_Acc-1] = 'n';
								printf("\n%c\n",temp_acc[Set_Posi_Acc-1]);
								x_pos = 0;
								y_pos = 0;
								break;
				case 6: TFT_Show_12x24_char(6+(Set_Posi_Acc-1)*13,25,BLACK,BLACK,'m',1);
								temp_acc[Set_Posi_Acc-1] = 'm';
								x_pos = 0;
								y_pos = 0;
								printf("\nSet_Posi_Acc - m: %d\n",Set_Posi_Acc);
								break;
				break;
			}
			if ( Check_Posi_Screen(290,175,320,205) ){ // press Delete
				int lim_x1 = 0, lim_x2 = 0;
				x_pos = 0;
				y_pos = 0;
				Set_Posi_Acc-=1;
				printf("\nSet_Posi_Acc - Del: %d\n",Set_Posi_Acc);
				if(Set_Posi_Acc <= 0) Set_Posi_Acc = 0;
				lim_x1 = 6+(Set_Posi_Acc-1)*13;
				lim_x2 = (6+(Set_Posi_Acc-1)*13)+14;
				TFT_Clear_Pos(lim_x1, 25, lim_x2, 25+24+1, LIGHTGREY);
				Set_Posi_Acc-=1;
				if(Set_Posi_Acc <= 0) Set_Posi_Acc = 0;
			}
		} 	
	
		if(Check_Posi_Screen( 280, 209, 320 , 240) && vrvi_Flag_InputAccount == 1){ // Press Enter
			printf("Press Enter Account Oki\n");
			vrvi_GetCounter_InputAccount = 0;
//			vrvi_Flag_InputAcc_KeyBoard = 0;
			vrvi_Flag_InputAccount = 0;
			SD_ReadBmp(0,0,320,240,"/nhamdan2022/login_acc.bmp");
			printf("Acc: ");
			memcpy(temp_acc_after,temp_acc,20);
			for(i = 0; i < Set_Posi_Acc; i++){
				printf("%c",temp_acc_after[i]);
				TFT_Show_12x24_char(26+i*12, 62, WHITE, BLACK, temp_acc[i], 1);
				x_pos = 0; y_pos = 0;	
			}			
			//Set_Posi_Acc = 0;
		}	
	}		
}

void Login_Input_PassWord(void){
	for(i = 0; i < 10; i++){
		row_n0 = 2 + i*34;  row_n1 = row_n0 + 30;	// Hang So	
		row_x1 = 2 + i*34;  row_x2 = row_x1 + 30;	// Hang Tren		
		row_x3 = 15 + i*35; row_x4 = row_x3 + 30; // Hang Co So
		row_x5 = 50 + i*35; row_x6 = row_x5 + 30; // Hang Duoi
		
		if( Check_Posi_Screen(row_n0 , 60  , row_n1 , 90 ) ){// Hang So
			Set_Posi_Pass++;
			switch(i){
				case 0: TFT_Print_Num(6+(Set_Posi_Pass-1)*13, 25, BLACK, BLACK, 24, 0, 1, 1, 1);
								temp_acc[Set_Posi_Pass-1] = '1';
								printf("%c",temp_acc[Set_Posi_Pass-1]);
								x_pos = 0;
								y_pos = 0;
								break; 
				case 1: TFT_Print_Num(6+(Set_Posi_Pass-1)*13, 25, BLACK, BLACK, 24, 0, 2, 1, 1);
								temp_acc[Set_Posi_Pass-1] = '2';
								printf("%c",temp_acc[Set_Posi_Pass-1]);
								x_pos = 0;
								y_pos = 0;
								break;
				case 2: TFT_Print_Num(6+(Set_Posi_Pass-1)*13, 25, BLACK, BLACK, 24, 0, 3, 1, 1);
								temp_acc[Set_Posi_Pass-1] = '3';
								printf("%c",temp_acc[Set_Posi_Pass-1]);
								x_pos = 0;
								y_pos = 0;
								break;
				case 3: TFT_Print_Num(6+(Set_Posi_Pass-1)*13, 25, BLACK, BLACK, 24, 0, 4, 1, 1);
								temp_acc[Set_Posi_Pass-1] = '4';
								printf("%c",temp_acc[Set_Posi_Pass-1]);
								x_pos = 0;
								y_pos = 0;
								break;
				case 4: TFT_Print_Num(6+(Set_Posi_Pass-1)*13, 25, BLACK, BLACK, 24, 0, 5, 1, 1);
								temp_acc[Set_Posi_Pass-1] = '5';
								printf("%c",temp_acc[Set_Posi_Pass-1]);
								x_pos = 0;
								y_pos = 0;
								break;
				case 5: TFT_Print_Num(6+(Set_Posi_Pass-1)*13, 25, BLACK, BLACK, 24, 0, 6, 1, 1);
								temp_acc[Set_Posi_Pass-1] = '6';
								printf("%c",temp_acc[Set_Posi_Pass-1]);
								x_pos = 0;
								y_pos = 0;
								break;
				case 6: TFT_Print_Num(6+(Set_Posi_Pass-1)*13, 25, BLACK, BLACK, 24, 0, 7, 1, 1);
								temp_acc[Set_Posi_Pass-1] = '7';
								printf("%c",temp_acc[Set_Posi_Pass-1]);
								x_pos = 0;
								y_pos = 0;
								break;
				case 7: TFT_Print_Num(6+(Set_Posi_Pass-1)*13, 25, BLACK, BLACK, 24, 0, 8, 1, 1);
								temp_acc[Set_Posi_Pass-1] = '8';
								printf("%c",temp_acc[Set_Posi_Pass-1]);
								x_pos = 0;
								y_pos = 0;
								break;
				case 8: TFT_Print_Num(6+(Set_Posi_Pass-1)*13, 25, BLACK, BLACK, 24, 0, 9, 1, 1);
								printf("%c",temp_acc[Set_Posi_Pass-1]);
								temp_acc[Set_Posi_Pass-1] = '9';
								x_pos = 0;
								y_pos = 0;
								break;
				case 9: TFT_Print_Num(6+(Set_Posi_Pass-1)*13, 25, BLACK, BLACK, 24, 0, 0, 1, 1);;
								temp_acc[Set_Posi_Pass-1] = '0';
								printf("%c",temp_acc[Set_Posi_Pass-1]);
								x_pos = 0;
								y_pos = 0;
								break;
				break;
			}
		} // end Hang So
		
		if( Check_Posi_Screen(row_x1 , 95  , row_x2 , 130) ){ // Hang tren
			Set_Posi_Pass++;
			switch(i){
				case 0: TFT_Show_12x24_char(6+(Set_Posi_Pass-1)*13,25,BLACK,BLACK,'q',1);
								temp_acc[Set_Posi_Pass-1] = 'q';
								printf("%c",temp_acc[Set_Posi_Pass-1]);
								x_pos = 0;
								y_pos = 0;
								break; 
				case 1: TFT_Show_12x24_char(6+(Set_Posi_Pass-1)*13,25,BLACK,BLACK,'w',1);
								temp_acc[Set_Posi_Pass-1] = 'w';
								printf("%c",temp_acc[Set_Posi_Pass-1]);
								x_pos = 0;
								y_pos = 0;
								break;
				case 2: TFT_Show_12x24_char(6+(Set_Posi_Pass-1)*13,25,BLACK,BLACK,'e',1);
								temp_acc[Set_Posi_Pass-1] = 'e';
								printf("%c",temp_acc[Set_Posi_Pass-1]);
								x_pos = 0;
								y_pos = 0;
								break;
				case 3: TFT_Show_12x24_char(6+(Set_Posi_Pass-1)*13,25,BLACK,BLACK,'r',1);
								temp_acc[Set_Posi_Pass-1] = 'r';
								printf("%c",temp_acc[Set_Posi_Pass-1]);
								x_pos = 0;
								y_pos = 0;
								break;
				case 4: TFT_Show_12x24_char(6+(Set_Posi_Pass-1)*13,25,BLACK,BLACK,'t',1);
								temp_acc[Set_Posi_Pass-1] = 't';
								printf("\n%c\n",temp_acc[Set_Posi_Pass-1]);
								x_pos = 0;
								y_pos = 0;
								break;
				case 5: TFT_Show_12x24_char(6+(Set_Posi_Pass-1)*13,25,BLACK,BLACK,'y',1);
								temp_acc[Set_Posi_Pass-1] = 'y';
								printf("%c",temp_acc[Set_Posi_Pass-1]);
								x_pos = 0;
								y_pos = 0;
								break;
				case 6: TFT_Show_12x24_char(6+(Set_Posi_Pass-1)*13,25,BLACK,BLACK,'u',1);
								temp_acc[Set_Posi_Pass-1] = 'u';
								printf("%c",temp_acc[Set_Posi_Pass-1]);
								x_pos = 0;
								y_pos = 0;
								break;
				case 7: TFT_Show_12x24_char(6+(Set_Posi_Pass-1)*13,25,BLACK,BLACK,'i',1);
								temp_acc[Set_Posi_Pass-1] = 'i';
								printf("\n%c\n",temp_acc[Set_Posi_Pass-1]);
								x_pos = 0;
								y_pos = 0;
								break;
				case 8: TFT_Show_12x24_char(6+(Set_Posi_Pass-1)*13,25,BLACK,BLACK,'o',1);
								printf("%c",temp_acc[Set_Posi_Pass-1]);
								temp_acc[Set_Posi_Pass-1] = 'o';
								x_pos = 0;
								y_pos = 0;
								break;
				case 9: TFT_Show_12x24_char(6+(Set_Posi_Pass-1)*13,25,BLACK,BLACK,'p',1);
								temp_acc[Set_Posi_Pass-1] = 'p';
								printf("%c",temp_acc[Set_Posi_Pass-1]);
								x_pos = 0;
								y_pos = 0;
								break;
				break;
			}
		} // end Hang tren
		
		if( Check_Posi_Screen(row_x3 , 135 , row_x4 , 170) ){// Hang co so
			Set_Posi_Pass++;
			switch(i){
				case 0: TFT_Show_12x24_char(6+(Set_Posi_Pass-1)*13,25,BLACK,BLACK,'a',1);
								temp_acc[Set_Posi_Pass-1] = 'a';
								printf("\n%c\n",temp_acc[Set_Posi_Pass-1]);
								x_pos = 0;
								y_pos = 0;
								break;
				case 1: TFT_Show_12x24_char(6+(Set_Posi_Pass-1)*13,25,BLACK,BLACK,'s',1);
								temp_acc[Set_Posi_Pass-1] = 's';
								x_pos = 0;
								y_pos = 0;
								break;
				case 2: TFT_Show_12x24_char(6+(Set_Posi_Pass-1)*13,25,BLACK,BLACK,'d',1);
								temp_acc[Set_Posi_Pass-1] = 'd';
								x_pos = 0;
								y_pos = 0;
								break;
				case 3: TFT_Show_12x24_char(6+(Set_Posi_Pass-1)*13,25,BLACK,BLACK,'f',1);
								temp_acc[Set_Posi_Pass-1] = 'f';
								x_pos = 0;
								y_pos = 0;
								break;
				case 4: TFT_Show_12x24_char(6+(Set_Posi_Pass-1)*13,25,BLACK,BLACK,'g',1);
								temp_acc[Set_Posi_Pass-1] = 'g';
								x_pos = 0;
								y_pos = 0;
								break;
				case 5: TFT_Show_12x24_char(6+(Set_Posi_Pass-1)*13,25,BLACK,BLACK,'h',1);
								temp_acc[Set_Posi_Pass-1] = 'h';
								printf("\n%c\n",temp_acc[Set_Posi_Pass-1]);
								x_pos = 0;
								y_pos = 0;
								break;
				case 6: TFT_Show_12x24_char(6+(Set_Posi_Pass-1)*13,25,BLACK,BLACK,'j',1);
								temp_acc[Set_Posi_Pass-1] = 'j';
								x_pos = 0;
								y_pos = 0;
								break;
				case 7: TFT_Show_12x24_char(6+(Set_Posi_Pass-1)*13,25,BLACK,BLACK,'k',1);
								temp_acc[Set_Posi_Pass-1] = 'k';
								x_pos = 0;
								y_pos = 0;
								break;
				case 8: TFT_Show_12x24_char(6+(Set_Posi_Pass-1)*13,25,BLACK,BLACK,'l',1);
								temp_acc[Set_Posi_Pass-1] = 'l';
								x_pos = 0;
								y_pos = 0;
								break;
				break;
			}
		} 

		if( Check_Posi_Screen(row_x5 , 175 , row_x6 , 205) ){// Hang Duoi
			Set_Posi_Pass+=1;
			switch(i){
				case 0: TFT_Show_12x24_char(6+(Set_Posi_Pass-1)*13,25,BLACK,BLACK,'z',1);
								temp_acc[Set_Posi_Pass-1] = 'z';
								printf("\n%c\n",temp_acc[Set_Posi_Pass-1]);
								x_pos = 0;
								y_pos = 0;
								break;
				case 1: TFT_Show_12x24_char(6+(Set_Posi_Pass-1)*13,25,BLACK,BLACK,'x',1);
								temp_acc[Set_Posi_Pass-1] = 'x';
								x_pos = 0;
								y_pos = 0;
								break;
				case 2: TFT_Show_12x24_char(6+(Set_Posi_Pass-1)*13,25,BLACK,BLACK,'c',1);
								temp_acc[Set_Posi_Pass-1] = 'c';
								x_pos = 0;
								y_pos = 0;
								break;
				case 3: TFT_Show_12x24_char(6+(Set_Posi_Pass-1)*13,25,BLACK,BLACK,'v',1);
								temp_acc[Set_Posi_Pass-1] = 'v';
								x_pos = 0;
								y_pos = 0;
								break;
				case 4: TFT_Show_12x24_char(6+(Set_Posi_Pass-1)*13,25,BLACK,BLACK,'b',1);
								temp_acc[Set_Posi_Pass-1] = 'b';
								x_pos = 0;
								y_pos = 0;
								break;
				case 5: TFT_Show_12x24_char(6+(Set_Posi_Pass-1)*13,25,BLACK,BLACK,'n',1);
								temp_acc[Set_Posi_Pass-1] = 'n';
								printf("\n%c\n",temp_acc[Set_Posi_Pass-1]);
								x_pos = 0;
								y_pos = 0;
								break;
				case 6: TFT_Show_12x24_char(6+(Set_Posi_Pass-1)*13,25,BLACK,BLACK,'m',1);
								temp_acc[Set_Posi_Pass-1] = 'm';
								x_pos = 0;
								y_pos = 0;
								printf("\nSet_Posi_Pass - m: %d\n",Set_Posi_Pass);
								break;
				break;
			}
			if ( Check_Posi_Screen(290,175,320,205) ){ // press Delete
				x_pos = 0;
				y_pos = 0;
				Set_Posi_Pass-=1;
				printf("\nSet_Posi_Pass - Del: %d\n",Set_Posi_Pass);
				TFT_Clear_Pos(6+(Set_Posi_Pass-1)*13, 25, (6+(Set_Posi_Pass-1)*13)+12-1, 25+24+1, LIGHTGREY);
				Set_Posi_Pass-=1;
			}
		} 	
	
		if( Check_Posi_Screen(280    , 209 , 320    , 240) && (vrvi_Flag_InputPassWord == 1) ){// Press Enter
			printf("Press Enter PassWord Oki\n");
			vrvi_Flag_InputPassWord = 0;
			SD_ReadBmp(0,0,320,240,"/nhamdan2022/login_pass.bmp");
			printf("Pass: ");				
			
			for(i = 0; i < Set_Posi_Acc; i++){
				printf("%c",temp_acc_after[i]);
				TFT_Show_12x24_char(26+i*12, 62, WHITE, BLACK, temp_acc_after[i], 1);
				x_pos = 0; y_pos = 0;	
			}
			
			for(i = 0; i < Set_Posi_Pass; i++){
				printf("%c",temp_acc[i]);
				TFT_Show_12x24_char(26+i*12, 110, WHITE, BLACK, '*', 1);
				x_pos = 0; y_pos = 0;		
			}
		vrvi_GetCounter_InputPassWord = 0;
		Set_Posi_Pass = 0;	Set_Posi_Acc  = 0;	
		}	
	}		
}

/*
function: Interface_PressKeyBoard
IMGName: /nhamdan2022/labankey_lower.bmp
				 /nhamdan2022/labankey_upper.bmp
*/
void Interface_PressKeyBoard(const TCHAR* IMGName){
	TFT_Gui_Fill(0,0,320,60,LIGHTGREY);
	SD_ReadBmp(0,60,320,180,IMGName);
}


void Interface_Login(void){
	/* Press Input Account */
	if( Check_Posi_Screen(21,62,300,82) ){ // Input Account
		vrvi_GetCounter_InputAccount++;
//		vrvi_Flag_InputAcc_KeyBoard++;
	}
	/* Press Input PassWord */
	if( Check_Posi_Screen( 30, 107, 300 , 135) ){ // Input PassWord
		vrvi_GetCounter_InputPassWord++;
	}
	
	/* Process Input Account */
	if(vrvi_GetCounter_InputAccount == 1){
		vrvi_GetCounter_InputAccount++;
//		vrvi_Flag_InputAcc_KeyBoard++;
//		vrvi_Flag_InputAccount  = 1;
//		vrvi_Flag_InputPassWord = 0;
		x_pos = 0; y_pos = 0;
		printf("\nvrvi_GetCounter_InputAccount: %d\n",vrvi_GetCounter_InputAccount);
//		printf("\vrvi_Flag_InputAcc_KeyBoard: %d\n",vrvi_Flag_InputAcc_KeyBoard);
		Interface_PressKeyBoard("/nhamdan2022/labankey_lower.bmp");
		Delay_ms(10);	
	}
	if(vrvi_GetCounter_InputAccount > 1 && vrvi_Flag_InputAcc_KeyBoard > 1){	
		Login_Input_Account();
	}	
	
	/* Process Input PassWord*/
	if(vrvi_GetCounter_InputPassWord == 1){
		vrvi_GetCounter_InputPassWord++;
//		vrvi_Flag_InputPassWord = 1;
//		vrvi_Flag_InputAccount  = 0;
		x_pos = 0; y_pos = 0;
		printf("\nvrvi_GetCounter_InputPassWord: %d\n",vrvi_GetCounter_InputPassWord);
		Interface_PressKeyBoard("/nhamdan2022/labankey_lower.bmp");
		Delay_ms(10);
	}
	if(vrvi_GetCounter_InputPassWord > 1){	
		Login_Input_PassWord();
	}	
}


void SYS_Init(void){
	SystemInit();
	Delay_init(72);
	TFT_Init();	
	uart_Init();
	touchInit();
	MSD0_SPI_Configuration();
	Timer_Init();
	printf("Do Van Thai\n");
//	Button();
	
//	TFT_Gui_Fill(0,0,320,60,LIGHTGREY);//-- OKI
//	SD_ScanfFile();//-- OKI
//	TFT_Gui_Img(10,10,180,242,gImage_thaidv);//-- OKI
	SD_ReadBmp(0,0,320,240,"/nhamdan2022/login.bmp"); //-- OKI
//	TFT_Print(26, 68,WHITE, BLACK, 16,"Do Van Thai" , 1);
//	SD_ReadBmp(0,60,320,180,"keyboard16b.bmp"); //-- OKI
//	TFT_Gui_Img(10,10,100,100,data1);
//	strTObyte();//-- OKI
/*********************************************************************/

}



/********************************* version 1 *********************************/ 
void SYS_Run(void){

	
	if(vri_FlagTimer2){
		Test_TouchV1();
	}
	Interface_Login();
	
	
	
//	if( Check_Posi_Screen(21,62,300,82) ){ // Input Account
//		TFT_Gui_Fill(0,0,320,60,LIGHTGREY);
//		SD_ReadBmp(0,60,320,180,"/nhamdan2022/labankey_lower.bmp");
//		vri_FlagLBAccount = 1;
//		printf("vri_FlagLBAccount: %d\n",vri_FlagLBAccount);
//		Delay_ms(200);
//		x_pos = 0;
//		y_pos = 0;
//	}
//	else if(Check_Posi_Screen( 30, 107, 300 , 135)){ // Input Pass
//		TFT_Gui_Fill(0,0,320,60,LIGHTGREY);
//		SD_ReadBmp(0,60,320,180,"/nhamdan2022/labankey_lower.bmp");
//		vri_FlagLBPassword = 1;
//		printf("vri_FlagLBPassword: %d\n",vri_FlagLBPassword);
//		Delay_ms(200);
//		x_pos = 0;
//		y_pos = 0;
//	}
//		
//	else if( Check_Posi_Screen(2,178,36,210) ){ 
//		TFT_Gui_Fill(0,0,320,60,LIGHTGREY);
//		SD_ReadBmp(0,60,320,180,"/nhamdan2022/labankey_upper.bmp");
//		x_pos = 0;
//		y_pos = 0;
//	}
//	
//	else if(Check_Posi_Screen( 280, 209, 320 , 240)){ // press ENTER
//		if(Input_Account){
//			Input_Account = 0;
//			SD_ReadBmp(0,0,320,240,"/nhamdan2022/login_acc.bmp");
//			printf("Acc: ");
//			for(i = 0; i < Set_Posi_Acc; i++){
//				printf("%c",temp_acc[i]);
//				TFT_Show_12x24_char(26+i*12, 62, WHITE, BLACK, temp_acc[i], 1);
//				x_pos = 0;
//				y_pos = 0;
//				Delay_ms(10);		
//			}
//			Set_Posi_Acc = 0;
//		}
//		else if(Input_Password){
//			printf("Pass: ");
//			Input_Password = 0;
//			for(i = 0; i < Set_Posi_Pass; i++){
//				printf("%c",temp_pass[i]);
//				TFT_Show_12x24_char(26+i*12, 102, WHITE, BLACK, temp_pass[i], 1);
//				x_pos = 0;
//				y_pos = 0;
//				Delay_ms(10);
//				
//			}
//			Set_Posi_Pass = 0;
//			
//		}
//	}
//	
//	if(vri_FlagLBAccount){ // input account
//		Input_Account = 1;
//		for(i = 0; i < 10; i++){
//			x1 = 2 + i*34; x2 = x1 + 30;	// Hang Tren		
//			x3 = 15 + i*35; x4 = x3 + 30; // Hang Co So
//			x5 = 50 + i*35; x6 = x5 + 30; // Hang Duoi
//			if( Check_Posi_Screen(x1,100,x2,130) ){ // Hang tren
//				Set_Posi_Acc++;
//				switch(i){
//					case 0: TFT_Show_12x24_char(6+Set_Posi_Acc*13,25,BLACK,BLACK,'q',1);
//									temp_acc[Set_Posi_Acc-1] = 'q';
//									printf("%c",temp_acc[Set_Posi_Acc-1]);
//									x_pos = 0;
//									y_pos = 0;
//									break; 
//					case 1: TFT_Show_12x24_char(6+Set_Posi_Acc*13,25,BLACK,BLACK,'w',1);
//									temp_acc[Set_Posi_Acc-1] = 'w';
//									printf("%c",temp_acc[Set_Posi_Acc-1]);
//									x_pos = 0;
//									y_pos = 0;
//									break;
//					case 2: TFT_Show_12x24_char(6+Set_Posi_Acc*13,25,BLACK,BLACK,'e',1);
//									temp_acc[Set_Posi_Acc-1] = 'e';
//									printf("%c",temp_acc[Set_Posi_Acc-1]);
//									x_pos = 0;
//									y_pos = 0;
//									break;
//					case 3: TFT_Show_12x24_char(6+Set_Posi_Acc*13,25,BLACK,BLACK,'r',1);
//									temp_acc[Set_Posi_Acc-1] = 'r';
//									printf("%c",temp_acc[Set_Posi_Acc-1]);
//									x_pos = 0;
//									y_pos = 0;
//									break;
//					case 4: TFT_Show_12x24_char(6+Set_Posi_Acc*13,25,BLACK,BLACK,'t',1);
//									temp_acc[Set_Posi_Acc-1] = 't';
//									printf("\n%c\n",temp_acc[Set_Posi_Acc-1]);
//									x_pos = 0;
//									y_pos = 0;
//									break;
//					case 5: TFT_Show_12x24_char(6+Set_Posi_Acc*13,25,BLACK,BLACK,'y',1);
//									temp_acc[Set_Posi_Acc-1] = 'y';
//									printf("%c",temp_acc[Set_Posi_Acc-1]);
//									x_pos = 0;
//									y_pos = 0;
//									break;
//					case 6: TFT_Show_12x24_char(6+Set_Posi_Acc*13,25,BLACK,BLACK,'u',1);
//									temp_acc[Set_Posi_Acc-1] = 'u';
//									printf("%c",temp_acc[Set_Posi_Acc-1]);
//									x_pos = 0;
//									y_pos = 0;
//									break;
//					case 7: TFT_Show_12x24_char(6+Set_Posi_Acc*13,25,BLACK,BLACK,'i',1);
//									temp_acc[Set_Posi_Acc-1] = 'i';
//									printf("\n%c\n",temp_acc[Set_Posi_Acc-1]);
//									x_pos = 0;
//									y_pos = 0;
//									break;
//					case 8: TFT_Show_12x24_char(6+Set_Posi_Acc*13,25,BLACK,BLACK,'o',1);
//									printf("%c",temp_acc[Set_Posi_Acc-1]);
//									temp_acc[Set_Posi_Acc-1] = 'o';
//									x_pos = 0;
//									y_pos = 0;
//									break;
//					case 9: TFT_Show_12x24_char(6+Set_Posi_Acc*13,25,BLACK,BLACK,'p',1);
//									temp_acc[Set_Posi_Acc-1] = 'p';
//									printf("%c",temp_acc[Set_Posi_Acc-1]);
//									x_pos = 0;
//									y_pos = 0;
//									break;
//					break;
//				}
//			} // end Hang tren
//			
//			if( Check_Posi_Screen(x3,140,x4,170) ){// Hang co so
//				Set_Posi_Acc++;
//				switch(i){
//					case 0: TFT_Show_12x24_char(6+Set_Posi_Acc*13,25,BLACK,BLACK,'a',1);
//									temp_acc[Set_Posi_Acc-1] = 'a';
//									printf("\n%c\n",temp_acc[Set_Posi_Acc-1]);
//									x_pos = 0;
//									y_pos = 0;
//									break;
//					case 1: TFT_Show_12x24_char(6+Set_Posi_Acc*13,25,BLACK,BLACK,'s',1);
//									temp_acc[Set_Posi_Acc-1] = 's';
//									x_pos = 0;
//									y_pos = 0;
//									break;
//					case 2: TFT_Show_12x24_char(6+Set_Posi_Acc*13,25,BLACK,BLACK,'d',1);
//									temp_acc[Set_Posi_Acc-1] = 'd';
//									x_pos = 0;
//									y_pos = 0;
//									break;
//					case 3: TFT_Show_12x24_char(6+Set_Posi_Acc*13,25,BLACK,BLACK,'f',1);
//									temp_acc[Set_Posi_Acc-1] = 'f';
//									x_pos = 0;
//									y_pos = 0;
//									break;
//					case 4: TFT_Show_12x24_char(6+Set_Posi_Acc*13,25,BLACK,BLACK,'g',1);
//									temp_acc[Set_Posi_Acc-1] = 'g';
//									x_pos = 0;
//									y_pos = 0;
//									break;
//					case 5: TFT_Show_12x24_char(6+Set_Posi_Acc*13,25,BLACK,BLACK,'h',1);
//									temp_acc[Set_Posi_Acc-1] = 'h';
//									printf("\n%c\n",temp_acc[Set_Posi_Acc-1]);
//									x_pos = 0;
//									y_pos = 0;
//									break;
//					case 6: TFT_Show_12x24_char(6+Set_Posi_Acc*13,25,BLACK,BLACK,'j',1);
//									temp_acc[Set_Posi_Acc-1] = 'j';
//									x_pos = 0;
//									y_pos = 0;
//									break;
//					case 7: TFT_Show_12x24_char(6+Set_Posi_Acc*13,25,BLACK,BLACK,'k',1);
//									temp_acc[Set_Posi_Acc-1] = 'l';
//									x_pos = 0;
//									y_pos = 0;
//									break;
//					break;
//				}
//			} 
//	
//			if( Check_Posi_Screen(x5,180,x6,210) ){// Hang co so
//				Set_Posi_Acc+=1;
//				switch(i){
//					case 0: TFT_Show_12x24_char(6+Set_Posi_Acc*13,25,BLACK,BLACK,'z',1);
//									temp_acc[Set_Posi_Acc-1] = 'z';
//									printf("\n%c\n",temp_acc[Set_Posi_Acc-1]);
//									x_pos = 0;
//									y_pos = 0;
//									break;
//					case 1: TFT_Show_12x24_char(6+Set_Posi_Acc*13,25,BLACK,BLACK,'x',1);
//									temp_acc[Set_Posi_Acc-1] = 'x';
//									x_pos = 0;
//									y_pos = 0;
//									break;
//					case 2: TFT_Show_12x24_char(6+Set_Posi_Acc*13,25,BLACK,BLACK,'c',1);
//									temp_acc[Set_Posi_Acc-1] = 'c';
//									x_pos = 0;
//									y_pos = 0;
//									break;
//					case 3: TFT_Show_12x24_char(6+Set_Posi_Acc*13,25,BLACK,BLACK,'v',1);
//									temp_acc[Set_Posi_Acc-1] = 'v';
//									x_pos = 0;
//									y_pos = 0;
//									break;
//					case 4: TFT_Show_12x24_char(6+Set_Posi_Acc*13,25,BLACK,BLACK,'b',1);
//									temp_acc[Set_Posi_Acc-1] = 'b';
//									x_pos = 0;
//									y_pos = 0;
//									break;
//					case 5: TFT_Show_12x24_char(6+Set_Posi_Acc*13,25,BLACK,BLACK,'n',1);
//									temp_acc[Set_Posi_Acc-1] = 'n';
//									printf("\n%c\n",temp_acc[Set_Posi_Acc-1]);
//									x_pos = 0;
//									y_pos = 0;
//									break;
//					case 6: TFT_Show_12x24_char(6+Set_Posi_Acc*13,25,BLACK,BLACK,'m',1);
//									temp_acc[Set_Posi_Acc-1] = 'm';
//									x_pos = 0;
//									y_pos = 0;
//									break;
//					break;
//				}
//			} 		
//		}		
//	}
//	/**********************************************************************************************************************************/
//	/**********************************************************************************************************************************/
//	/**********************************************************************************************************************************/
//	if(vri_FlagLBPassword){ // input pass
//		//printf("mode input pass\n");
//		for(i = 0; i < 10; i++){
//			y1 = 2 +  i*34;  y2 = y1 + 30;	// Hang Tren		
//			y3 = 15 + i*35; y4 = y3 + 30; // Hang Co So
//			y5 = 50 + i*35; y6 = y5 + 30; // Hang Duoi
//			if( Check_Posi_Screen(y1,100,y2,130) ){ // Hang tren
//				Set_Posi_Pass+=1;
//				switch(i){
//					case 0: TFT_Show_12x24_char(6+Set_Posi_Pass*13,25,BLACK,BLACK,'q',1);
//									temp_pass[Set_Posi_Pass-1] = 'q';
//									x_pos = 0;
//									y_pos = 0;
//									break; 
//					case 1: TFT_Show_12x24_char(6+Set_Posi_Pass*13,25,BLACK,BLACK,'w',1);
//									temp_pass[Set_Posi_Pass-1] = 'w';
//									x_pos = 0;
//									y_pos = 0;
//									break;
//					case 2: TFT_Show_12x24_char(6+Set_Posi_Pass*13,25,BLACK,BLACK,'e',1);
//									temp_pass[Set_Posi_Pass-1] = 'e';
//									x_pos = 0;
//									y_pos = 0;
//									break;
//					case 3: TFT_Show_12x24_char(6+Set_Posi_Pass*13,25,BLACK,BLACK,'r',1);
//									temp_pass[Set_Posi_Pass-1] = 'r';
//									x_pos = 0;
//									y_pos = 0;
//									break;
//					case 4: TFT_Show_12x24_char(6+Set_Posi_Pass*13,25,BLACK,BLACK,'t',1);
//									temp_pass[Set_Posi_Pass-1] = 't';
//									printf("\n%c\n",temp_pass[Set_Posi_Pass-1]);
//									x_pos = 0;
//									y_pos = 0;
//									break;
//					case 5: TFT_Show_12x24_char(6+Set_Posi_Pass*13,25,BLACK,BLACK,'y',1);
//									temp_pass[Set_Posi_Pass-1] = 'y';
//									x_pos = 0;
//									y_pos = 0;
//									break;
//					case 6: TFT_Show_12x24_char(6+Set_Posi_Pass*13,25,BLACK,BLACK,'u',1);
//									temp_pass[Set_Posi_Pass-1] = 'u';
//									x_pos = 0;
//									y_pos = 0;
//									break;
//					case 7: TFT_Show_12x24_char(6+Set_Posi_Pass*13,25,BLACK,BLACK,'i',1);
//									temp_pass[Set_Posi_Pass-1] = 'i';
//									printf("\n%c\n",temp_pass[Set_Posi_Pass-1]);
//									x_pos = 0;
//									y_pos = 0;
//									break;
//					case 8: TFT_Show_12x24_char(6+Set_Posi_Pass*13,25,BLACK,BLACK,'o',1);
//									temp_pass[Set_Posi_Pass-1] = 'o';
//									x_pos = 0;
//									y_pos = 0;
//									break;
//					case 9: TFT_Show_12x24_char(6+Set_Posi_Pass*13,25,BLACK,BLACK,'p',1);
//									temp_pass[Set_Posi_Pass-1] = 'p';
//									x_pos = 0;
//									y_pos = 0;
//									break;
//					break;
//				}
//			} // end Hang tren
//			
//			if( Check_Posi_Screen(y3,140,y4,170) ){// Hang co so
//				Set_Posi_Pass+=1;
//				switch(i){
//					case 0: TFT_Show_12x24_char(6+Set_Posi_Pass*13,25,BLACK,BLACK,'a',1);
//									temp_pass[Set_Posi_Pass-1] = 'a';
//									printf("\n%c\n",temp_pass[Set_Posi_Pass-1]);
//									x_pos = 0;
//									y_pos = 0;
//									break;
//					case 1: TFT_Show_12x24_char(6+Set_Posi_Pass*13,25,BLACK,BLACK,'s',1);
//									temp_pass[Set_Posi_Pass-1] = 's';
//									x_pos = 0;
//									y_pos = 0;
//									break;
//					case 2: TFT_Show_12x24_char(6+Set_Posi_Pass*13,25,BLACK,BLACK,'d',1);
//									temp_pass[Set_Posi_Pass-1] = 'd';
//									x_pos = 0;
//									y_pos = 0;
//									break;
//					case 3: TFT_Show_12x24_char(6+Set_Posi_Pass*13,25,BLACK,BLACK,'f',1);
//									temp_pass[Set_Posi_Pass-1] = 'f';
//									x_pos = 0;
//									y_pos = 0;
//									break;
//					case 4: TFT_Show_12x24_char(6+Set_Posi_Pass*13,25,BLACK,BLACK,'g',1);
//									temp_pass[Set_Posi_Pass-1] = 'g';
//									x_pos = 0;
//									y_pos = 0;
//									break;
//					case 5: TFT_Show_12x24_char(6+Set_Posi_Pass*13,25,BLACK,BLACK,'h',1);
//									temp_pass[Set_Posi_Pass-1] = 'h';
//									printf("\n%c\n",temp_pass[Set_Posi_Pass-1]);
//									x_pos = 0;
//									y_pos = 0;
//									break;
//					case 6: TFT_Show_12x24_char(6+Set_Posi_Pass*13,25,BLACK,BLACK,'j',1);
//									temp_pass[Set_Posi_Pass-1] = 'j';
//									x_pos = 0;
//									y_pos = 0;
//									break;
//					case 7: TFT_Show_12x24_char(6+Set_Posi_Pass*13,25,BLACK,BLACK,'k',1);
//									temp_pass[Set_Posi_Pass-1] = 'l';
//									x_pos = 0;
//									y_pos = 0;
//									break;
//					break;
//				}
//			} 
//	
//			if( Check_Posi_Screen(y5,180,y6,210) ){// Hang co so
//				Set_Posi_Pass+=1;
//				switch(i){
//					case 0: TFT_Show_12x24_char(6+Set_Posi_Pass*13,25,BLACK,BLACK,'z',1);
//									temp_pass[Set_Posi_Pass-1] = 'z';
//									printf("\n%c\n",temp_pass[Set_Posi_Pass-1]);
//									x_pos = 0;
//									y_pos = 0;
//									break;
//					case 1: TFT_Show_12x24_char(6+Set_Posi_Pass*13,25,BLACK,BLACK,'x',1);
//									temp_pass[Set_Posi_Pass-1] = 'x';
//									x_pos = 0;
//									y_pos = 0;
//									break;
//					case 2: TFT_Show_12x24_char(6+Set_Posi_Pass*13,25,BLACK,BLACK,'c',1);
//									temp_pass[Set_Posi_Pass-1] = 'c';
//									x_pos = 0;
//									y_pos = 0;
//									break;
//					case 3: TFT_Show_12x24_char(6+Set_Posi_Pass*13,25,BLACK,BLACK,'v',1);
//									temp_pass[Set_Posi_Pass-1] = 'v';
//									x_pos = 0;
//									y_pos = 0;
//									break;
//					case 4: TFT_Show_12x24_char(6+Set_Posi_Pass*13,25,BLACK,BLACK,'b',1);
//									temp_pass[Set_Posi_Pass-1] = 'b';
//									x_pos = 0;
//									y_pos = 0;
//									break;
//					case 5: TFT_Show_12x24_char(6+Set_Posi_Pass*13,25,BLACK,BLACK,'n',1);
//									temp_pass[Set_Posi_Pass-1] = 'n';
//									printf("\n%c\n",temp_pass[Set_Posi_Pass-1]);
//									x_pos = 0;
//									y_pos = 0;
//									break;
//					case 6: TFT_Show_12x24_char(6+Set_Posi_Pass*13,25,BLACK,BLACK,'m',1);
//									temp_pass[Set_Posi_Pass-1] = 'm';
//									x_pos = 0;
//									y_pos = 0;
//									break;
//					break;
//				}
//			} 		
//		}		
//	}
	
}
/********************************* end version 1 *********************************/

/********************************* version 2 *********************************/
//char temp[20];
//u8 *str;
//void SYS_Run(void){
//	int x1 = 0, x2 = 0;
//	if(vri_FlagTimer2){
//		Test_TouchV1();
//	}
//	if( Check_Posi_Screen(21,62,300,82) ){
//		TFT_Gui_Fill(0,0,320,60,LIGHTGREY);
//		SD_ReadBmp(0,60,320,180,"/nhamdan2022/labankey_lower.bmp");
//		vri_FlagLBLower = 1;
//		x_pos = 0;
//			y_pos = 0;
//	}
//	else if( Check_Posi_Screen(2,178,36,210) ){
//		TFT_Gui_Fill(0,0,320,60,LIGHTGREY);
//		SD_ReadBmp(0,60,320,180,"/nhamdan2022/labankey_upper.bmp");
//		x_pos = 0;
//		y_pos = 0;
//	}
//	else if(Check_Posi_Screen( 280, 209,320 , 240)){
//		SD_ReadBmp(0,0,320,240,"/nhamdan2022/login_acc.bmp");
//		for(i = 0; i < Set_Posi_key; i++){
//			printf("%c",temp[i]);
//			TFT_Show_12x24_char(26+i*12, 58, WHITE, BLACK, temp[i], 1);
//		}

//		Set_Posi_key = 0;
//		x_pos = 0;
//		y_pos = 0;
//		Delay_ms(100);
//	}
//	
//	if(vri_FlagLBLower){
//		for(i = 0; i < 10; i++){
//			x1 = 2 + i*34;
//			x2 = x1 + 30;
//			if( Check_Posi_Screen(x1,100,x2,130) ){
//				Set_Posi_key++;
//				switch(i){
//					case 0: TFT_Show_12x24_char(6+Set_Posi_key*13,25,BLACK,BLACK,'q',1);
//									temp[Set_Posi_key-1] = 'q';
//									x_pos = 0;
//									y_pos = 0;
//									break;
//					case 1: TFT_Show_12x24_char(6+Set_Posi_key*13,25,BLACK,BLACK,'w',1);
//									temp[Set_Posi_key] = 'w';
//									x_pos = 0;
//									y_pos = 0;
//									break;
//					case 2: TFT_Show_12x24_char(6+Set_Posi_key*13,25,BLACK,BLACK,'e',1);
//									temp[Set_Posi_key] = 'e';
//									x_pos = 0;
//									y_pos = 0;
//									break;
//					case 3: TFT_Show_12x24_char(6+Set_Posi_key*13,25,BLACK,BLACK,'r',1);
//									temp[Set_Posi_key] = 'r';
//									x_pos = 0;
//									y_pos = 0;
//									break;
//					case 4: TFT_Show_12x24_char(6+Set_Posi_key*13,25,BLACK,BLACK,'t',1);
//									temp[Set_Posi_key] = 't';
//									x_pos = 0;
//									y_pos = 0;
//									break;
//					case 5: TFT_Show_12x24_char(6+Set_Posi_key*13,25,BLACK,BLACK,'y',1);
//									temp[Set_Posi_key] = 'y';
//									x_pos = 0;
//									y_pos = 0;
//									break;
//					case 6: TFT_Show_12x24_char(6+Set_Posi_key*13,25,BLACK,BLACK,'u',1);
//									temp[Set_Posi_key] = 'u';
//									x_pos = 0;
//									y_pos = 0;
//									break;
//					case 7: TFT_Show_12x24_char(6+Set_Posi_key*13,25,BLACK,BLACK,'i',1);
//									temp[Set_Posi_key] = 'i';
//									x_pos = 0;
//									y_pos = 0;
//									break;
//					case 8: TFT_Show_12x24_char(6+Set_Posi_key*13,25,BLACK,BLACK,'o',1);
//									temp[Set_Posi_key] = 'o';
//									x_pos = 0;
//									y_pos = 0;
//									break;
//					case 9: TFT_Show_12x24_char(6+Set_Posi_key*13,25,BLACK,BLACK,'p',1);
//									temp[Set_Posi_key] = 'p';
//									x_pos = 0;
//									y_pos = 0;
//									break;
//					break;
//				}
//			}
//		}
//	}
//}
/********************************* end version 2 *********************************/
