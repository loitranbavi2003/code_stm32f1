#ifndef _SYSTEM_H_
#define _SYSTEM_H_
#include <stm32f10x.h>
#include "ff.h"
#include "diskio.h"
#include "lib_sdcard.h"

void LED_Run(void);
void Test_Fill(void);
void Test_FillRec(void);
void Test_Circle(void);
void Test_Triangle(void);

void Test_font_Char(void);
void Test_Font_String(void);
void Test_Font_String_Bold(void);
void Test_Print_Num(void);
void Test_Main(void);

void Button(void);
void Test_TouchV1(void);
void KeyBoard(void);
void SD_ReadBmp(u16 x, u16 y, u16 picxel_X, u16 picxel_Y, const TCHAR* Fname);
void SD_ReadBmp_Normal(void);

void SYS_Init(void);
void SYS_Run(void);



#endif


