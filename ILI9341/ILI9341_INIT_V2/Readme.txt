TFT ILI9341 version 2.0
tính năng: 
	1. hiển thị chữ(in thường, in đậm):
		- In thường có các size: 8,12,16,24,32,48
		- In đậm có các size   : 16,24,32
	2. vẽ hình khối:
		- đường thẳng,
		- hình tam giác, tròn, vuông....
	3. hiển thị hình ảnh ( ảnh 16 bit - 565 ): TFT_Gui_Img;
	4. đọc ảnh từ thẻ nhớ: SD_ReadBmp; 
tình trạng: đã hoàn thành các tính năng trên
thời gian : thứ 6, 20/01/2022 - 9h27