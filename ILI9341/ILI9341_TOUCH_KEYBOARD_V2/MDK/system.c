#include "stm32f10x.h"                  // Device header
#include "stm32f10x_gpio.h"             // Keil::Device:StdPeriph Drivers:GPIO
#include "stm32f10x_rcc.h"              // Keil::Device:StdPeriph Drivers:RCC
#include "stm32f10x_spi.h"              // Keil::Device:StdPeriph Drivers:SPI
#include "stm32f10x_tim.h"              // Keil::Device:StdPeriph Drivers:TIM
#include "ILI9341_Gui.h"
#include "ILI9341_Driver.h"
#include "ILI9341_Define.h"
#include "touchscreen.h"
#include "SPI_MSD0_Driver.h"
#include "ff.h"
#include "diskio.h"
#include "lib_sdcard.h"
#include "delay_sys.h"
#include "keyboard.h"
#include "usart.h"
#include "stdio.h"
#include "timer.h"
#include "string.h"

u16 ColorTab[5]={RED,GREEN,BLUE,YELLOW,MAGENTA};
FATFS fs, Fn;        		 /* Khu v?c l�m vi?c (h? th?ng t?p) cho ? dia logic */
FRESULT res;
FIL fsrc, fdst;      /* file objects */
UINT br,bw;
int k = 0, i = 0, j = 0;
unsigned char buffer1[10000];
//Test_touchV1
char counter_buff[30];
uint16_t x_pos = 0;
uint16_t y_pos = 0;				
uint16_t position_array[2];	

void Test_FillRec(void){
	u8 i=0;
	TFT_Gui_Fill(0,20,ili_dev.width,ili_dev.height-20,WHITE);
	for(i=0; i<5; i++){
		POINT_COLOR = ColorTab[i];
		TFT_DrawRectangle(ili_dev.width/2-80+(i*15),ili_dev.height/2-80+(i*15),ili_dev.width/2-80+(i*15)+60,ili_dev.height/2-80+(i*15)+60); 
	}
	Delay_ms(500);	
	TFT_Gui_Fill(0,20,ili_dev.width,ili_dev.height-20,WHITE); 
	for (i=0; i<5; i++){
		POINT_COLOR = ColorTab[i];
		TFT_Gui_Fill(ili_dev.width/2-80+(i*15),ili_dev.height/2-80+(i*15),ili_dev.width/2-80+(i*15)+60,ili_dev.height/2-80+(i*15)+60,POINT_COLOR); 
	}
	Delay_ms(500);
}
/*********************************************************************************/
void Test_Fill(void){
	TFT_Gui_Fill(0,0,ili_dev.width,ili_dev.height,WHITE);
	Delay_ms(1000);
	TFT_Gui_Fill(0,0,ili_dev.width,ili_dev.height,RED);
	Delay_ms(1000);
	TFT_Gui_Fill(0,0,ili_dev.width,ili_dev.height,GREEN);
	Delay_ms(1000);
	TFT_Gui_Fill(0,0,ili_dev.width,ili_dev.height,BLUE);
}
/*********************************************************************************/
void Test_Circle(void){
	u8 i=0;
	TFT_Gui_Fill(0,20,ili_dev.width,ili_dev.height-20,WHITE);
	for (i=0; i<5; i++){  
		TFT_Gui_Circle(ili_dev.width/2-80+(i*25),ili_dev.height/2-50+(i*25),ColorTab[i],30,0);
	}
	Delay_ms(500);	
	TFT_Gui_Fill(0,20,ili_dev.width,ili_dev.height-20,WHITE); 
	for (i=0; i<5; i++){ 
		TFT_Gui_Circle(ili_dev.width/2-80+(i*25),ili_dev.height/2-50+(i*25),ColorTab[i],30,1);
	}
	Delay_ms(500);
	
}
/*********************************************************************************/
void Test_Triangle(void){
	u8 i=0;
	TFT_Gui_Fill(0,20,ili_dev.width,ili_dev.height-20,WHITE);
	for(i=0;i<5;i++){
		POINT_COLOR=ColorTab[i];
		TFT_Gui_Draw_Triangel(ili_dev.width/2-80+(i*20),ili_dev.height/2-20+(i*15),ili_dev.width/2-50-1+(i*20),ili_dev.height/2-20-52-1+(i*15),ili_dev.width/2-20-1+(i*20),ili_dev.height/2-20+(i*15));
	}
	Delay_ms(500);	
	TFT_Gui_Fill(0,20,ili_dev.width,ili_dev.height-20,WHITE); 
	for(i=0;i<5;i++){
		POINT_COLOR=ColorTab[i];
		ILI9431_Gui_Fill_Triangel(ili_dev.width/2-80+(i*20),ili_dev.height/2-20+(i*15),ili_dev.width/2-50-1+(i*20),ili_dev.height/2-20-52-1+(i*15),ili_dev.width/2-20-1+(i*20),ili_dev.height/2-20+(i*15));
	}
	Delay_ms(500);
}
/*********************************************************************************/
void Test_font_Char(void){
//	TFT_Show_6x8_char(10,10,BLACK,WHITE,'T',1);
	TFT_Show_6x12_char(10,20,BLACK,WHITE,'A',1);
	TFT_Show_8x16_char(10,40,BLACK,WHITE,'A',1);
	TFT_Show_12x24_char(20,60,BLACK,WHITE,'A',1);
	TFT_Show_16x32_char(50,90,BLACK,YELLOW,'A',0);
	TFT_Show_24x48_char(20,120,BLACK,YELLOW,'A',0);
}
/*********************************************************************************/
void Test_Font_String(void){ // In Thuong
//	TFT_Print(10,10,BLACK,WHITE, 8,"Thai Salem",1);
	TFT_Print(10,20,BLACK,WHITE,12,"Thai Salem",1);
	TFT_Print(10,40,BLACK,WHITE,16,"Thai Salem",1);
	TFT_Print(10,60,BLACK,WHITE,24,"Thai Salem",1);
	TFT_Print(10,90,BLACK,WHITE,48,"Thai Salem",1);
	Delay_ms(1000);
}
/*********************************************************************************/
void Test_Font_String_Bold(void){ // In Dam
	TFT_Print_Bold(20,20,BLACK,WHITE,16,"Thai MCU",1);
	TFT_Print_Bold(20,50,BLACK,WHITE,24,"Thai Salem",1);
	TFT_Print_Bold(20,70,BLACK,WHITE,32,"22/05/2000",1);
}
/*********************************************************************************/
void Test_Print_Num(void){
	TFT_Print_Num(100,100,BLACK,YELLOW,32,1,220500,6,1); // IN Dam
	TFT_Print_Num(130,130,BLACK,YELLOW,24,0,60700,5,1); // In Thuong
}
/*********************************************************************************/

void Button(void){
	POINT_COLOR = RED;
	TFT_DrawRectangle(20,50,80,110); 
	TFT_Gui_Fill(20,50,80,110,BLUE);
	TFT_Print_Center(20,50,80,110,BLACK,BLUE,32,BOLD,"ON",1);
	POINT_COLOR = BLUE;
	TFT_DrawRectangle(160,50,220,110); 
	TFT_Gui_Fill(160,50,220,110,RED);
	TFT_Print_Center(160,50,220,110,BLACK,BLUE,32,BOLD,"OFF",1);
	Delay_ms(1);
}
/*********************************************************************************/
void Test_TouchV1(void){
	if(TP_Touchpad_Pressed()){				
		if(TP_Read_Coordinates(position_array) == TOUCHPAD_DATA_OK){
			BT_IRQ_Pin.vruc_FlagChange = 1;
			x_pos = position_array[0];
			y_pos = position_array[1];		
				/* Hien thi vi tri cam ung */			
			//sprintf(counter_buff, "POS X: %.3d", x_pos);						
			//TFT_Print(10, 10, BLACK, WHITE, 16, (char*)counter_buff, 1);					
			//sprintf(counter_buff, "POS Y: %.3d", y_pos);						
			//TFT_Print(10, 25, BLACK, WHITE, 16, (char*)counter_buff, 1);	
			printf("X: %d\n",x_pos);
			printf("Y: %d\n",y_pos);
			printf("**************************\n");
			Delay_ms(10);
//			TFT_Clear_Pos(0,0,320,60,LIGHTGREY);
//			TFT_Clear(WHITE);
		}	
	}
//	if((x_pos > 20 && x_pos < 80) && (y_pos > 50 && y_pos < 110)){
//		TFT_Clear_Pos(0, 200, 240, 280,WHITE);
//		TFT_Print_Center(0, 200, 240, 270,  BLACK, WHITE, 24, NORMAL, "Do Van Thai", 1);
//	}	
//	else if((x_pos > 160 && x_pos < 220) && (y_pos > 50 && y_pos < 110)){
//		TFT_Clear_Pos(0, 200, 240, 280,WHITE);
//		TFT_Print_Center(0, 200, 240, 270,  BLACK, WHITE, 24, NORMAL, "Vu Thi Thu Ha", 1);
//	}
//	x_pos = y_pos = 0;
}
/*********************************************************************************/
void SD_ReadBmp(u16 x, u16 y, u16 picxel_X, u16 picxel_Y, const TCHAR* Fname){
	int m = 0;
	int npicxel = picxel_X*picxel_Y*2;
	int nByte = npicxel/(picxel_Y/10);

	res = f_mount(0,&fs);
	res = f_open(&fsrc, Fname, FA_OPEN_EXISTING | FA_READ);
	
	// Doc 70 bytes dau
	res = f_read(&fsrc, buffer1,70, &br);
	for(i=0;i<sizeof(buffer1);i++) buffer1[i]=0;
	for(m = 0; m < picxel_Y/10; m++){
		res = f_read(&fsrc, buffer1, nByte, &br);
		TFT_Gui_Img(x,y+m*10,picxel_X,10, buffer1);
		for(i=0;i<sizeof(buffer1);i++) buffer1[i]=0;
	}
	f_close(&fsrc);
	Delay_ms(6000);
}
/*********************************************************************************/
void SD_ReadBmp_Normal(void){
	res = f_mount(0,&fs);							// G?n k?t h? th?ng t?p	
	res = f_open(&fsrc,"login16b.bmp", FA_OPEN_EXISTING | FA_READ);
	
	// Doc 70 bytes dau
	res = f_read(&fsrc, buffer1,70, &br);     /* Read a chunk of src file */

	for(i=0;i<sizeof(buffer1);i++) buffer1[i]=0;

	// Doc cac bytes con lai va hien thi len LCD
	for(k = 0; k < 24; k++){
		res = f_read(&fsrc, buffer1,6400, &br);     /* Read a chunk of src file */
		Gui_DrawbmpUser(0,k*10,320,10,buffer1);
		for(i=0;i<sizeof(buffer1);i++) buffer1[i]=0;
	}	
	f_close(&fsrc);
}
/*********************************************************************************/
struct frame{

	unsigned char Data[100];

};
struct frame fr;
/*********************************************************************************/
unsigned char Rbuffer[5000];
void strTObyte(){ // ghi lieu vao file -> doc tu file ra -> hien thi len TFT => OKi
	unsigned char *Wbuffer;

	for(i = 0; i<5000; i++){
		fr.Data[i] = dataImg[i];
	}
	Wbuffer = (char*)malloc(sizeof(fr));
	memcpy(Wbuffer,(const unsigned char*)&fr,sizeof(fr));

	res = f_mount(0,&fs);
	/*-------------------------------------------------------------------*/
	res = f_open(&fdst, "CodeIMG.txt", FA_CREATE_ALWAYS | FA_WRITE);
	
	if(res != FR_OK){
		printf("open file error : %d\n\r",res);
		printf("Error\n");
	}
	else{
	  res = f_write(&fdst, Wbuffer, sizeof(fr), &bw);/* Ghi n� v�o t?p dst */
		if(res == FR_OK){
			printf("Creart FILE Successful\n");
			printf("write data ok! %d\n\r",bw);
		}
		else{
			printf("write data error : %d\n\r",res);
		}
	}
	f_close(&fdst);	
	
	printf("\nread file test......\n\r");
	res = f_open(&fsrc, "CodeIMG.txt", FA_OPEN_EXISTING | FA_READ);
	
	if(res != FR_OK){
		printf("open file error : %d\n\r",res);
	}
	else{
		res = f_read(&fsrc, Rbuffer,5000, &br);     /* Read a chunk of src file */
		if(res==FR_OK){
			printf("\n\rread data num : %d\n\r",br);
			printf("%s",Rbuffer);
		}
		else{
			printf("read file error : %d\n\r",res);
		}
	}		
	f_close(&fsrc);
	free(Wbuffer);
	TFT_Gui_Img(10,10,50,50,Rbuffer);
}
/*********************************************************************************/

void ThaiMCU(void){
	SD_ReadBmp(0,0,320,240,"/nhamdan2022/ytb.bmp");
	Delay_ms(9000);
	SD_ReadBmp(0,0,320,240,"/nhamdan2022/thaidv006.bmp");
	Delay_ms(9000);
	SD_ReadBmp(0,0,320,240,"/nhamdan2022/thaidv007.bmp");
	Delay_ms(9000);
	SD_ReadBmp(0,0,320,240,"/nhamdan2022/thaidv008.bmp");
	Delay_ms(9000);
	SD_ReadBmp(0,0,320,240,"/nhamdan2022/thaidv010.bmp");
	Delay_ms(9000);
}

void Test_Main(void){
//	Test_Fill();
//	Delay_ms(1000);
	TFT_Clear(WHITE);
	Test_FillRec();
	Delay_ms(1000);
	Test_Triangle();
	Delay_ms(1000);
	Test_Circle();
	Delay_ms(1000);
	TFT_Clear(WHITE);
	ThaiMCU();
//	Test_font_Char();
//	Delay_ms(3000);
//	TFT_Clear(WHITE);
//	Test_Font_String();
//	Delay_ms(3000);
//	TFT_Clear(WHITE);
//	Test_Font_String_Bold();
//	Delay_ms(3000);
//	TFT_Clear(WHITE);
//	Test_Print_Num();
//	Delay_ms(3000);
//	TFT_Clear(WHITE);
}
/*********************************************************************************/

int Check_Posi_Screen(u16 x1, u16 y1, u16 x2, u16 y2){
	if((x_pos >= x1 && x_pos <= x2) && (y_pos >= y1 && y_pos <= y2)) return 1;
	else return 0;
}

void SYS_Init(void){
	SystemInit();
	Delay_init(72);
	TFT_Init();	
	uart_Init();
	touchInit();
	MSD0_SPI_Configuration();
	Timer_Init();
	printf("Do Van Thai\n");
//	Button();
	
//	TFT_Gui_Fill(0,0,320,60,LIGHTGREY);//-- OKI
//	SD_ScanfFile();//-- OKI
//	TFT_Gui_Img(10,10,180,242,gImage_thaidv);//-- OKI
//	SD_ReadBmp(0,0,320,240,"/nhamdan2022/login.bmp"); //-- OKI
//	TFT_Print(26, 68,WHITE, BLACK, 16,"Do Van Thai" , 1);
//	SD_ReadBmp(0,60,320,180,"keyboard16b.bmp"); //-- OKI
//	TFT_Gui_Img(10,10,100,100,data1);
//	strTObyte();//-- OKI
/*********************************************************************/

}

int SCREEN_WAIT			= 1;
int SCREEN_LOGIN    = 0;
int SCREEN_KEYBOARD = 0;
int SCREEN_HOME     = 0;
char KeyBoard_On[10]={'q','w','e','r','t','y','u','i','o','p'};
int Set_Posi_Acc = 0, Set_Posi_Pass = 0;
int vri_FlagLBAccount = 0, vri_FlagLBPassword = 0;
int Input_Account = 0, Input_Password = 0;

/********************************* version 1 *********************************/
char temp_acc[20], temp_pass[20], temp_acc_after[20], temp_pass_after[20];
u8 *str_acc, *str_pass; 
int row_n0 = 0, row_n1 = 0; // hang so
int row_x1 = 0, row_x2 = 0; // hang tren
int row_x3 = 0, row_x4 = 0; // hang co so
int row_x5 = 0, row_x6 = 0; // Hang Duoi

volatile int vrvi_GetCounter_InputAccount  = 0;
volatile int vrvi_Flag_InputAccount 			 = 0;
volatile int vrvi_GetCounter_InputPassWord = 0;
volatile int vrvi_Flag_InputPassWord 			 = 0;
volatile int vrvi_GetCounter_PressLogin    = 0;

char user[]="thaimcu";
char pass[]="12345678";

void Login_Input_Account(void){
	for(i = 0; i < 10; i++){
		row_n0 = 2 + i*34;  row_n1 = row_n0 + 30;	// Hang So	
		row_x1 = 2 + i*34;  row_x2 = row_x1 + 30;	// Hang Tren		
		row_x3 = 15 + i*35; row_x4 = row_x3 + 30; // Hang Co So
		row_x5 = 50 + i*35; row_x6 = row_x5 + 30; // Hang Duoi
		
		if( Check_Posi_Screen(row_n0,60,row_n1,90) ){ // Hang So
			Set_Posi_Acc++;
			switch(i){
				case 0: TFT_Print_Num(6+(Set_Posi_Acc-1)*13, 25, BLACK, BLACK, 24, 0, 1, 1, 1);
								temp_acc[Set_Posi_Acc-1] = '1';
								printf("%c",temp_acc[Set_Posi_Acc-1]);
								x_pos = 0; y_pos = 0;								
								break; 
				case 1: TFT_Print_Num(6+(Set_Posi_Acc-1)*13, 25, BLACK, BLACK, 24, 0, 2, 1, 1);
								temp_acc[Set_Posi_Acc-1] = '2';
								printf("%c",temp_acc[Set_Posi_Acc-1]);
								x_pos = 0; y_pos = 0;
								break;
				case 2: TFT_Print_Num(6+(Set_Posi_Acc-1)*13, 25, BLACK, BLACK, 24, 0, 3, 1, 1);
								temp_acc[Set_Posi_Acc-1] = '3';
								printf("%c",temp_acc[Set_Posi_Acc-1]);
								x_pos = 0; y_pos = 0;
								break;
				case 3: TFT_Print_Num(6+(Set_Posi_Acc-1)*13, 25, BLACK, BLACK, 24, 0, 4, 1, 1);
								temp_acc[Set_Posi_Acc-1] = '4';
								printf("%c",temp_acc[Set_Posi_Acc-1]);
								x_pos = 0; y_pos = 0;
								break;
				case 4: TFT_Print_Num(6+(Set_Posi_Acc-1)*13, 25, BLACK, BLACK, 24, 0, 5, 1, 1);
								temp_acc[Set_Posi_Acc-1] = '5';
								printf("%c",temp_acc[Set_Posi_Acc-1]);
								x_pos = 0; y_pos = 0;
								break;
				case 5: TFT_Print_Num(6+(Set_Posi_Acc-1)*13, 25, BLACK, BLACK, 24, 0, 6, 1, 1);
								temp_acc[Set_Posi_Acc-1] = '6';
								printf("%c",temp_acc[Set_Posi_Acc-1]);
								x_pos = 0; y_pos = 0;
								break;
				case 6: TFT_Print_Num(6+(Set_Posi_Acc-1)*13, 25, BLACK, BLACK, 24, 0, 7, 1, 1);
								temp_acc[Set_Posi_Acc-1] = '7';
								printf("%c",temp_acc[Set_Posi_Acc-1]);
								x_pos = 0; y_pos = 0;
								break;
				case 7: TFT_Print_Num(6+(Set_Posi_Acc-1)*13, 25, BLACK, BLACK, 24, 0, 8, 1, 1);
								temp_acc[Set_Posi_Acc-1] = '8';
								printf("%c",temp_acc[Set_Posi_Acc-1]);
								x_pos = 0; y_pos = 0;
								break;
				case 8: TFT_Print_Num(6+(Set_Posi_Acc-1)*13, 25, BLACK, BLACK, 24, 0, 9, 1, 1);
								printf("%c",temp_acc[Set_Posi_Acc-1]);
								temp_acc[Set_Posi_Acc-1] = '9';
								x_pos = 0; y_pos = 0;
								break;
				case 9: TFT_Print_Num(6+(Set_Posi_Acc-1)*13, 25, BLACK, BLACK, 24, 0, 0, 1, 1);;
								temp_acc[Set_Posi_Acc-1] = '0';
								printf("%c",temp_acc[Set_Posi_Acc-1]);
								x_pos = 0; y_pos = 0;
								break;
				break;
			}
		} // end Hang So
		
		if( Check_Posi_Screen(row_x1,95,row_x2,130) ){ // Hang tren
			Set_Posi_Acc++;
			switch(i){
				case 0: TFT_Show_12x24_char(6+(Set_Posi_Acc-1)*13,25,BLACK,BLACK,'q',1);
								temp_acc[Set_Posi_Acc-1] = 'q';
								printf("%c",temp_acc[Set_Posi_Acc-1]);
								x_pos = 0; y_pos = 0;
								break; 
				case 1: TFT_Show_12x24_char(6+(Set_Posi_Acc-1)*13,25,BLACK,BLACK,'w',1);
								temp_acc[Set_Posi_Acc-1] = 'w';
								printf("%c",temp_acc[Set_Posi_Acc-1]);
								x_pos = 0; y_pos = 0;
								break;
				case 2: TFT_Show_12x24_char(6+(Set_Posi_Acc-1)*13,25,BLACK,BLACK,'e',1);
								temp_acc[Set_Posi_Acc-1] = 'e';
								printf("%c",temp_acc[Set_Posi_Acc-1]);
								x_pos = 0; y_pos = 0;
								break;
				case 3: TFT_Show_12x24_char(6+(Set_Posi_Acc-1)*13,25,BLACK,BLACK,'r',1);
								temp_acc[Set_Posi_Acc-1] = 'r';
								printf("%c",temp_acc[Set_Posi_Acc-1]);
								x_pos = 0; y_pos = 0;
								break;
				case 4: TFT_Show_12x24_char(6+(Set_Posi_Acc-1)*13,25,BLACK,BLACK,'t',1);
								temp_acc[Set_Posi_Acc-1] = 't';
								printf("\n%c\n",temp_acc[Set_Posi_Acc-1]);
								x_pos = 0; y_pos = 0;
								break;
				case 5: TFT_Show_12x24_char(6+(Set_Posi_Acc-1)*13,25,BLACK,BLACK,'y',1);
								temp_acc[Set_Posi_Acc-1] = 'y';
								printf("%c",temp_acc[Set_Posi_Acc-1]);
								x_pos = 0; y_pos = 0;
								break;
				case 6: TFT_Show_12x24_char(6+(Set_Posi_Acc-1)*13,25,BLACK,BLACK,'u',1);
								temp_acc[Set_Posi_Acc-1] = 'u';
								printf("%c",temp_acc[Set_Posi_Acc-1]);
								x_pos = 0; y_pos = 0;
								break;
				case 7: TFT_Show_12x24_char(6+(Set_Posi_Acc-1)*13,25,BLACK,BLACK,'i',1);
								temp_acc[Set_Posi_Acc-1] = 'i';
								printf("\n%c\n",temp_acc[Set_Posi_Acc-1]);
								x_pos = 0; y_pos = 0;
								break;
				case 8: TFT_Show_12x24_char(6+(Set_Posi_Acc-1)*13,25,BLACK,BLACK,'o',1);
								printf("%c",temp_acc[Set_Posi_Acc-1]);
								temp_acc[Set_Posi_Acc-1] = 'o';
								x_pos = 0; y_pos = 0;
								break;
				case 9: TFT_Show_12x24_char(6+(Set_Posi_Acc-1)*13,25,BLACK,BLACK,'p',1);
								temp_acc[Set_Posi_Acc-1] = 'p';
								printf("%c",temp_acc[Set_Posi_Acc-1]);
								x_pos = 0; y_pos = 0;
								break;
				break;
			}
		} // end Hang tren
		
		if( Check_Posi_Screen(row_x3,135,row_x4,170) ){// Hang co so
			Set_Posi_Acc++;
			switch(i){
				case 0: TFT_Show_12x24_char(6+(Set_Posi_Acc-1)*13,25,BLACK,BLACK,'a',1);
								temp_acc[Set_Posi_Acc-1] = 'a';
								printf("\n%c\n",temp_acc[Set_Posi_Acc-1]);
								x_pos = 0; y_pos = 0;
								break;
				case 1: TFT_Show_12x24_char(6+(Set_Posi_Acc-1)*13,25,BLACK,BLACK,'s',1);
								temp_acc[Set_Posi_Acc-1] = 's';
								x_pos = 0; y_pos = 0;
								break;
				case 2: TFT_Show_12x24_char(6+(Set_Posi_Acc-1)*13,25,BLACK,BLACK,'d',1);
								temp_acc[Set_Posi_Acc-1] = 'd';
								x_pos = 0; y_pos = 0;
								break;
				case 3: TFT_Show_12x24_char(6+(Set_Posi_Acc-1)*13,25,BLACK,BLACK,'f',1);
								temp_acc[Set_Posi_Acc-1] = 'f';
								x_pos = 0; y_pos = 0;
								break;
				case 4: TFT_Show_12x24_char(6+(Set_Posi_Acc-1)*13,25,BLACK,BLACK,'g',1);
								temp_acc[Set_Posi_Acc-1] = 'g';
								x_pos = 0; y_pos = 0;
								break;
				case 5: TFT_Show_12x24_char(6+(Set_Posi_Acc-1)*13,25,BLACK,BLACK,'h',1);
								temp_acc[Set_Posi_Acc-1] = 'h';
								printf("\n%c\n",temp_acc[Set_Posi_Acc-1]);
								x_pos = 0; y_pos = 0;
								break;
				case 6: TFT_Show_12x24_char(6+(Set_Posi_Acc-1)*13,25,BLACK,BLACK,'j',1);
								temp_acc[Set_Posi_Acc-1] = 'j';
								x_pos = 0; y_pos = 0;
								break;
				case 7: TFT_Show_12x24_char(6+(Set_Posi_Acc-1)*13,25,BLACK,BLACK,'k',1);
								temp_acc[Set_Posi_Acc-1] = 'k';
								x_pos = 0; y_pos = 0;
								break;
				case 8: TFT_Show_12x24_char(6+(Set_Posi_Acc-1)*13,25,BLACK,BLACK,'l',1);
								temp_acc[Set_Posi_Acc-1] = 'l';
								x_pos = 0; y_pos = 0;
								break;
				break;
			}
		} 

		if( Check_Posi_Screen(row_x5,175,row_x6,205) ){// Hang Duoi
			Set_Posi_Acc+=1;
			switch(i){
				case 0: TFT_Show_12x24_char(6+(Set_Posi_Acc-1)*13,25,BLACK,BLACK,'z',1);
								temp_acc[Set_Posi_Acc-1] = 'z';
								printf("\n%c\n",temp_acc[Set_Posi_Acc-1]);
								x_pos = 0; y_pos = 0;
								break;
				case 1: TFT_Show_12x24_char(6+(Set_Posi_Acc-1)*13,25,BLACK,BLACK,'x',1);
								temp_acc[Set_Posi_Acc-1] = 'x';
								x_pos = 0; y_pos = 0;
								break;
				case 2: TFT_Show_12x24_char(6+(Set_Posi_Acc-1)*13,25,BLACK,BLACK,'c',1);
								temp_acc[Set_Posi_Acc-1] = 'c';
								x_pos = 0; y_pos = 0;
								break;
				case 3: TFT_Show_12x24_char(6+(Set_Posi_Acc-1)*13,25,BLACK,BLACK,'v',1);
								temp_acc[Set_Posi_Acc-1] = 'v';
								x_pos = 0; y_pos = 0;
								break;
				case 4: TFT_Show_12x24_char(6+(Set_Posi_Acc-1)*13,25,BLACK,BLACK,'b',1);
								temp_acc[Set_Posi_Acc-1] = 'b';
								x_pos = 0; y_pos = 0;
								break;
				case 5: TFT_Show_12x24_char(6+(Set_Posi_Acc-1)*13,25,BLACK,BLACK,'n',1);
								temp_acc[Set_Posi_Acc-1] = 'n';
								printf("\n%c\n",temp_acc[Set_Posi_Acc-1]);
								x_pos = 0; y_pos = 0;
								break;
				case 6: TFT_Show_12x24_char(6+(Set_Posi_Acc-1)*13,25,BLACK,BLACK,'m',1);
								temp_acc[Set_Posi_Acc-1] = 'm';
								x_pos = 0; y_pos = 0;
								break;
				break;
			}
			if ( Check_Posi_Screen(290,175,320,205) ){ // press Delete
				x_pos = 0; y_pos = 0;
//				Set_Posi_Acc-=1;
				if(Set_Posi_Acc-1 <= 0) Set_Posi_Acc = 0;
				printf("\nSet_Posi_Acc - Del: %d\n",Set_Posi_Acc-1);
				TFT_Clear_Pos(6+(Set_Posi_Acc-2)*13, 25, (6+(Set_Posi_Acc-2)*13)+14, 25+24+1, LIGHTGREY);
				Set_Posi_Acc-=2;
				if(Set_Posi_Acc-2 <= 0) Set_Posi_Acc = 0;
			}
		} 	
	
		if(Check_Posi_Screen( 280, 209, 320 , 240) && vrvi_Flag_InputAccount == 1){ // Press Enter
			SD_ReadBmp(0,0,320,240,"/nhamdan2022/login_acc.bmp");
			x_pos = 0;
			y_pos = 0;
			printf("Acc: ");
			SCREEN_LOGIN = 1;
			SCREEN_KEYBOARD = 0;
			memcpy(temp_acc_after,temp_acc,Set_Posi_Acc);
			for(i = 0; i < Set_Posi_Acc; i++){
				printf("%c",temp_acc_after[i]);
				TFT_Show_12x24_char(26+i*12, 62, WHITE, BLACK, temp_acc_after[i], 1);
				Delay_ms(10);		
			}
			printf("\n");
			//memcpy(temp_acc,NULL,20);
			vrvi_GetCounter_InputAccount = 0;
		}	
	}		
}

void Login_Input_PassWord(void){
	for(i = 0; i < 10; i++){
		row_n0 = 2 + i*34;  row_n1 = row_n0 + 30;	// Hang So	
		row_x1 = 2 + i*34;  row_x2 = row_x1 + 30;	// Hang Tren		
		row_x3 = 15 + i*35; row_x4 = row_x3 + 30; // Hang Co So
		row_x5 = 50 + i*35; row_x6 = row_x5 + 30; // Hang Duoi
		
		if( Check_Posi_Screen(row_n0 , 60  , row_n1 , 90 ) ){// Hang So
			Set_Posi_Pass++;
			switch(i){
				case 0: TFT_Print_Num(6+(Set_Posi_Pass-1)*13, 25, BLACK, BLACK, 24, 0, 1, 1, 1);
								temp_pass[Set_Posi_Pass-1] = '1';
								printf("%c",temp_pass[Set_Posi_Pass-1]);
								x_pos = 0;
								y_pos = 0;
								break; 
				case 1: TFT_Print_Num(6+(Set_Posi_Pass-1)*13, 25, BLACK, BLACK, 24, 0, 2, 1, 1);
								temp_pass[Set_Posi_Pass-1] = '2';
								printf("%c",temp_pass[Set_Posi_Pass-1]);
								x_pos = 0;
								y_pos = 0;
								break;
				case 2: TFT_Print_Num(6+(Set_Posi_Pass-1)*13, 25, BLACK, BLACK, 24, 0, 3, 1, 1);
								temp_pass[Set_Posi_Pass-1] = '3';
								printf("%c",temp_pass[Set_Posi_Pass-1]);
								x_pos = 0;
								y_pos = 0;
								break;
				case 3: TFT_Print_Num(6+(Set_Posi_Pass-1)*13, 25, BLACK, BLACK, 24, 0, 4, 1, 1);
								temp_pass[Set_Posi_Pass-1] = '4';
								printf("%c",temp_pass[Set_Posi_Pass-1]);
								x_pos = 0;
								y_pos = 0;
								break;
				case 4: TFT_Print_Num(6+(Set_Posi_Pass-1)*13, 25, BLACK, BLACK, 24, 0, 5, 1, 1);
								temp_pass[Set_Posi_Pass-1] = '5';
								printf("%c",temp_pass[Set_Posi_Pass-1]);
								x_pos = 0;
								y_pos = 0;
								break;
				case 5: TFT_Print_Num(6+(Set_Posi_Pass-1)*13, 25, BLACK, BLACK, 24, 0, 6, 1, 1);
								temp_pass[Set_Posi_Pass-1] = '6';
								printf("%c",temp_pass[Set_Posi_Pass-1]);
								x_pos = 0;
								y_pos = 0;
								break;
				case 6: TFT_Print_Num(6+(Set_Posi_Pass-1)*13, 25, BLACK, BLACK, 24, 0, 7, 1, 1);
								temp_pass[Set_Posi_Pass-1] = '7';
								printf("%c",temp_pass[Set_Posi_Pass-1]);
								x_pos = 0;
								y_pos = 0;
								break;
				case 7: TFT_Print_Num(6+(Set_Posi_Pass-1)*13, 25, BLACK, BLACK, 24, 0, 8, 1, 1);
								temp_pass[Set_Posi_Pass-1] = '8';
								printf("%c",temp_pass[Set_Posi_Pass-1]);
								x_pos = 0;
								y_pos = 0;
								break;
				case 8: TFT_Print_Num(6+(Set_Posi_Pass-1)*13, 25, BLACK, BLACK, 24, 0, 9, 1, 1);
								printf("%c",temp_pass[Set_Posi_Pass-1]);
								temp_pass[Set_Posi_Pass-1] = '9';
								x_pos = 0;
								y_pos = 0;
								break;
				case 9: TFT_Print_Num(6+(Set_Posi_Pass-1)*13, 25, BLACK, BLACK, 24, 0, 0, 1, 1);;
								temp_pass[Set_Posi_Pass-1] = '0';
								printf("%c",temp_pass[Set_Posi_Pass-1]);
								x_pos = 0;
								y_pos = 0;
								break;
				break;
			}
		} // end Hang So
		
		if( Check_Posi_Screen(row_x1 , 95  , row_x2 , 130) ){ // Hang tren
			Set_Posi_Pass++;
			switch(i){
				case 0: TFT_Show_12x24_char(6+(Set_Posi_Pass-1)*13,25,BLACK,BLACK,'q',1);
								temp_pass[Set_Posi_Pass-1] = 'q';
								printf("%c",temp_pass[Set_Posi_Pass-1]);
								x_pos = 0;
								y_pos = 0;
								break; 
				case 1: TFT_Show_12x24_char(6+(Set_Posi_Pass-1)*13,25,BLACK,BLACK,'w',1);
								temp_pass[Set_Posi_Pass-1] = 'w';
								printf("%c",temp_pass[Set_Posi_Pass-1]);
								x_pos = 0;
								y_pos = 0;
								break;
				case 2: TFT_Show_12x24_char(6+(Set_Posi_Pass-1)*13,25,BLACK,BLACK,'e',1);
								temp_pass[Set_Posi_Pass-1] = 'e';
								printf("%c",temp_pass[Set_Posi_Pass-1]);
								x_pos = 0;
								y_pos = 0;
								break;
				case 3: TFT_Show_12x24_char(6+(Set_Posi_Pass-1)*13,25,BLACK,BLACK,'r',1);
								temp_pass[Set_Posi_Pass-1] = 'r';
								printf("%c",temp_pass[Set_Posi_Pass-1]);
								x_pos = 0;
								y_pos = 0;
								break;
				case 4: TFT_Show_12x24_char(6+(Set_Posi_Pass-1)*13,25,BLACK,BLACK,'t',1);
								temp_pass[Set_Posi_Pass-1] = 't';
								printf("\n%c\n",temp_pass[Set_Posi_Pass-1]);
								x_pos = 0;
								y_pos = 0;
								break;
				case 5: TFT_Show_12x24_char(6+(Set_Posi_Pass-1)*13,25,BLACK,BLACK,'y',1);
								temp_pass[Set_Posi_Pass-1] = 'y';
								printf("%c",temp_pass[Set_Posi_Pass-1]);
								x_pos = 0;
								y_pos = 0;
								break;
				case 6: TFT_Show_12x24_char(6+(Set_Posi_Pass-1)*13,25,BLACK,BLACK,'u',1);
								temp_pass[Set_Posi_Pass-1] = 'u';
								printf("%c",temp_pass[Set_Posi_Pass-1]);
								x_pos = 0;
								y_pos = 0;
								break;
				case 7: TFT_Show_12x24_char(6+(Set_Posi_Pass-1)*13,25,BLACK,BLACK,'i',1);
								temp_pass[Set_Posi_Pass-1] = 'i';
								printf("\n%c\n",temp_pass[Set_Posi_Pass-1]);
								x_pos = 0;
								y_pos = 0;
								break;
				case 8: TFT_Show_12x24_char(6+(Set_Posi_Pass-1)*13,25,BLACK,BLACK,'o',1);
								printf("%c",temp_pass[Set_Posi_Pass-1]);
								temp_pass[Set_Posi_Pass-1] = 'o';
								x_pos = 0;
								y_pos = 0;
								break;
				case 9: TFT_Show_12x24_char(6+(Set_Posi_Pass-1)*13,25,BLACK,BLACK,'p',1);
								temp_pass[Set_Posi_Pass-1] = 'p';
								printf("%c",temp_pass[Set_Posi_Pass-1]);
								x_pos = 0;
								y_pos = 0;
								break;
				break;
			}
		} // end Hang tren
		
		if( Check_Posi_Screen(row_x3 , 135 , row_x4 , 170) ){// Hang co so
			Set_Posi_Pass++;
			switch(i){
				case 0: TFT_Show_12x24_char(6+(Set_Posi_Pass-1)*13,25,BLACK,BLACK,'a',1);
								temp_pass[Set_Posi_Pass-1] = 'a';
								printf("\n%c\n",temp_pass[Set_Posi_Pass-1]);
								x_pos = 0;
								y_pos = 0;
								break;
				case 1: TFT_Show_12x24_char(6+(Set_Posi_Pass-1)*13,25,BLACK,BLACK,'s',1);
								temp_pass[Set_Posi_Pass-1] = 's';
								x_pos = 0;
								y_pos = 0;
								break;
				case 2: TFT_Show_12x24_char(6+(Set_Posi_Pass-1)*13,25,BLACK,BLACK,'d',1);
								temp_pass[Set_Posi_Pass-1] = 'd';
								x_pos = 0;
								y_pos = 0;
								break;
				case 3: TFT_Show_12x24_char(6+(Set_Posi_Pass-1)*13,25,BLACK,BLACK,'f',1);
								temp_pass[Set_Posi_Pass-1] = 'f';
								x_pos = 0;
								y_pos = 0;
								break;
				case 4: TFT_Show_12x24_char(6+(Set_Posi_Pass-1)*13,25,BLACK,BLACK,'g',1);
								temp_pass[Set_Posi_Pass-1] = 'g';
								x_pos = 0;
								y_pos = 0;
								break;
				case 5: TFT_Show_12x24_char(6+(Set_Posi_Pass-1)*13,25,BLACK,BLACK,'h',1);
								temp_pass[Set_Posi_Pass-1] = 'h';
								printf("\n%c\n",temp_pass[Set_Posi_Pass-1]);
								x_pos = 0;
								y_pos = 0;
								break;
				case 6: TFT_Show_12x24_char(6+(Set_Posi_Pass-1)*13,25,BLACK,BLACK,'j',1);
								temp_pass[Set_Posi_Pass-1] = 'j';
								x_pos = 0;
								y_pos = 0;
								break;
				case 7: TFT_Show_12x24_char(6+(Set_Posi_Pass-1)*13,25,BLACK,BLACK,'k',1);
								temp_pass[Set_Posi_Pass-1] = 'k';
								x_pos = 0;
								y_pos = 0;
								break;
				case 8: TFT_Show_12x24_char(6+(Set_Posi_Pass-1)*13,25,BLACK,BLACK,'l',1);
								temp_pass[Set_Posi_Pass-1] = 'l';
								x_pos = 0;
								y_pos = 0;
								break;
				break;
			}
		} 

		if( Check_Posi_Screen(row_x5 , 175 , row_x6 , 205) ){// Hang Duoi
			Set_Posi_Pass+=1;
			switch(i){
				case 0: TFT_Show_12x24_char(6+(Set_Posi_Pass-1)*13,25,BLACK,BLACK,'z',1);
								temp_pass[Set_Posi_Pass-1] = 'z';
								printf("\n%c\n",temp_pass[Set_Posi_Pass-1]);
								x_pos = 0;
								y_pos = 0;
								break;
				case 1: TFT_Show_12x24_char(6+(Set_Posi_Pass-1)*13,25,BLACK,BLACK,'x',1);
								temp_pass[Set_Posi_Pass-1] = 'x';
								x_pos = 0;
								y_pos = 0;
								break;
				case 2: TFT_Show_12x24_char(6+(Set_Posi_Pass-1)*13,25,BLACK,BLACK,'c',1);
								temp_pass[Set_Posi_Pass-1] = 'c';
								x_pos = 0;
								y_pos = 0;
								break;
				case 3: TFT_Show_12x24_char(6+(Set_Posi_Pass-1)*13,25,BLACK,BLACK,'v',1);
								temp_pass[Set_Posi_Pass-1] = 'v';
								x_pos = 0;
								y_pos = 0;
								break;
				case 4: TFT_Show_12x24_char(6+(Set_Posi_Pass-1)*13,25,BLACK,BLACK,'b',1);
								temp_pass[Set_Posi_Pass-1] = 'b';
								x_pos = 0;
								y_pos = 0;
								break;
				case 5: TFT_Show_12x24_char(6+(Set_Posi_Pass-1)*13,25,BLACK,BLACK,'n',1);
								temp_pass[Set_Posi_Pass-1] = 'n';
								printf("\n%c\n",temp_pass[Set_Posi_Pass-1]);
								x_pos = 0;
								y_pos = 0;
								break;
				case 6: TFT_Show_12x24_char(6+(Set_Posi_Pass-1)*13,25,BLACK,BLACK,'m',1);
								temp_pass[Set_Posi_Pass-1] = 'm';
								x_pos = 0;
								y_pos = 0;
								printf("\nSet_Posi_Pass - m: %d\n",Set_Posi_Pass);
								break;
				break;
			}
			if ( Check_Posi_Screen(290,175,320,205) ){ // press Delete
				x_pos = 0;
				y_pos = 0;
				Set_Posi_Pass-=1;
				printf("\nSet_Posi_Pass - Del: %d\n",Set_Posi_Pass);
				TFT_Clear_Pos(6+(Set_Posi_Pass-1)*13, 25, (6+(Set_Posi_Pass-1)*13)+12-1, 25+24+1, LIGHTGREY);
				Set_Posi_Pass-=1;
			}
		} 	
	
		if( Check_Posi_Screen(280    , 209 , 320    , 240) && (vrvi_Flag_InputPassWord == 1) ){// Press Enter
			printf("Press Enter PassWord Oki\n");
			vrvi_Flag_InputPassWord = 0;
			SD_ReadBmp(0,0,320,240,"/nhamdan2022/login_pass.bmp");
			SCREEN_LOGIN    = 1;
			SCREEN_KEYBOARD = 0;	
			for(i = 0; i < Set_Posi_Acc; i++){
				TFT_Show_12x24_char(26+i*12, 62, WHITE, BLACK, temp_acc_after[i], 1);
			}
			printf("Pass: ");	
			memcpy(temp_pass_after,temp_pass,Set_Posi_Pass);
			for(i = 0; i < Set_Posi_Pass; i++){
				printf("%c",temp_pass_after[i]);
				TFT_Show_12x24_char(26+i*12, 110, WHITE, BLACK, '*', 1);		
			}
			printf("\n");
			vrvi_GetCounter_InputPassWord = 0;
		}	
	}		
}

/*
function: Interface_PressKeyBoard
IMGName: /nhamdan2022/labankey_lower.bmp
				 /nhamdan2022/labankey_upper.bmp
*/
void Interface_PressKeyBoard(const TCHAR* IMGName){
	TFT_Gui_Fill(0,0,320,60,LIGHTGREY);
	SD_ReadBmp(0,60,320,180,IMGName);
}


void Interface_Login(void){
	
	if(SCREEN_WAIT){
		SD_ReadBmp(0,0,320,240,"/nhamdan2022/login.bmp");
		SCREEN_LOGIN = 1;
		SCREEN_WAIT = 0;
	}
	
	if(SCREEN_LOGIN){		
		if( Check_Posi_Screen(20,62,300,82) ){ // Input Account
			vrvi_GetCounter_InputAccount = 1;
			SCREEN_KEYBOARD = 1;
			SCREEN_LOGIN = 0;
			printf("MODE SCREEN LOGIN - INPUT ACCOUNT\n");
			printf("vrvi_GetCounter_InputAccount: %d\n",vrvi_GetCounter_InputAccount);
			printf("SCREEN_KEYBOARD: %d\n", SCREEN_KEYBOARD);
			x_pos = 0; y_pos = 0;
		}
		else if( Check_Posi_Screen( 20, 107, 300 , 135) ){ // Input PassWord
			printf("MODE SCREEN LOGIN - INPUT PASSWORD\n");
			vrvi_GetCounter_InputPassWord = 1;
			SCREEN_KEYBOARD  = 1;
			SCREEN_LOGIN     = 0;
			x_pos = 0; y_pos = 0;
		}
		else if( Check_Posi_Screen( 20, 200, 300 , 230) ){ // Press Login
			printf("MODE SCREEN LOGIN - PRESS LOGIN\n");
			vrvi_GetCounter_PressLogin = 1;
			SCREEN_KEYBOARD  = 0;
			SCREEN_LOGIN     = 0;
			SCREEN_HOME			 = 1;
			x_pos = 0; y_pos = 0;
		}
	}
	if(SCREEN_KEYBOARD){
		SCREEN_LOGIN = 0;
		if(vrvi_GetCounter_InputAccount == 1){ // Press Input Account;
			vrvi_GetCounter_InputAccount++;
			vrvi_Flag_InputAccount = 1;			
			printf("\nvrvi_GetCounter_InputAccount: %d\n",vrvi_GetCounter_InputAccount);
			Interface_PressKeyBoard("/nhamdan2022/labankey_lower.bmp");
			Delay_ms(10);	
		}	
		if(vrvi_GetCounter_InputAccount > 1){	 // Process Account;
			Login_Input_Account();
		}	
		
		if(vrvi_GetCounter_InputPassWord == 1){ // Press Input PassWord;
			vrvi_GetCounter_InputPassWord++;
			vrvi_Flag_InputPassWord = 1;
			printf("\nvrvi_GetCounter_InputPassWord: %d\n",vrvi_GetCounter_InputPassWord);
			Interface_PressKeyBoard("/nhamdan2022/labankey_lower.bmp");
			Delay_ms(10);
		}
		if(vrvi_GetCounter_InputPassWord > 1){	// Process PassWord;
			Login_Input_PassWord();
		}	
	}
	if(SCREEN_HOME){
		if(strcmp(user,temp_acc_after)==0 && strcmp(pass,temp_pass_after)==0){
			printf("\nSet_Posi_Acc: %d\n",Set_Posi_Acc);
			printf("Set_Posi_Pass: %d",Set_Posi_Pass);
			printf("\nAcc: ");
			for(i = 0; i < Set_Posi_Acc; i++){
				printf("%c",temp_acc_after[i]);
			}
			printf("\n");
			printf("Pass: ");
			for(i = 0; i < Set_Posi_Pass; i++){
				printf("%c",temp_pass_after[i]);	
			}
			printf("\n");
			memcpy(temp_acc,'\0',Set_Posi_Acc);
			memcpy(temp_acc_after,'\0',Set_Posi_Acc);
			memcpy(temp_pass,'\0',Set_Posi_Pass);
			memcpy(temp_pass_after,'\0',Set_Posi_Pass);
			Set_Posi_Acc = 0; Set_Posi_Pass = 0;			
			SD_ReadBmp(135,95,50,50,"/nhamdan2022/checked.bmp");
			Delay_ms(20000);
			//Test_Main();
			
			SCREEN_HOME = 0;
			SCREEN_WAIT = 1;
		}
		else{
			printf("\nSet_Posi_Acc: %d\n",Set_Posi_Acc);
			printf("Set_Posi_Pass: %d\n",Set_Posi_Pass);
			printf("Acc: ");
			for(i = 0; i < Set_Posi_Acc; i++){
				printf("%c",temp_acc_after[i]);
			}
			printf("\n");
			printf("Pass: ");
			for(i = 0; i < Set_Posi_Pass; i++){
				printf("%c",temp_pass_after[i]);	
			}
			printf("\n");
			memcpy(temp_acc,'\0',Set_Posi_Acc);
			memcpy(temp_acc_after,'\0',Set_Posi_Acc);
			memcpy(temp_pass,'\0',Set_Posi_Pass);
			memcpy(temp_pass_after,'\0',Set_Posi_Pass);
			Set_Posi_Acc = 0; Set_Posi_Pass = 0;		
			SD_ReadBmp(135,95,50,50,"/nhamdan2022/error.bmp");
			Delay_ms(20000);
			SCREEN_HOME = 0;
			SCREEN_WAIT = 1;
		}		
	}
}


void SYS_Run(void){
	if(vri_FlagTimer2){
		Test_TouchV1();
	}
	Interface_Login();
}




