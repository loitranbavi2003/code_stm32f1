#ifndef _LIB_SDCARD_H_
#define _LIB_SDCARD_H_

#include "ff.h"
#include "SPI_MSD0_Driver.h"
#include <string.h>
#include "usart.h"

extern FRESULT res;

extern FATFS fs;        		 /* Khu v?c l�m vi?c (h? th?ng t?p) cho ? �?a logic */

extern FIL fsrc, fdst;      /* file objects */
/* Exported Functions --------------------------------------------------------*/

/* N�t b?t d?u du?c qu�t (c�n du?c s? d?ng nhu khu v?c l�m vi?c) */
FRESULT scan_files (char* path ); 
         	
int SD_TotalSize(char *path);
void SD_ScanfFile(void);
//void SD_ReadBmp(u16 x, u16 y, u16 picxel_X, u16 picxel_Y, const TCHAR* Fname);


#endif














